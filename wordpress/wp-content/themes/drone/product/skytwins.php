<?php
/*
  Template Name: Skytwins Product
 */
get_header();
?>

<body id="product">
  <div id="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main>
      <section class="banner">
        <div class="container">
          <div class="banner__main">
            <h3>SKYTWINS</h3>
            <span>スカイツインズ</span>
          </div>
        </div>
      </section><!-- .banner // -->

        <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>ドローンのプロポをツインモニター化！<br>パイロットが取得できる情報が倍増する</h3>
              <span>スカイツインズとは？</span>
            </div>
            <div class="lineQualification__main">
              <p>
                <div class="ac"><img src="<?php bloginfo('template_url')?>/product/skytwins/images/skytwinslogo.jpg?v=791cb52fe167374c12b0c00e597316ed" alt="thumb"></div>
              </p>
              <div class="postThumb">
                <div class="row">
                  <div class="postThumb__text col-md-7">
                    <h3>スカイツインズとは？</h3>
                    <p>スカイツインズとは、プロポに２つのモニターを取り付け、各モニターの情報を同時に取得することができる日本発の装置です。スカイツインズは、ユーザー自身の手により簡単にプロポに取り付けることが可能です。</p>
                  </div>
                  <div class="postThumb__thumb col-md-5" style="text-align: center;">
                    <img src="<?php bloginfo('template_url')?>/product/skytwins/images/skytwinsimg.jpg?v=b5dd254056717a1b095177fe7ddaf41c" alt="スカイツインズ">
                  </div>
                </div>
              </div>
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
        <section class="whyIsDrones">
          <article class="container">
            <div class="headBox">
              <h3>スカイツインズでできること</h3>
            </div>
            <div class="whyIsDrones__main">
              <p>通常ドローンパイロットは、プロポに取り付けたモニター1画面のみを見ながら操作しますが、スカイツインズを使用すれば、赤外線カメラ/暗視カメラ/画像解析アプリ/運航管理システム(ドローンスコープ)などから得られる各種情報をリアルタイムに可視光映像と一緒に見ることができるので、取得できる情報量が２倍に増えます。<br>また、現場のパイロットの情報量が増えるだけでなく、様々な現場の作業効率を飛躍的に向上させることも期待できます。</p>
            </div>
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i> スカイツインズの使用例</h3>
            </div>
            <div class="example mb50">
              <h3>▶︎例1　デュアルサーチ200と併用した場合</h3>
              <p>通常はこのように可視光映像を１画面で見るのみですが、スカイツインズを使えばサーマルカメラの映像も同時に見ることが可能です。</p>
              <div class="ac mb-20">
                <img src="<?php bloginfo('template_url')?>/product/skytwins/images/example01.jpg?v=8cda50623e16535ded071e1727f3e98c" alt="スカイツインズ">
              </div>
              <div class="notes mb-20">
                <p>※「デュアルサーチ200」とは… サーマルカメラ世界シェアNO.1のFLIR製の高性能サーマルカメラを組み込んだ、小型ドローン搭載に最適化された装置です。</p>
              </div>
            </div>
            <div class="example mb50">
              <h3>▶︎例2　ドローンスコープマッパーと併用した場合</h3>
              <p>通常はこのように可視光映像を１画面で見るのみですが、スカイツインズを使えばドローンスコープマッパーの映像も同時に見ることが可能です。</p>
              <div class="ac mb-20">
                <img src="<?php bloginfo('template_url')?>/product/skytwins/images/example02.jpg?v=de4cdfec5d15f0f8e4398d18d058ff67" alt="スカイツインズ">
              </div>
              <div class="notes mb-20">
                <p>※ドローンスコープマッパーとは… 遠隔地からリアルタイムで映像を確認し、パイロットへ指示ができる監視・指示システムです。</p>
              </div>
            </div>
            <div class="example mb50">
              <h3>▶︎例3　他現場で撮影中のドローンと併用した場合</h3>
              <p>通常はこのように可視光映像を１画面で見るのみですが、スカイツインズを使えば他現場で撮影中の2台目のドローン映像も同時に見ることが可能です</p>
              <div class="ac mt-20">
                <img src="<?php bloginfo('template_url')?>/product/skytwins/images/example03.jpg?v=e416684649eddb33cab6ee1e1a183b7e" alt="スカイツインズ">
              </div>
              <div class="notes mb-20">
                <p>※ドローンスコープマッパーとは… 遠隔地からリアルタイムで映像を確認し、パイロットへ指示ができる監視・指示システムです。</p>
              </div>
            </div>

          </article>
        </section>
        <section class="regarding">
          <article class="container">
            <div class="headBox">
              <h3>スカイツインズのご購入について</h3>
            </div>
            <div class="whyIsDrones__main">
              <p>「スカイツインズ」は、世界初や日本初のドローン関連商品＆サービスを提供する会員制サービス「スカイビジネス会員」に入会している方(個人、法人、自治体)限定販売となっております。</p>
            </div>
          </article>
        </section>
        <!-- End /box Regarding -->

        <section class="lessonPrice">
          <article class="container">
                <h3><i class="fa fa-bookmark" aria-hidden="true"></i> 価格：12,800円 (税別)<br>
              <span class="snote">※機体は付属しません。 </span></h3>
            <div class="lessonPrice__main">
              <div class="lessonPrice__main-list">
                <h3>スカイツインズ</h3>


                          <div class="row">
                  <div class="postThumbTitle__thumb col-md-6">


                                <p style="color:#00a0e9; margin:0 0 10px 0; text-align:left">【脱着方法】</p>
                                <p>スカイツインズは、Phantom 4 &amp; Proのプロポ送信機の標準モニター機器用ホルダーまたは、上下から挟み込む一般的なモニター機器用ホルダーへ、通常のモニター機器を脱着させる方法と同様に容易に脱着することができます。<br>
                                また、スカイツインズの装着する二つのスマートフォン、タブレット、モニター機器なども標準ホルダーと同様の使用方法でホルダーから容易に脱着することが可能です。</p>
                                
                                 <p style="color:#00a0e9; margin:20px 0 10px 0; text-align:left">【スペック表】</p>
                      <ul style="list-style-type : disc;  margin:0 0 20px 20px; text-align:left">
                        <li>サーチライト用メインLED ４個</li>
                        <li>制御器本体 １個</li>
                        <li>リポバッテリー 4S/4000mA １個</li>
                        <li>取扱説明書</li>
                      </ul>

                    
                  </div>
                  <div class="postThumbTitle__text col-md-6">
                                <img src="<?php bloginfo('template_url')?>/product/skytwins/images/skytwinsimg04.jpg?v=ea01e34755d0b898c6577a1da8fd8957" class="pc" alt="thumb" style="text-align :right">
                                                          <img src="<?php bloginfo('template_url')?>/product/skytwins/images/skytwinsimg04.jpg?v=ea01e34755d0b898c6577a1da8fd8957" class="sp" alt="thumb" style="text-align :center">
                    <button type="button" class="btn pc" style="margin:30px 0; font-size:1.4em;"   onclick="location.href='https://dronestore-plus.com/i/dwt000009view'">ご購入はこちら<i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
                <button type="button" class="btn sp" style="margin:30px 0;"   onclick="location.href='https://dronestore-plus.com/i/dwt000009view'">ご購入はこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>




                 <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店 </a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>




          </article>
        </section>
        <!-- End /box Lesson price -->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>
</body>
</html>