<?php
/*
  Template Name: Dual Search Product
 */
get_header();
?>

<body id="product">
    <!-- You write code for this content block in another file -->
<div id="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <section class="banner">
        <div class="container">
          <div class="banner__main">
            <h3>DUAL SEARCH 200</h3>
            <span>デュアルサーチ200</span>
          </div>
        </div>
    </section><!-- .banner // -->

    <section class="lineQualification">
        <article class="container">
            <div class="headBox">
                <h3>見えないものが見えてくる。<br>世界最軽量のサーマルドローンシステム</h3>
                <span>デュアルサーチ200</span>
            </div>
            <div class="ac">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/dualsearc_logo1.svg" alt="thumb">
                <p style=" margin-top:10px;">（ Phantom 4 Pro専用 ）<p>
              </div>
              <div class="title-bg-red">
              <p> 国内ドローン業界では最安値</p>
              </div>
                    <p style="text-align: left; font-size:10px; margin-bottom:5px;">▼ DUAL SEARCH 200 PV</p>
                    <div class="movie-wrap">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/fazy9wMHESs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

          </div>
        </article>
    </section>




    <section class="whyIsDrones">
        <article class="container">
          <div class="headBox pc">
            <h3>サーマルドローンは潜在市場を可視化・顕在化する</h3>
          </div>
                <div class="headBox sp">
            <h3>サーマルドローンは潜在市場を<br>可視化・顕在化する</h3>
          </div>
                <div class="ac mt-20">
            <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/img-dualsearch200_01.jpg" alt="デュアルサーチ200">
          </div>
                <div class="ac mt-20">
            <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/img-dualsearch200_02.jpg" alt="デュアルサーチ200">
          </div>
        </article>
    </section>

      <section class="whyIsDrones">
        <article class="container">
                <div class="headBox">
            <h3>サーマルドローンがレスキューをナビゲート</h3>
          </div>
                <div class="row">
                <div class="postThumbTitle__thumb col-md-4">
                <div class="mb30">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/rescue01.jpg" class="pc" alt="レスキュー写真" style="text-align :right">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/rescue01.jpg" class="sp" alt="レスキュー写真" style="text-align :center">
                </div>
                <p style="text-align: left;">これまで発見が難しかった遭難者・行方不明者の発見に貢献します。</p>
                </div>

                <div class="postThumbTitle__thumb col-md-4">
                <div class="mb30">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/rescue02.jpg" class="pc" alt="防災写真" style="text-align :center">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/rescue02.jpg" class="sp" alt="防災写真" style="text-align :center">
                </div>
                <p style="text-align: left;">火災発生時の現場、大災害時に道路が封鎖されていてもドローンなら上空から確認できます。さらにこれまで探索が打ち切られた「夜」であってもサーモグラフィカメラならば高い精度で発見可能です。</p>
                </div>

                <div class="postThumbTitle__thumb col-md-4 ">
                <div class="mb30">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/rescue03.jpg" class="pc" alt="野生動物調査写真" style="text-align :left">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/rescue03.jpg" class="sp" alt="野生動物調査写真" style="text-align :center">
                </div>
                <p style="text-align: left;">動物・鳥獣の監視・調査は保護の目的や農作物への被害を食い止めるために重要です。通常の可視光カメラでは発見できなかった動物もサーマルドローンは発見できます。</p>
                </div>
                </div>
        </article>
    </section>





    <section class="lineQualification">
        <article class="container">

                <div class="lineQualification__main">

            <p></p><div class="ac">
            </div>
            <p></p>

            <div class="postThumb">

                     <div class="ac mt-20 mb30">
                     <h3>デュアルサーチ200とは？</h3>
            <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/dualsearc_logo2.svg" alt="デュアルサーチ200ロゴ">
          </div>



               <p style="text-align: center; font-size:20px; font-weight:600; line-height:2" class="pc">小型ドローン搭載用に最適化された形状と機能を兼ね備えた装置で、<br>様々な産業シーンに幅広く利用することが可能です！</p>
               <p style="text-align: center; font-size:15px; font-weight:600; line-height:1.7" class="sp">小型ドローン搭載用に最適化された形状と機能を兼ね備えた装置で、<br>様々な産業シーンに幅広く利用することが可能です！</p>


              <div class="row">
                <div class="postThumb__text col-md-12">
                  <div class="mb30">
                                <p class="pc">サーマルカメラ”世界シェアNo.1”のFLIR社製で、高性能なサーマルカメラ「BOSON(重さ約30g)」を組み込んだ「デュアルサーチ200」は、小型ドローンにユーザー自らが簡単に取り付けられます。 それにより、小型ドローンに標準装備されている可視光カメラと一緒に、サーマルカメラで空撮することができます。</p>
                                </div>
                                <div>
                  <p class="sp">サーマルカメラ”世界シェアNo.1”のFLIR社製で、高性能なサーマルカメラ「BOSON(重さ約30g)」を組み込んだ「デュアルサーチ200」は、小型ドローンにユーザー自らが簡単に取り付けられます。 それにより、小型ドローンに標準装備されている可視光カメラと一緒に、サーマルカメラで空撮することができます。</p>
                                </div>
                </div>
              </div>



             <div class="ac mt-20 mb20">
          <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_img01.jpg" class="pc" alt="デュアルサーチ200写真">


                <div class="ac mt-20">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_img05.jpg" class="sp" alt="デュアルサーチ200写真">
              </div>
                  </div>
         <div class="ac mt-20">
          <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_img04.png" class="pc" alt="デュアルサーチ200写真">
                </div>






                <div class="row">
                <div class="postThumbTitle__thumb col-md-6">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_img02.jpg" class="sp" alt="デュアルサーチ200写真" style="text-align :center">
                </div>
                <div class="postThumbTitle__thumb col-md-6">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_img03.jpg" class="sp" alt="デュアルサーチ200写真" style="text-align :center">
                </div>
                <div class="postThumbTitle__thumb col-md-6">
                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_img04.jpg" class="sp" alt="デュアルサーチ200写真" style=" text-align:center">
                </div>
               </div>

            </div>
          </div>
        </article>

        <article class="container">

          <div class="postThumbTitle" >

                  <p style="text-align: center; font-size:20px; font-weight:500; background-color:#00a0e9; color:#FFF; padding-bottom:20px; padding-top:20px; margin-top:50px;" class="pc">
                  産業用ドローンにトランスフォーム！！</p>

                     <p style="text-align: center; font-size:20px; font-weight:500; background-color:#00a0e9; color:#FFF; padding-bottom:20px; padding-top:20px; margin-top:50px;" class="sp">
                  産業用ドローンにトランスフォーム!!</p>

          </div>



                <div class="ac mt-20 pc">
            <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/img-dualsearch200_03.jpg" alt="デュアルサーチ200">
          </div>

                 <div class="ac mt-20 sp">
             <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/transform.jpg" alt="デュアルサーチ200">
          </div>
             <div class="row pc">
                <div class="postThumbTitle__thumb col-md-6 mb50">
                              <p style="text-align: center;">標準可視光カメラのみ</p>
                            </div>
                <div class="postThumbTitle__thumb col-md-6 mb50">
                              <p style="text-align: center;">サーモグラフィカメラと<br/>標準カメラのサーモグラフィカメラ</p>
                </div>
              </div>
            </div>
        </article>
    </section>








    <section class="whyIsDrones">
        <article class="container">

          <div class="row">
                <div class="postThumbTitle__thumb col-md-6 mb50">
                            <div class="mb30">
                              <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/logo_boson.jpg" class="pc" alt="BOSONロゴ" style="text-align :right">
                                </div>
                                <div class="mb50">
                  <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/logo_boson_sp.jpg" class="sp" alt="BOSONロゴ" style="text-align :center">
                            </div>
                            <p style="text-align: left;">「デュアルサーチ200」に組み込まれたサーマルカメラ「BOSON」は、解像度が"320×256"、フレームレートは"60"、映像伝送距離は最長"200メートル"ですが、映像の録画は最長"2km"まで可能です。</p>
                            </div>

                <div class="postThumbTitle__thumb col-md-6">
                              <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/boson.jpg" class="pc" alt="BOSON写真" style="text-align :right">
                                <div class="mb50">
                  <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/boson.jpg" class="sp" alt="BOSON写真" style="text-align :center">
                </div></div>
              </div>
          </div>
        </article>
    </section>



    <section class="whyIsDrones">
        <article class="container">

          <div class="row">
                <div class="postThumbTitle__thumb col-md-6">
                            <div class="mb30">
                                <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/skytwins.jpg" class="pc" alt="スカイツインズ写真" style="text-align :right">
                  <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/logo_st_sp.jpg" class="sp" alt="スカイツインズロゴ" style="text-align :center">
                            </div>
                            <p style="text-align: left;" class="sp">「デュアルサーチ200」に付属のモニターアームを使えば、小型ドローンのプロポに二つ目のモニターを追加することができ、可視光カメラとサーマルカメラの映像を一緒に見ることが可能となります。</p>
                            </div>
                <div class="postThumbTitle__thumb col-md-6">
                           <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/logo_st.jpg" class="pc" alt="スカイツインズロゴ" style="text-align :right">
                <div>
                            <div class="mb30">
                            <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/skytwins.jpg" class="sp"alt="スカイツインズ写真" style="text-align :center">
                                </div>
                            <p style="text-align: left;" class="pc">「デュアルサーチ200」に付属のモニターアームを使えば、小型ドローンのプロポに二つ目のモニターを追加することができ、可視光カメラとサーマルカメラの映像を一緒に見ることが可能となります。</p>
                </div>
              </div>
          </div>










              <div class="row">
                <div class="postThumbTitle__thumb col-md-6">
                             <div class="mb30">
                              <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/skytwins_img01.jpg" class="pc" alt="スカイツインズ写真" style="text-align :right">
                  <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/skytwins_img01.jpg" class="sp" alt="スカイツインズ写真" style="text-align :center">
                                 <p style="text-align: right; font-size:9px; color:#999">※イメージ画像</p>
                                 </div>

                                <p style="text-align: left;">「デュアルサーチ200」は、お絵かきアプリ(スカイスケッチ)を使って空撮中に映像をキャプチャし、モニターに指でタッチしてマーキングやコメント書きすることができます。</p>
                            </div>
                <div class="postThumbTitle__thumb col-md-6">
                            <div class="mb30">
                              <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/skytwins_img02.jpg" class="pc" alt="スカイツインズ写真" style="text-align :right">
                  <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/skytwins_img02.jpg" class="sp" alt="スカイツインズ写真" style="text-align :center">
                                 <p style="text-align: right; font-size:9px; color:#999">※イメージ画像</p>
                                 </div>
                            <p style="text-align: left;">気になる箇所をお絵描きアプリ(スカイスケッチ)でマーキングすることにより、"現場作業効率の向上"や、提出物となる"業務報告書の付加価値"を上げることが可能となります。</p>
                </div>
              </div>
        </article>
    </section>


    <section class="whyIsDrones">
        <article class="container">
         <p style="text-align: center; font-size:20px; font-weight:600; margin-top:80px;" class="pc">デュアルサーチ200の操縦体験フェア開催中！(無料)</p>

          <p style="text-align: center; font-size:18px; font-weight:600; line-height:0 margin-top:50px;" class="sp">デュアルサーチ200の</p>
             <p style="text-align: center; font-size:18px; font-weight:600; line-height:0" class="sp">操縦体験フェア開催中！(無料)</p>

                <div class="ac mt-20">
            <a href="/dualsearch200_/fair/"><img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_fair.png" alt="試運転フェア"></a>
          </div>

        </article>
    </section>


      <section class="regarding">
        <article class="container">
          <div class="headBox">
            <h3>デュアルサーチ200のご購入について</h3>
          </div>

          <div class="whyIsDrones__main">
            <p>「デュアルサーチ200」は、世界初や日本初のドローン関連商品＆サービスを提供する会員制サービス「スカイビジネス会員」に入会している方(個人、法人、自治体)限定販売となっております。</p>

          </div>
        </article>
      </section>


      <section class="lessonPrice">
        <article class="container">
          <h3><i class="fa fa-bookmark" aria-hidden="true"></i> 価格：590,000円 (税別)<br>
            <span class="snote">デュアルサーチ200　〜 frame rate 60Hz 〜</span></h3>
          <div class="lessonPrice__main">

            <div class="lessonPrice__main-list">
              <h3>デュアルサーチ200</h3>
                <div class="row">
                <div class="postThumbTitle__thumb col-md-6">

                  <p style="text-align: left;">サーマルカメラ”世界シェアNo.1”のFLIR社製で、高性能なサーマルカメラ「BOSON(重さ約30g)」を組み込んだ「デュアルサーチ200」は、小型ドローンにユーザー自らが簡単に取り付けられます。 それにより、小型ドローンに標準装備されている可視光カメラと一緒に、サーマルカメラで空撮することができます。</p>

                  <p style="color:#00a0e9; margin:0 0 10px 0; text-align:left">【使用環境仕様】</p>
                    <ul style="list-style-type : disc;  margin:0 0 20px 20px; text-align:left">
                      <li>動作温度範囲：0〜40°C（結露なきこと）</li>
                      <li>保存温度範囲：-20〜60°C（結露なきこと）</li>
                      <li>最大推奨風速：5/s 以下</li>
                    </ul>
                </div>
                <div class="postThumbTitle__text col-md-6">
                  <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_img05.jpg" class="pc" alt="デュアルサーチ200写真" style="text-align :right">
                  <img src="<?php bloginfo('template_url')?>/product/dualsearch200_new/images/ds200_img05.jpg" class="sp" alt="デュアルサーチ200写真" style="text-align :center">
                  <a href="https://dronestore-plus.com/i/dwt000008view" target="_blank"><button type="button" class="btn pc" style="margin:30px 0; font-size:1.4em;">ご購入はこちら　<i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button></a>
                </div>
              </div>
              <a href="https://dronestore-plus.com/i/dwt000008view" target="_blank"><button type="button" class="btn sp" style="margin:30px 0;">ご購入はこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button></a>
            </div>

                 <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店 </a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
 

              <!--<div class="lessonPrice__main-postThumbbg">
                  <div class="textBox">
                    <p>ドローン中継士 ３級を受講し合格された方には、「ドローン中継士 ３級 資格証」が発行されます(有効期限5年/民間資格)。</p>

                  </div>
              </div>-->
            </div>
            <!--<div class="lessonPrice__main-list">

              <div class="lessonPrice__main-nav">
                <ul>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">東京DN店</a></li>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">仙台フォーラス店</a></li>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">福岡イオン乙金店 </a></li>
                </ul>
                <ul>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">栃木宇都宮店</a></li>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">福島郡山店</a></li>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">東京お台場店 </a></li>
                </ul>
                <ul>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">千葉BIGHOP店</a></li>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">東京千代田店</a></li>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">AKITIA店 </a></li>
                </ul>
                <ul>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">高知本町店</a></li>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">久慈 西モータース店</a></li>
                  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="index.html#">京田辺店 </a></li>
                </ul>
              </div>
              <div class="boxMore">
                <button type="button" class="btn">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
              </div>
            </div>-->
          </div>
        </article>
      </section>
      <!-- End /box Lesson price -->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
<?php get_footer();?>
</body>

</html>
