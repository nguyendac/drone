<?php
get_header();
?>
<body>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto"><?php the_title();?></h2>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="page">
      <div class="row wrap">
        <?php the_content()?>
      </div>
      <!--wrap-->
    </section>
    <!--/.st_shop-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>