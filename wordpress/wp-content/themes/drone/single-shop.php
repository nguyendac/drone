<?php
get_header();
?>
<body>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">SHOPS</h2>
        <span>店舗一覧</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="st_shop">
      <div class="row wrap">
        <ul class="breadcrumb show_pc">
          <li><a href="/">TOP</a></li>
          <li><a href="/shops">SHOPS</a></li>
          <li><?php _e(taxonomy_hierarchy())?>の店舗一覧</li>
        </ul>
        <!--/.breadcrumb-->
        <div class="gr_shop_detail">
          <article>
            <h3 class="ttl_art"><?php the_title();?></h3>
            <div class="art_top">
              <?php
              if(get_post_meta($post->ID,'shop_image',true)): ?>
                <figure><img src="<?php _e(get_post_meta($post->ID,'shop_image',true)['url'])?>" alt="<?php the_title()?>"></figure>
              <?php else:?>
                <figure class="default_logo"><img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="<?php the_title()?>"></figure>
              <?php endif;?>
              <div class="art_top_l">
                <p><?php _e(nl2br(get_post_meta($post->ID,'shop_intro',true)))?></p>
              </div>
              <!--/.left-->
            </div>
            <!--/.art_top-->
            <div class="art_ct">
              <div class="art_ct_map">
                <iframe src="https://maps.google.com/maps?q='<?php _e(get_post_meta($post->ID,'shop_address',true))?>+'&output=embed" allowfullscreen></iframe>
              </div>
              <!--/.map-->
              <div class="art_ct_infor">
                <dl>
                  <dt>店名</dt>
                  <dd><?php the_title();?></dd>
                </dl>
                <dl>
                  <dt>オープン日</dt>
                  <dd><?php  _e(get_post_meta($post->ID,'shop_date',true))?></dd>
                </dl>
                <dl>
                  <dt>場所</dt>
                  <dd><?php  _e(get_post_meta($post->ID,'shop_address',true))?></dd>
                </dl>
                <dl>
                  <dt>営業時間</dt>
                  <dd><?php  _e(get_post_meta($post->ID,'shop_time_open',true))?></dd>
                </dl>
                <dl>
                  <dt>お問い合わせ</dt>
                  <dd><a class="tel" href="tel:<?php _e(get_post_meta($post->ID,'shop_tel',true))?>"><?php  _e(get_post_meta($post->ID,'shop_tel',true))?></a> / <a class="mail" href="mailto:<?php _e(get_post_meta($post->ID,'shop_mail',true))?>"><?php _e(get_post_meta($post->ID,'shop_mail',true))?></a></dd>
                </dl>
                <dl>
                  <dt>備考</dt>
                  <dd>完全予約制</dd>
                </dl>
                <dl>
                  <dt>予約方法</dt>
                  <dd>お電話、<a class="contact" href="/shops/contact-shop?shop_name=<?php the_title()?>">メール </a>でお問い合わせください。</dd>
                </dl>
                <ul class="btn_l">
                  <?php
                  $check_shop_type = get_post_meta($post->ID,'shop_type',false);
                  $shop_type = $GLOBALS['variable_shop']['shop_type'];
                  foreach($shop_type as $key => $value):
                  ?>
                  <li>
                    <?php if(in_array($key,$check_shop_type)): ?>
                      <a href="/shops?type=<?php _e($value)?>"><?php _e($value)?></a>
                    <?php else:?>
                      <a href="#" class="none"><?php _e($value)?></a>
                    <?php endif;?>
                  </li>
                <?php endforeach;?>
                </ul>
                <!--/.btn_l-->
              </div>
              <!--/.infor-->
            </div>
            <!--/.art_ct-->
            <div class="art_ct_bt">
              <?php
                $shop_mail = 'ryu@waza-mono.com';
                if(get_post_meta($post->ID,'shop_email',true)!=''){
                  $shop_mail = get_post_meta($post->ID,'shop_email',true);
                }
              ?>
              <a class="btn_link" href="/shops/contact-shop?shop_name=<?php the_title()?>&shop_email=<?php _e($shop_mail)?>&id=<?php echo $post->ID?>"><span>問い合わせ・予約</span></a>
              <em><span>お問合せ電話番号</span><a class="tel roboto" href="tel:<?php _e(get_post_meta($post->ID,'shop_tel',true))?>"><?php _e(get_post_meta($post->ID,'shop_tel',true))?></a></em>
            </div>
            <!--/.art_bt-->
          </article>
          <div class="back_shop"><a href="/shops">店舗一覧へ戻る</a></div>
        </div>
        <!--/.gr_shop-->
      </div>
      <!--wrap-->
    </section>
    <!--/.st_shop-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>