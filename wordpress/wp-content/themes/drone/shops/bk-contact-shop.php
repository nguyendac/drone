<?php
/*
  Template Name: Contact Shop Page
 */
 if( !session_id()){
   session_start();
 }
 if(!$_GET) {
   global $wp_query;
   $wp_query->set_404();
   status_header( 404 );
   get_template_part( 404 ); exit();
 }
 if(isset($_POST['sm'])) {
   $check = array();
   $mail = '';
   $post = $_POST;
   if(!isset($_POST['check_privacy'])) {
     array_push($check,$key);
     flash('check_privacy','<span class="wpcf7-not-valid-tip">必須項目です</span>');
   } else {
     flash('check_privacy_checked','checked');
   }
   foreach($post as $key => $value) {
     if(!$value) {
       array_push($check,$key);
       flash($key,'<span class="wpcf7-not-valid-tip">必須項目です</span>');
     }
     if($key == 'email') {
       if(!$value) {
         array_push($check,$key);
         flash($key,'<span class="wpcf7-not-valid-tip">必須項目です</span>');
       } else {
         if(!filter_var($value,FILTER_VALIDATE_EMAIL)) {
           flash($key,'<span class="wpcf7-not-valid-tip">必須項目です</span>');
         } else {
           $mail = $value;
         }
       }
     }
     if($key == 'email_confirm' && $value != $mail) {
       array_push($check,$key);
       flash($key,'<span class="wpcf7-not-valid-tip">必須項目です</span>');
     }
     if($value) {
       flash($key.'_value',$value);
     }
   }
   if(count($check) > 0) {
     wp_safe_redirect(get_bloginfo('url').'/shops/contact-shop?shop_name='.$post["shop_name"].'&shop_email='.$post['shop_email'].'&id='.$_GET['id']);
     exit;
   } else {
     $to = $post['shop_email'];
     $subject = 'DRONE THE WORLD '.$post['shop_name'].' へのお問い合わせ';
     $headers =  'MIME-Version: 1.0' . "\r\n";
     $headers .= 'From: '.$post['first_name'].$post['last_name'].'様'.' <info@drone-the-world.com>' . "\r\n";
     $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
     $mess_admin = $post['first_name'].$post['last_name'].'様<br><br>';
     $mess_admin .= "この度はお問い合わせありがとうございます。<br><br>";
     $mess_admin .= '━━━━━━━━━━━━━━━━━━━━━━━━<br>';
     $mess_admin .= '以下の内容でメールを受け付けました。<br>';
     $mess_admin .= '━━━━━━━━━━━━━━━━━━━━━━━━<br>';
     $mess_admin .= '■ お名前　 '.$post['first_name'].$post['last_name'].'<br>';
     $mess_admin .= '■ メールアドレス　 '.$post['email'].'<br>';
     $mess_admin .= '■ お問い合わせ内容<br><br>';
     $mess_admin .= $post['content'];
     $mess_admin .= '<br><br>';
     $mess_admin .= '———————-<br>';
     $mess_admin .= 'このメールは DRONE THE WORLD '.$post['shop_name'].' (https://drone-the-world.com/) のお問い合わせフォームから送信されました';
     mail($to, $subject, $mess_admin, $headers);

     // client
     $client = $post['email'];
     $subject_client = 'DRONE THE WORLD へのお問い合わせ';
     $mess_client =  $post['first_name']. $post['last_name'].'様<br><br>';
     $mess_client .= "この度はお問い合わせありがとうございます。<br><br>";
     $mess_client .= '━━━━━━━━━━━━━━━━━━━━━━━━<br>';
     $mess_client .= '以下の内容でメールを受け付けました。<br>';
     $mess_client .= '━━━━━━━━━━━━━━━━━━━━━━━━<br>';
     $mess_client .= '■ お名前　 '.$post['first_name'].$post['last_name'].'<br>';
     $mess_client .= '■ メールアドレス　 '.$post['email'].'<br>';
     $mess_client .= '■ お問い合わせ内容<br><br>';
     $mess_client .= $post['content'];
     $mess_client .= '<br><br>';
     $mess_client .= '———————-<br>';
     $mess_client .= 'このメールは DRONE THE WORLD (https://drone-the-world.com/) のお問い合わせフォームから自動送信されました';
     mail($client, $subject_client, $mess_client, $headers);
     // end client
     flash('success','<div class="wpcf7-response-output wpcf7-mail-sent-ok" role="alert">ありがとうございます。メッセージは送信されました。</div>');
   }
 }
get_header();
?>
<body>
<?php if (have_posts()) : while (have_posts()) : the_post();?>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">SHOPS</h2>
        <span>店舗一覧</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="st_contact">
      <div class="row">
        <ul class="breadcrumb show_pc">
          <li><a href="/">TOP</a></li>
          <li><a href="/shops">SHOPS</a></li>
          <li><a href="<?php echo get_post_permalink($_GET['id'])?>"><?php _e($_GET['shop_name'])?></a></li>
          <li>お問い合わせ</li>
        </ul>
        <!--/.breadcrumb-->
        <div class="gr_contact wrap">
          <h3 class="ttl_art"><?php _e($_GET['shop_name'])?></h3>
          <p class="req">*必須</p>
          <form class="frm_contact" action="" method="post">
            <div class="frm_group">
              <label>お名前<sub>*</sub></label>
              <div class="ip_left frm_ip">
                <div class="cts_ip">
                  <input type="text" class="frm_control" placeholder="姓" name="first_name" value="<?php flash('first_name_value')?>">
                  <?php flash('first_name')?>
                </div>
                <div class="cts_ip">
                  <input type="text" class="frm_control" placeholder="名" name="last_name" value="<?php flash('last_name_value')?>">
                  <?php flash('last_name')?>
                </div>
              </div>
            </div>

            <div class="frm_group">
              <label>メールアドレス<sub>*</sub></label>
              <div class="ip_left">
                <input type="text" class="frm_control" placeholder="example@example.com" name="email" value="<?php flash('email_value')?>">
                <?php flash('email')?>
                <em>※確認のため2度入力してください</em>
              </div>
            </div>

            <div class="frm_group">
              <label>メールアドレス確認<sub>*</sub></label>
              <div class="ip_left">
                <input type="text" class="frm_control" placeholder="example@example.com" name="email_confirm">
                <?php flash('email_confirm')?>
              </div>
            </div>

            <div class="frm_group">
              <label>お問い合わせ内容<sub>*</sub></label>
              <div class="ip_left">
                <textarea class="frm_textarea" name="content"><?php flash('content_value')?></textarea>
                <?php flash('content')?>
              </div>
            </div>

            <div class="frm_group">
              <label>個人情報の取扱い<sub>*</sub></label>
              <div class="ip_left">
                <label class="frm_checkbox"><input type="checkbox" name="check_privacy" class="check" <?php flash('check_privacy_checked')?>><span><a href="/privacy">個人情報の取扱い</a>に同意する</span></label>
                <?php flash('check_privacy')?>
              </div>
            </div>
            <input type="hidden" name="shop_name" value="<?php _e($_GET['shop_name'])?>">
            <input type="hidden" name="shop_email" value="<?php _e($_GET['shop_email'])?>">
            <div class="frm_btn">
              <button type="submit" name="sm" value="1"><span>送信する</span></button>
            </div>

          </form>
          <?php flash('success')?>
          <!-- <div class="frm_contact">
            <?php //the_content();?>
          </div> -->
        </div>
        <!--/.gr_contact-->
      </div>
      <!--/.wrap-->
    </section>
    <!--/.st_contact-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>