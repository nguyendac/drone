<div class="gr_shop">
  <div class="s_item">
    <h3 class="ttl_art">目的から検索</h3>
    <div class="gr_item">
      <ul class="gr_item_l">
        <?php
          $shop_type = $GLOBALS['variable_shop']['shop_type'];
          foreach($shop_type as $key => $value):
        ?>
        <li>
          <a href="/shops?type=<?php _e($value)?>">
            <figure>
              <img src="<?php bloginfo('template_url')?>/shops/images/img_0<?php _e($key)?>.jpg" alt="Images 01">
              <figcaption><?php _e($value)?></figcaption>
            </figure>
          </a>
        </li>
        <?php endforeach;?>
      </ul>
    </div>
    <!--/.gr_item-->
  </div>
  <!--/.s_item-->
</div>