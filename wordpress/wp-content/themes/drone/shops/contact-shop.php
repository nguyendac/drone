<?php
/*
  Template Name: Contact Shop Page
 */
get_header();
?>
<body>
<?php if (have_posts()) : while (have_posts()) : the_post();?>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">SHOPS</h2>
        <span>店舗一覧</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="st_contact">
      <div class="row">
        <ul class="breadcrumb show_pc">
          <li><a href="/">TOP</a></li>
          <li><a href="/shops">SHOPS</a></li>
          <li><a href="<?php echo get_post_permalink($_GET['id'])?>"><?php _e($_GET['shop_name'])?></a></li>
          <li>お問い合わせ</li>
        </ul>
        <!--/.breadcrumb-->
        <div class="gr_contact wrap">
          <h3 class="ttl_art"><?php _e($_GET['shop_name'])?></h3>
          <p class="req">*必須</p>
          <div class="frm_contact">
            <?php the_content();?>
          </div>
        </div>
        <!--/.gr_contact-->
      </div>
      <!--/.wrap-->
    </section>
    <!--/.st_contact-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>