<?php
/*
  Template Name: Shop Search
 */
get_header();
?>

<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">SHOPS</h2>
        <span>店舗一覧</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="st_shop">
      <div class="row wrap">
        <ul class="breadcrumb show_pc">
          <li><a href="/">TOP</a></li>
          <li>SHOPS</li>
        </ul>
        <!--/.breadcrumb-->
        <?php
          $type = '';
          $type_name = '';
          $shop_type = $GLOBALS['variable_shop']['shop_type'];
          if(array_key_exists('type',$_GET)) {
            $tmp = $_GET['type'];
            foreach($shop_type as $key => $value) {
              if($value === $tmp) {
                $type = $key;
                $type_name = $value;
              }
            }
          }
        ?>
        <?php if(!$type):?>
        <div class="gr_shop">
          <div class="s_item">
            <h3 class="ttl_art">エリアから検索</h3>
            <div class="bx_area">
              <?php
                $terms = apply_filters('all_terms','');
                foreach($terms as $key => $term):
              ?>
              <dl class="dl_0<?php _e($term->order)?>">
                <dt><?php _e($term->name_parent)?></dt>
                <dd>
                  <ul>
                    <?php foreach($term->childs as $child): ?>
                    <li class="<?php echo (!$child->count) ?  'none': '';?>"><a href="<?php _e(esc_url(get_term_link($child->term_id)))?>"><?php _e($child->name)?></a></li>
                    <?php endforeach;?>
                  </ul>
                </dd>
              </dl>
              <?php endforeach;?>
            </div>
            <!--/.bx_area-->
          </div>
          <!--/.s_item-->
        </div>
        <?php else:?>
          <div class="gr_shop_shoplist">
            <h3 class="ttl_art"><?php _e($type_name)?></h3>
            <div class="item_art">
          <?php
           $articles = apply_filters('shop_type',$type);
           while($articles->have_posts()):$articles->the_post();
          ?>
          <article>
            <div class="top">
              <h4><a href="<?php the_permalink()?>"><?php the_title();?></a></h4>
            </div>
            <?php
            if(get_post_meta($post->ID,'shop_image',true)): ?>
              <figure><a href="<?php the_permalink();?>"><img src="<?php _e(get_post_meta($post->ID,'shop_image',true)['url'])?>" alt="<?php the_title()?>"></a></figure>
            <?php else:?>
              <figure class="default_logo"><a href="<?php the_permalink()?>"><img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="<?php the_title()?>"></a></figure>
            <?php endif;?>
            <a class="btn_link" href="<?php the_permalink()?>"><span>詳細を見る</span></a>
            <!-- <em><span>お問合せ電話番号</span><a class="tel roboto" href="tel:<?php _e(get_post_meta($post->ID,'shop_tel',true))?>"><?php _e(get_post_meta($post->ID,'shop_tel',true))?></a></em> -->
            <p><?php _e(get_post_meta($post->ID,'shop_address',true))?></p>
          </article>
          <?php endwhile;wp_reset_query();?>
          </div>
        </div>
        <?php endif;?>
        <!--/.gr_shop-->
        <?php get_template_part('shops/shops','type')?>
		  
		                  <div class="gr_art row">
                        <div class="col-md-12"  style="text-align:center; margin-top:50px;">
                          <a href="https://drone-net.co.jp/taiken_seminar/"  target="_blank"><figure><img src="https://drone-the-world.com/wp-content/uploads/2018/11/banner.jpg" alt="thumb"></figure></a>
                        </div>
                      </div>
		  
	  				        
      </div>

      <!--wrap-->

    </section>

    <!--/.st_shop-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>

<?php get_footer();?>
</body>
</html>