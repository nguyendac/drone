window.addEventListener('DOMContentLoaded',function(){
  $('#show_list_area').next().slideUp();
  $(document).on('click','#show_list_area',function(){
    if($(this).hasClass('active')){
      $(this).removeClass('active');
      $(this).next().slideUp();
    } else {
      $(this).addClass('active');
      $(this).next().slideDown();
    }
  })
})