<?php
/*
  Template Name: Store Service
 */
get_header();
?>

<body id="product">
    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
            <div class="container">
              <div class="banner__main">
                <h3>DRONE STORE＋</h3>
                <span>ドローンストアプラス</span>
              </div>
            </div>
        </section><!-- .banner // -->

          <section class="lineQualification">
            <article class="container">
              <div class="headBox pc">
                <h3>他では絶対に手に入らない！<br>ドローンネット社製の完全オリジナル商品を<br>取り扱う会員制通販サイト「ドローンストア＋」</h3>
              </div>
              <div class="headBox sp" style="font-size:90%;">
                <h3>他では絶対に手に入らない！<br>ドローンネット社製の<br>完全オリジナル商品を取り扱う<br>会員制通販サイト<br>「ドローンストア＋」</h3>
              </div>
              <div class="postThumb">
                <div class="row">
                  <div class="postThumb__text col-md-7-2">
                    <h3  class="pc">drone store+(ドローン ストア プラス)とは</h3>
                    <h3 class="sp">drone store+<br>(ドローン ストア プラス)とは</h3>
                    <p>ドローンネット社が運営するアジア初、日本初、業界初といった新規性＆進歩性を有する、ドローンソリューションを利用できる会員専用の通販サイトです。無料登録で購入できるオリジナル商品から、スカイビジネス会員（月額7,980円）限定の特別商品までお客様のニーズに合わせてビジネス、エンターテイメントに活用できるここでしか購入できないオリジナル商品を多数取り揃えております。ご購入の際は<a href="https://c1.members-support.jp/drone-the-world/Login" target="_blank" style="color:#eb6100;"><strong><u>こちら</u></strong></a>から新規会員登録を行ってください。</p>
                  </div>
                  <p>
                    <div class="ac" style="padding:10px">
                      <a href="https://dronestore-plus.com" target="_blank"  ><img src="<?php bloginfo('template_url')?>/service/store/images/logo_dronestoreplus.jpg" alt="thumb"></a>
                    </div>
                  </p>
                  <div class="link">
                    <p>
                      <a href="https://dronestore-plus.com" target="_blank"  style="font-size:1.3em; color:#eb6100; font-weight:700; margin: 1em 0 1em 1em; float : left;">▶︎ <u>drone store+ へ</u></a>
                    </p>
                    <p><a href="https://dronestore-plus.com/shopping-guide/" target="_blank"  style="font-size:1.3em; color:#eb6100; font-weight:700; margin: 0 0 0 1em; float : left;">▶︎ <u>drone store+ ご利用ガイドはコチラ</u></a>
                    </p>
                  </div>
                </div>
              </div>
            </article>
          </section>
          <!-- End /box LINE qualification -->
          <section class="regarding">
            <article class="container">
              <div class="headBox pc">
                <h3 >drone store+ ご利用について</h3>
              </div>
                <div class="headBox sp">
                <h3 style="font-size: 1.6em;">drone store+ ご利用について</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>「drone store+」は、ドローンネット社が運営する会員制サービスに入会している方（個人、法人、自治体）限定の通販サイトとなっております。</p>
              </div>
            </article>
          </section>
          <!-- End /box Regarding -->

          <section class="lessonPrice">
            <article class="container">
              <div class="lessonPrice__main">
                  <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店 </a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </article>
          </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>

  </html>