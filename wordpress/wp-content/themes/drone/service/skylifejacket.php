<?php
/*
  Template Name: Sky Life Jacket Service
 */
get_header();
?>

<body id="product">
    <!-- You write code for this content block in another file -->

    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
            <div class="container">
              <div class="banner__main">
                <h3>SKY LIFE JACKET</h3>
                <span>スカイライフジャケット</span>
              </div>
            </div>
        </section><!-- .banner // -->

          <section class="lineQualification st_suka">
            <article class="container">
              <div class="headBox">
                <h3>緊急災害時、水難事故の人命救助に<br>ドローンがライフジャケットを届ける!<br><span>スカイライフジャケット</span>とは？</h3>
              </div>
              <div class="img_top">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skylifejacket/images/sky_jacket.jpg" alt="">
                </figure>
              </div>
              <div class="postThumb">
                <div class="row">
                  <div class="postThumb__text col-md-7">
                    <h3>スカイライフジャケットとは？</h3>
                    <p>簡単な手順でInspire2に取り付けることが可能な特許出願中
                    の<strong style="font-weight: bold;">ライフジャケット投下装置</strong>です。<br>
                    ドローンで救難者の近くに自動膨張式ライフジャケットを運
                    び、救助隊が到着するまでの安全を確保します。</p>
                  </div>
                  <div class="postThumb__text col-md-5">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/skylifejacket/images/img_fly.jpg" alt="">
                    </figure>
                  </div>
                </div>
              </div>
            </article>
          </section>
          <!-- End /box LINE qualification -->
          <section class="st_life">
            <article class="container">
              <div class="headBox">
                <h3  class="pc">スカイライフジャケットでできること</h3>
                 <h3  class="sp" style="font-size:1.6em;">スカイライフジャケットでできること</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>自然災害が多発している日本においては、安全性の確保が難しい危険な場所、人が立ち入れない狭小地や道路などが寸断されて孤
                立してしまった場所なども容易に移動・撮影できるドローンの活躍に大きな期待が寄せられています。「スカイライフジャケット
                」はその中でも難しいとされてきた、<span>緊急時の水難事故における上空からの救助活動が可能</span>となります。ヘリコプターが入り込め
                ない場所や、災害時・緊急時のソリューションの一つとして自治体などでも今後ますます活躍のシーンが拡がります。
                また、「スカイサーチライト」を搭載した別機と連携することで、<span>夜間や洞窟など暗い場所で水難事故が発生した場合でも</span>、強力
                LEDで現場を照らし、救難者を確認した後に正確な位置にライフジャケットを投下することが可能となります。</p>
              </div>
            </article>
            <article class="container">
              <div class="headBox">
                <h3 class="pc">スカイライフジャケット稼働時動画</h3>
                 <h3  class="sp" style="font-size:1.6em;">スカイライフジャケット稼働時動画</h3>
              </div>
              <div class="whyIsDrones__main">
                <p class="video"><iframe width="836" height="440" src="https://www.youtube.com/embed/8-iy4yTvOQ0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></p>
                <!-- <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skylifejacket/images/img_h.jpg" alt="">
                </figure> -->
              </div>
            </article>
          </section>
          <!--/.st_life-->
          <section class="st_use whyIsDrones">
            <article class="container">
              <div class="postThumbTitle">
                <h3 class="ttl_use"><i class="fa fa-check" aria-hidden="true"></i> スカイライフジャケットの使用方法</h3>
              </div>
              <div class="main_art">
                <p>①投下ユニットの取り付け<br>ブラケットA〜Cをタイラップとネジで固定して取り付ける。</p>
                <p>②ライフジャケットの組み込み<br>ライフジャケットを指定の方法で折りたたみ、ベルト部分に組み込む。</p>
                <p>③プロポ設定<br>本装置を組み込んだ後は必ず、自動ランディングギアOFF設定を行う。</p>
                <p>④投下動作<br>・風向きなどを考慮して10m程度の高度より投下。<br>・河川などで投下する場合は、要救助者よりも川上10m前後を目標に投下。</p>
                <p>※救助の状況は現場ごとに異なりますので、実際の救助時を想定して常に飛行訓練、投下訓練を行い適切な投下ができるよう<br>飛行技術工場に努めましょう。</p>
              </div>
              <!--/.main_art-->
            </article>
            <article class="container">
              <div class="headBox">
                <h3  class="pc">スカイライフジャケットご購入について</h3>
                <h3  class="sp"  style="font-size:1.6em; line-height: 1.6em">スカイライフジャケット<br>ご購入について</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>「スカイライフジャケット」は、世界初や日本初のドローン関連商品＆サービスを提供する会員制サービス「スカイビジネ<br>ス会員」に入会している方(個人、法人、自治体)限定販売となっております。</p>
              </div>
            </article>
          </section>
          <!--/.st_use-->
           <section class="lessonPrice">
          <article class="container">
            <h3><i class="fa fa-bookmark" aria-hidden="true"></i> 価格：138,000円 (税別)<br>
            <span class="snote">※機体は付属しません。 </span></h3>
            <div class="lessonPrice__main">
              <div class="lessonPrice__main-list">
                <h3>スカイライフジャケット</h3>
                <div class="row">
                  <div class="postThumbTitle__thumb col-md-6">
                    <p style="color:#00a0e9; margin:0 0 10px 0; text-align:left;font-weight: bold;">【セット内容】</p>
                    <ul style="list-style-type : disc;  margin:0 0 20px 0; text-align:left">
                      <li><strong style="font-weight: bold;">・ブラケット(A)</strong></li>
                      <li><strong style="font-weight: bold;">・ブラケット(B)</strong></li>
                      <li><strong style="font-weight: bold;">・自動膨張式ライフジャケット</strong></li>
                      <li><strong style="font-weight: bold;">・タイラップ</strong></li>
                      <li><strong style="font-weight: bold;">・ライフジャケットブラケットB取付ネジ</strong></li>
                      <li><strong style="font-weight: bold;">・ライフジャケット予備ボンベ</strong></li>
                    </ul>
                  </div>
                  <div class="postThumbTitle__text thumb_life col-md-6">
                    <img src="<?php bloginfo('template_url')?>/service/skylifejacket/images/img_03.jpg" alt="thumb" style="text-align :right">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <p style="margin-bottom: 10px;">※ライフジャケットは、国土交通省型式承認品Type G及びType A が装着可能です。</p>
                    <p style="margin-bottom: 10px;">※本製品は緊急時の人名救助を目的に開発されています。 航空法132条の3号における緊急時の飛行においては
                    人命救助が優先となりますので、飛行許可は不要ですが、訓練や試験飛行については通常の飛行となりますので
                    飛行には必ず「危険物輸送」「物件投下」の 飛行許可を取得する必要があります。 </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4"></div>
                  <div class="col-md-8"><button type="button" class="btn pc" style="margin:30px 0 50px; font-size:1.4em;"  onclick="location.href='https://dronestore-plus.com/i/dwt000010view'">ご購入はこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button></div>
                  
                 
                  
                </div>
                <button type="button" class="btn sp" style="margin:30px 0;" onclick="location.href='https://dronestore-plus.com/i/dwt000010view'">ご購入はこちら<i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                
                <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
                <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
          </article>
        </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>

  </html>