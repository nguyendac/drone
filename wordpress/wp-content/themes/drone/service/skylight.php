<?php
/*
  Template Name: Sky Light Service
 */
get_header();
?>

<body id="product">
  <div id="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main>
      <section class="banner">
          <div class="container">
            <div class="banner__main">
              <h3>SKY SEARCH LIGHT</h3>
              <span>スカイサーチライト</span>
            </div>
          </div>
      </section><!-- .banner // -->

      <section class="lineQualification">
        <article class="container">
          <div class="headBox">
            <h3>強力LED照明で夜間の飛行をサポートする</h3>
            <span>スカイサーチライトとは？</span>
          </div>
          <div class="lineQualification__main pc">
            <div class="postThumb_solid mb50">
              <div class="row">
                <div class="postThumb__thumb_solid" style="text-align: left;">
                  <img src="<?php bloginfo('template_url')?>/service/skylight/images/searchlightlogo.jpg?v=7f56e58010bb17fcd2926031435d2009" alt="スカイサーチライトロゴ">
                </div>
                <div class="postThumb__thumb_right" style="text-align: right;">
                  <img src="<?php bloginfo('template_url')?>/service/skylight/images/serchlightimg.jpg?v=da7c7c6a563cb756b739072c97fef916" alt="スカイサーチライト">
                </div>
              </div>
            </div>
          </div>
          <div class="lineQualification__main sp">
            <div class="postThumb_solid mb50">
              <div class="row">
                <div class="postThumb__thumb_solid" style="text-align: center; width:100%;">
                  <img src="<?php bloginfo('template_url')?>/service/skylight/images/skysearchimglogo.jpg?v=3cc6e7522bf0cf81b436d74df0a4b538" alt="スカイサーチライトロゴ">
                </div>
              </div>
            </div>
          </div>
          <div class="postThumb">
            <div class="row">
              <div class="postThumb__text col-md-7-2">
                <h3>スカイサーチライトとは？</h3>
                <p>機体のネジを交換して取り付けることが可能な、<strong>Inspaire2専用設計のLEDサーチライト</strong>です。脚を下げた状態では機体周辺照明用のサブLEDが点灯し、脚を上げるとサブLEDが消灯しメインLEDが点灯します。<br>国交省での飛行許可も取得済み<span style="color:#eb6100;">(飛行性能に影響がない簡易改造による申請)</span>で、日常場面で使用可能です。 </p>
              </div>
              <p>
                <div class="ac" style="padding:10px">
                  <img src="<?php bloginfo('template_url')?>/service/skylight/images/skysearchimg2.jpg?v=3c0f3ca8a902613d46183300065f07ff" alt="thumb">
                </div>
              </p>
            </div>
          </div>
        </article>
      </section>
      <!-- End /box LINE qualification -->
        <section class="whyIsDrones">
          <article class="container">
            <div class="headBox pc">
              <h3>スカイサーチライトはこんなところで活躍します</h3>
            </div>
            <div class="headBox sp">
              <h3 style="line-height:1.8em; font-size:1.6em;">スカイサーチライトは<br>こんなところで活躍します</h3>
            </div>
            <div class="postThumbTitle">
                <h3 class="pc"><i class="fa fa-check" aria-hidden="true"></i> 夜間の捜索や、災害時、停電時に夜間を照らす</h3>
                <h3 class="pc"><i class="fa fa-check" aria-hidden="true"></i> 物件投下装置と併用しながら現場を明るく照らす</h3>
                <h3 class="pc"><i class="fa fa-check" aria-hidden="true"></i> 災害対策作業中に明るく照らす</h3>
                <h3 class="sp" style="font-size:1em;"><i class="fa fa-check" aria-hidden="true"></i> 夜間の捜索や、災害時、停電時に夜間を照らす</h3>
                <h3 class="sp" style="font-size:1em;"><i class="fa fa-check" aria-hidden="true"></i> 物件投下装置と併用しながら現場を明るく照らす</h3>
                <h3 class="sp" style="font-size:1em;"><i class="fa fa-check" aria-hidden="true"></i> 災害対策作業中に明るく照らす</h3>
            </div>
                <p>
              <div class="width:100%;">
                <img src="<?php bloginfo('template_url')?>/service/skylight/images/skysearchimg3.jpg?v=96c4c3a8cffb7b2a9bd6ceba3f07feb6" alt="thumb">
              </div>
              </p>
                            <p>
              <div class="ac">
                <img src="<?php bloginfo('template_url')?>/service/skylight/images/arrow2.jpg?v=3bcab068899a6c41b3f99320ae4e3ba3" alt="arrow">
              </div>
              </p>
              <p style="font-style:bold; font-size:125%; line-height: 2em; ">自然災害が多発している日本においては、安全性の確保が難しい危険な場所、人が立ち入れない狭小地や道路などが寸断されて孤立してしまった場所なども容易に撮影できるドローンの活躍に大きな期待が寄せられています。スカイサーチライトはその中でも難しいとされてきた。<span style="color:#00a0e9; border-bottom: 1px solid #00a0e9;">災害時における夜間や早朝の暗いところでの救助活動が可能</span>となります。
災害時や緊急時にソリューションの一つとして活用でき、ドローンの活躍シーンが益々拡がります。また災害対策以外にも、<span style="color:#00a0e9; border-bottom: 1px solid #00a0e9;">フェスやキャンプ場などエンターテイメント分野でも利用可能</span>です。</p>

          </article>
        </section>

        <section class="regarding">
          <article class="container">
            <div class="headBox">
              <h3>スカイサーチライト稼働時動画</h3>
            </div>

            <div class="whyIsDrones__main"><div class="video">

            <p><iframe width="836" height="440" src="https://www.youtube.com/embed/WSBpLF8LMwM?rel=0" frameborder="0" allow="autoplay; encrypted-media " allowfullscreen></iframe></p>
            </div></div>
          </article>
        </section>


          <section class="regarding">
            <article class="container">
              <div class="headBox">
                <h3>搭載機能</h3>
              </div>
              <div class="example02 mb50">
                <h3>▶︎ 夜間飛行時に機体の前後左右上下と、周囲を照らすサブ照明LED搭載</h3>
                <p>メインLED点灯と同時にサブ照明は消灯してバッテリー消費を抑えます。</p>
              </div>
              <div class="example02 mb50">
                <h3>▶︎ サーチライト用メインLED４基搭載</h3>
                <p>専用LEDドライバーの新規設計でいつでも安定した光量を発揮します。</p>
              </div>
              <div class="example02 mb50">
                <h3>▶︎ サーチライト用バッテリー残量管理機能搭載</h3>
                <p>バッテリー残量に応じてサブ照明の明滅点灯で操縦者へ知らせます。</p>
              </div>
            </article>
          </section>
        <!-- End /box Regarding -->
        <section class="regarding">
          <article class="container">
            <div class="headBox">
              <h3>スカイサーチライトのご購入について</h3>
            </div>
            <div class="whyIsDrones__main">
              <p>「スカイサーチライト」は、世界初や日本初のドローン関連商品＆サービスを提供する会員制サービス「スカイビジネス会員」に入会している方(個人、法人、自治体)限定販売となっております。</p>
            </div>
          </article>
        </section>
        <!-- End /box Regarding -->

          <!-- LESSON-->
        <section class="lessonPrice">
          <article class="container">
                <h3><i class="fa fa-bookmark" aria-hidden="true"></i> 価格：298,000円 (税別)<br>
              <span class="snote">※機体は付属しません。 </span></h3>
            <div class="lessonPrice__main">
              <div class="lessonPrice__main-list">
                <h3>スカイサーチライト</h3>


                          <div class="row">
                  <div class="postThumbTitle__thumb col-md-6">


                                <p style="color:#00a0e9; margin:0 0 10px 0; text-align:left">【セット内容】</p>
                      <ul style="list-style-type : disc;  margin:0 0 20px 20px; text-align:left">
                        <li>サーチライト用メインLED ４個</li>
                        <li>制御器本体 １個</li>
                        <li>リポバッテリー 4Sタイプ １個</li>
                        <li>取扱説明書</li>
                      </ul>

                    <p style="color:#00a0e9; margin:0 0 10px 0; text-align:left">【仕様】</p>
                      <ul style="list-style-type : disc;  margin:0 0 20px 20px; text-align:left">
                        <li>点灯時間：約13分（４灯フル点灯は 約6分、以降2灯点灯）</li>
                        <li>飛行時間：最大約13分（Inspire2のバッテリー状態により変動します）</li>
                      </ul>
                      <p class="notes" style="font-size:85%;">＜注意＞ ※本ユニットを搭載した場合、X4S以外のカメラを取り付けることができません<br>
                      ※機体のネジを外す際、T8トルクスドライバーが必要です。
                      </p>
                  </div>
                  <div class="postThumbTitle__text col-md-6">
                                <img src="<?php bloginfo('template_url')?>/service/skylight/images/skysearchimg4.jpg?v=ea01e34755d0b898c6577a1da8fd8957" class="pc" alt="thumb" style="text-align :center">
                                                          <img src="<?php bloginfo('template_url')?>/service/skylight/images/skysearchimg4.jpg?v=ea01e34755d0b898c6577a1da8fd8957" class="sp" alt="thumb" style="text-align :center">
                    <button type="button" class="btn pc" style="margin:30px 0; font-size:1.4em;"    onclick="location.href='https://dronestore-plus.com/i/dwt000002view'">ご購入はこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                                       
                                                        <button type="button" class="btn sp" style="margin:30px 0;" onclick="location.href='https://dronestore-plus.com/i/dwt000002view'">
                ご購入はこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                                                
                                 
                  </div>
                </div>

 
                </div>





                 <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>




          </article>
        </section>
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>
</body>

</html>