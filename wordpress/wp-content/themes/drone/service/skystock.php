<?php
/*
  Template Name: Sky Stock Service
 */
get_header();
?>

<body id="product">
    <!-- You write code for this content block in another file -->

    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
            <div class="container">
              <div class="banner__main">
                <h3>SKY STOCK</h3>
                <span>スカイストック</span>
              </div>
            </div>
        </section><!-- .banner // -->

          <section class="lineQualification">
            <article class="container">
              <div class="headBox">
                <h3>SKYSTOCKとは？</h3>
              </div>
              <div class="bx_top">
                <h4>アジア初、ドローン空撮素材専門のマーケットプレイス</h4>
                <p>ドローンで撮影した写真素材・動画素材を、サイト上で自由に売買することができます。<br>日本各地や海外の美しい風景や、ドローン空撮ならではのダイナミックな写真・映像が豊富！商用利用も可能です。素材をお探しの方も、ご自身の空撮素材を出品・販売したいクリエイターの方も、あらゆるシーンでご活用ください。</p>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skystock/images/img01.jpg?v=170d6d245119b0e1bdaa9c10421ac11f" alt="Images 01">
                </figure>
              </div>
              <!--/.bx_top-->
              <div class="bx_center">
                <div class="row">
                  <div class="col-sm-6">
                    <h4 class="ttl_h4"><i class="fa fa-film" aria-hidden="true"></i>素材を出品される方</h4>
                    <dl>
                      <dt>■出品するには</dt>
                      <dd>
                        <em>スカイストックに空撮した映像を出品する方や空撮映像を販売したい方は、事前にスカイビジネス会員への登録が必要になります。</em>
                      </dd>
                    </dl>
                    <dl>
                      <dt>■メリット</dt>
                      <dd>
                        <ul>
                          <li>・スカイビジネス会員登録後、出品・アップロードが誰でも簡単にできる！</li>
                          <li>・素材の小売価格は自分で設定可能！</li>
                          <li>・素材が売れた場合、販売価格の50％を成功報酬としてお支払い</li>
                        </ul>
                      </dd>
                    </dl>
                    <dl>
                      <dt>■販売までの流れ</dt>
                      <dd>
                        <ul>
                          <li>STEP<ins>1</ins>　会員登録（スカイビジネス会員）</li>
                          <li>STEP<ins>2</ins>　素材アップロード</li>
                          <li>STEP<ins>3</ins>　素材審査</li>
                          <li>STEP<ins>4</ins>　審査結果のご連絡/販売開始</li>
                          <li>STEP<ins>5</ins>　報酬のお支払い</li>
                        </ul>
                      </dd>
                    </dl>
                  </div>
					
                  <div class="col-sm-6 " style="margin-top:24px;">
                    <h4 class="ttl_h4"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 素材を購入される方 </h4>
                    <dl>
                      <dt>■購入するには</dt>
                      <dd>
                        <em>素材を購入したい方は、ドローンネット社が提供する会員制サービス(無料)への登録が必要となります。登録者は「スカイストック」サイトから制限なく購入することが可能で、好きな分だけ簡単・自由にダウンロードすることができます。</em>
                      </dd>
                    </dl>
                    <dl>
                      <dt>■メリット</dt>
                      <dd>
                        <ul>
                          <li>・無料会員登録後、年会費等は一切不要！</li>
                          <li>・４K動画からWeb用まで、豊富な素材が揃います。</li>
                          <li>・PVや番組制作など、商用利用も可能！</li>
                        </ul>
                      </dd>
                    </dl>
                    <dl>
                      <dt>■こんな用途に</dt>
                      <dd>
                        <em>企業のホームページ制作、イメージPV、商品パンフレットのイメージ画像、プレゼン資料、番組制作、CM制作、映画制作、記念行事のカタログ、年賀状やカレンダーなど。</em>
                      </dd>
                    </dl>
                  </div>
                </div>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skystock/images/img_02.jpg?v=f8d22d7bdca1adf5b02c3177ffad4b0d" alt="">
                </figure>
                <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='https://skystock.jp/'">スカイストックはこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

				  
				  </div>
              </div>
              <!--/.bx_center-->
            </article>
          </section>
          <!-- End /box LINE qualification -->
          <section class="drawing">
            <article class="container">
              <div class="headBox">
                <h3>ドローン映像出品までの道</h3>
              </div>
              <figure class="pc"><img src="<?php bloginfo('template_url')?>/service/skystock/images/img_03.jpg?v=283b2f5d37ea5f03208990e4fcd2ae18" alt=""></figure>
              <figure class="pc"><img src="<?php bloginfo('template_url')?>/service/skystock/images/img_04.jpg?v=da83d5a47b9404566a586bdec33b3d05" alt=""></figure>
				   <figure class="sp"><img src="<?php bloginfo('template_url')?>/service/skystock/images/img_03sp.jpg?v=da83d5a47b9404566a586bdec33b3d05" alt=""></figure>
            </article>
          </section>
          <!-- End /box Regarding -->
          <section class="dronnet">
            <article class="container">
              <div class="headBox">
                <h3>ドローンネット社はこのようなイベントも企画しています</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>ドローンに関する法整備は世界中でまだ始まったばかりです。<br>
                株式会社ドローンネットはドローン愛好家の皆様限定で、インストラクターが同行する安心・安全なドローン空撮体験ツアーを企画していく予定です。まるで鳥になったかのように様々な角度から風景を収めることで、ドローン空撮ならではの驚きの絶景を味わうことができるでしょう。</p>
              </div>
              <ul class="list_img">
                <li><img src="<?php bloginfo('template_url')?>/service/skystock/images/img_05.jpg?v=f28ed70575b90d0db873c2bb057ee005" alt=""></li>
                <li><img src="<?php bloginfo('template_url')?>/service/skystock/images/img_06.jpg?v=681ce87ff9da50beda953ad3f0868003" alt=""></li>
              </ul>
              <!--/.list_img-->
              <figure>
                <img src="<?php bloginfo('template_url')?>/service/skystock/images/img_07.jpg?v=ff7916d17956937768410d766b5fd3c1" alt="">
              </figure>
            </article>
          </section>
          <!-- End /box Regarding -->
          <section class="regarding">
            <article class="container">
              <div class="headBox">
                <h3>スカイストックのご利用について</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>スカイストックの出品ご利用には、「スカイビジネス会員」へのご入会が必要となります。</p>
              </div>
            </article>
          </section>
          <!-- End /box Regarding -->

 
          <section class="lessonPrice">
            <article class="container">
              <div class="lessonPrice__main">
                <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">スカイストックの費用について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="/wp-content/uploads/2018/10/skystocklogo.png" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>こ購入者は入会金、年会費など一切不要</li>
                          <li>出品者は「スカイビジネス会員であれば出品費用無料」</li>
                          <li>素材が売れた場合、販売価格の50%をお支払い</li>
                        </ul>
                                                <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://skystock.jp/'">スカイストックはこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://skystock.jp/'">スカイストックはこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>



                </div>
				  
                <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </article>
          </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>
  </html>