<?php
/*
  Template Name: Minimini Service
 */
get_header();
?>

<body id="service">
    <!-- You write code for this content block in another file -->
    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
            <div class="container">
              <div class="banner__main">
                <h3>MINIMINI DRONE</h3>
                <span>ミニミニドローン</span>
              </div>
            </div>
        </section><!-- .banner // -->
        <section class="whyIsDrones">
          <article class="container">
            <div class="headBox">
              <h3>ミニミニドローンとは？</h3>
            </div>
            <div class="whyIsDrones__main mb50">
              <p>ミニミニドローンは、重さ99g以下の小型比ドローンのことで、「マイクロドローン」「トイドローン」とも呼ばれています。
              比較的安価で操縦も簡単なことから、「ドローンに興味があるけれど、上手く飛ばせるかわからないのにいきなり10万も20万も払って購入するのはちょっと気が引ける…」そんな風に思っているドローン初心者にとってはハードルが低く、大変人気を集めています。<br>
              エンタメ系での使用も注目されており、PV撮影やドローンでの自撮りを楽しむなど、新しい文化を作りつつあります。
              </p>
            </div>
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i>ミニミニドローンで撮影した映像</h3>
            </div>
            <p>ミニミニドローンでは、小型ながらこのように臨場感たっぷりの迫力の映像を撮影することが可能です。</p>
            <div class="video">
              <iframe src="https://www.youtube.com/embed/bXzDDRFe-qg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="bx_item">
              <h4>ミニミニドローンの活用事例</h4>
              <div class="row">
                <div class="col-md-4 img_b">
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/service/minimini/images/img_01.jpg" alt="thumb">
                  </figure>
                </div>
                <div class="col-md-8">
                  <p>
                    2018年10月6日に北九州で開催された東京ガールズコレクションにて、ドローンネット社とgrid_FPV社で共同開発したミニミニドローンでの世界初のランウェイ撮影が行われました。<br>
                    ミニミニドローンで撮影されたのは、イオンカードのイメージモデルを務める人気モデルの近藤千尋さん。<br>
                    イオンシネマで流れるイオンカード劇場CMのためにランウェイの撮影を行いました。前回のランウェイでは近藤千尋さんが自撮り棒を持って、お客さんと一緒に記念写真を撮りながらランウェイを歩く様子が撮影されたので、今回は新たなチャレンジとしてミニミニドローンで撮影してみようという話になったのがきっかけです。
                  </p>
                </div>
              </div>
            </div>
            <!--/.bx_item-->
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i> 事例から見たミニミニドローンのメリット</h3>
            </div>
            <p>ミニミニドローンでは、小型ながらこのように臨場感たっぷりの迫力の映像を撮影することが可能です。</p>
            <div class="bx_item">
              <h4>メリット❶　手軽で見た目のデザイン性が高い</h4>
              <p>カメラマンがランウェイの上に上がることなく撮影が可能。小型なので見た目も可愛くデザイン性に優れています。</p>
            </div>
            <!--/.bx_item-->
            <div class="bx_item">
              <h4>メリット❷　普通では撮影できない映像を撮ることができる</h4>
              <p>カメラマンがランウェイの上に上がることなく撮影が可能。小型なので見た目も可愛くデザイン性に優れています。</p>
              <ul class="list">
                <li><span>例1</span>まるで空を飛んでいるかのように、ミニミニドローンでステージの周りをグルグル回って撮影。</li>
                <li><span>例2</span>2人のダンサーの間をすり抜け、そのまま近藤千尋さんの顔のアップを撮影。</li>
                <li><span>例3</span>急上昇して、近藤千尋さんとお客さんを頭上から撮影。</li>
              </ul>
            </div>
            <!--/.bx_item-->
            <div class="bx_item">
              <h4>メリット❸　安全性が高くイベントの妨げにならずに撮影が可能</h4>
              <p>小型ドローンなので、普通のドローンに比べて音も小さくイベントの妨げになりません。 また、プロペラにはガードが付いているため、人を傷つける可能性が非常に少ないという点も安心です。</p>
            </div>
            <!--/.bx_item-->
            <!-- <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i>東京ガールズコレクションで撮影した映像</h3>
            </div>
            <p>ミニミニドローンでは、小型ながらこのように臨場感たっぷりの迫力の映像を撮影することが可能です。</p>
            <div class="video">
              <iframe src="https://www.youtube.com/embed/bXzDDRFe-qg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div> -->
          </article>
        </section>
        <section class="whyIsDrones">
          <article class="container">
            <div class="headBox">
              <h3 class="pc">ドローンネット社オリジナルミニミニドローン開発の経緯</h3>
              <h3 class="sp">ドローンネット社オリジナル<br>ミニミニドローン開発の経緯</h3>
            </div>
            <div class="whyIsDrones__main mb50">
              <p>ドローンネット社オリジナルミニミニドローンは、小型のドローンでハイビジョン以上の画質で撮影したいという思いがきっかけで開発を始めました。2018年の頭に開発することに成功し、屋内での飛行や、これまで撮影が難しかった場所でも高画質で撮影ができるようになりました。<br>軽量で柔らかく、人を傷つける可能性が非常に少ないという安全性に優れている点は重要な利点だと考えています。
              </p>
            </div>
          </article>
        </section>
        <section class="whyIsDrones">
          <article class="container">
            <div class="headBox">
              <h3 class="pc">ミニミニドローンの今後の展望</h3>
              <h3 class="sp" style="font-size:1.8em;">ミニミニドローンの今後の展望</h3>
            </div>
            <div class="whyIsDrones__main mb50">
              <p>ミニミニドローンは、すでに映像業界での採用が着々と始まっており、イベントでの活用はより広まっていくことが期待されます。今後は災害時、警備、点検などのエンターテイメント業界以外での産業における活用も進むでしょう。
              </p>
              <p>軽量のため持ち運びも便利で、かつ操縦も簡単、その場でスマホで映像確認ができることから、旅行先などでの自撮りを目的とした、一般利用における活用方法もあります。<br>まさにミニミニドローンを使った新しい自撮り文化が生まれるといっても過言ではありません。
              </p>
              <p>現在、ドローンネット社ではお客様の上でも絶対に落ちないシステムで飛行可能なミニミニドローンも開発中で、近日発表予定です。イベント上空でもさらに安全に飛行撮影ができるようになり、今後の活用がますます期待されます。
              </p>
              <ul class="list_img">
                <li><img src="<?php bloginfo('template_url')?>/service/minimini/images/img_02.jpg" alt=""></li>
                <li><img src="<?php bloginfo('template_url')?>/service/minimini/images/img_03.jpg" alt=""></li>
              </ul>
            </div>
          </article>
        </section>
        <section class="lessonPrice bx_bottom">
          <article class="container">
            <div class="lessonPrice__main-list">
              <h3 class="pc">ミニミニドローン、近日発売予定！</h3>
              <h3 class="sp" style="font-size:1.5em;">ミニミニドローン、近日発売予定！</h3>
              <div class="boxMore">
                <button type="button" class="btn"  onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
              </div>
            </div>
          </article>
        </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>

  </html>