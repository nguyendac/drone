<?php
/*
  Template Name: Flight certfication Service
 */
get_header();
?>


<body>
    <!-- You write code for this content block in another file -->
    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
          <div class="container">
            <div class="banner__main">
              <h3>FLIGHT CERTIFICATION</h3>
              <span>10時間飛行証明</span>
            </div>
          </div>
        </section><!-- .banner // -->
        <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>ドローンを合法的に外で自由に飛ばせるようになるために！</h3>
              <span>ドローン ザ ワールドスクール<br>10時間飛行証明コース</span>
            </div>
            <div class="thumb">
              <img src="<?php bloginfo('template_url')?>/service/flightcertification/images/certi_01.png?v=a390fda00fe6927c81a37994e9ddc47d" alt="スカイサーチライトロゴ">
            </div>
            <div class="postThumb certi_02">
              <div class="row">
                <h3>ドローン ザ ワールドスクール10時間飛行証明コースとは</h3>
                <p>機体重量200g以上のドローンを屋外で操縦する場合、次のような規制があります。</p>
                <div class="postThumb__text col-md-6">
                  <h4>【次の空域での飛行は禁止】</h4>
                  <ul>
                    <li>・空港周辺</li>
                    <li>・150M以上の上空</li>
                    <li>・人家の集中地域</li>
                  </ul>
                  <h4>【飛行の方法については次の事項を守ること】</h4>
                  <ul>
                    <li>・日中（日出から日没まで）に飛行させること</li>
                    <li>・目視の範囲内で飛行させること</li>
                    <li>・建物や人などとの距離の確保を行う</li>
                    <li>・催し場所など人が集まる場所で飛行させないこと</li>
                    <li>・危険物などの輸送は行わないこと</li>
                    <li>・物を投下しないこと</li>
                  </ul>
                </div>
                <div class="postThumb__thumb col-md-6 ">
                  <img src="<?php bloginfo('template_url')?>/service/flightcertification/images/certi_02.png?v=a3d02d634f79a7abacb3693b270a10b4" alt="thumb">
                </div>
                <p>ドローン飛行の際には厳しい規制が設けられていますが、「夜間に飛行させたい」「目視外で飛行させる必要がある」「人口集中地域で飛行させたい」といったように上記のルール外でドローン飛行を行いたい場合、<span>国土交通省の手続きを経て、「包括飛行許可」という承認を得る必要があります。</span></p>
              </div>
            </div>
            <div class="certi_03">
              <figure>
                <img src="<?php bloginfo('template_url')?>/service/flightcertification/images/certi_03.png?v=12286aaf77c6945882126f1c8ac7a513" alt="">
                <img src="<?php bloginfo('template_url')?>/service/flightcertification/images/arrow_down.png?v=2f0dfe01c5c69e9bd9cd8d65d14ee950" alt="">
              </figure>
            </div>
          </article>
        </section>
        <section class="whyIsDrones certi_04">
          <article class="container">
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i>国交省に申請するのにも"条件"があります！</h3>
            </div>
          </article>
        </section>
        <section>
          <article class="container">
            <div class="headBox">
              <h3>包括飛行許可の申請を行う際には<br>「10時間飛行」の実績が必須</h3>
            </div>
            <p>包括飛行許可の申請を国土交通省に行う際には、「一定以上のドローンの操縦技術を持っていること」「航空法の正しい知識を持っていること」など、さまざまな条件が必要となりますが、そのなかでも特に重要なのが「10時間以上の飛行実績があること」という条件です。<br>「10時間の飛行であれば、独学で何とかなるのでは？」と思うかもしれませんが、実際には容易なことではありません。</p>
            <p>航空法やドローンの飛行に関する正しい知識を習得したうえで、ホバリングをはじめ八の字飛行や遠隔でのの離着陸などの技術を身に着けるには、しっかりと講習を受けることが必要です。</p>
          </article>
        </section>
        <section class="whyIsDrones">
          <article class="container">
            <div class="headBox">
              <h3>ドローン ザ ワールドスクール<br>【10時間飛行証明コース】の概要</h3>
            </div>
          </article>
        </section>
        <section class="lessonPrice">
          <article class="container">
            <h3><i class="fa fa-bookmark"></i> 価格：110,000円 (税別) <br><span class="snote">※スカイビジネス会員様限定のコースとなります。</span></h3>
            <div class="lessonPrice__main">
              <div class="example">
                <h3>▶︎ 航空法や簡単な機体構造などについての座学</h3>
                <h3>▶︎ ゴーグルを着けて丸ゲートをくぐったり８の字飛行する実技訓練</h3>
                <h3>▶︎ ドローン搭載カメラで上空撮影訓練</h3><br>
                <h3>▶︎ 障害物回避訓練・目視外飛行訓練</h3>
                <h3>▶︎ レッスンで使用するドローンやゴーグルの貸し出しは無料</h3>
              </div>
              <figure class="customCerti"><img src="<?php bloginfo('template_url')?>/service/flightcertification/images/certi_04.png?v=a25651936ad110e9382f93917db73f18" alt=""><img src="<?php bloginfo('template_url')?>/service/flightcertification/images/certi_05.png" alt=""></figure>
              <h3>ドローン ザ ワールドスクール 【10時間飛行証明コース】の受講料</h3>
              <div class="postThumb custompostThumb">
                <figure><img src="<?php bloginfo('template_url')?>/service/flightcertification/images/certi_06.png?v=dd68c0178bf831290cf6d37542473fd1" alt=""></figure>
                <p><span>※10日間飛行証明コースは、スカイビジネス会員様限定のコースとなります。</span></p>
              </div>
              <div class="postThumb">
                <figure><img src="<?php bloginfo('template_url')?>/service/flightcertification/images/certi_07.png?v=862254cf64ff98c269f6eddf82aeac32" alt=""></figure>
              </div>
              <button type="button" class="btn" style="margin:30px 0;">コースの詳細へ  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
            <div class="lessonPrice__main">
                  <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onClick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
          </article>
        </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>

  </html>