<?php
/*
  Template Name: Controller Service
 */
get_header();
?>

<body id="service">
    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
            <div class="container">
              <div class="banner__main">
                <h3>DRONE CONTROLLER</h3>
                <span>ドローン管制士 3級</span>
              </div>
            </div>
        </section><!-- .banner // -->

          <section class="lineQualification">
            <article class="container">
              <div class="headBox pc">
                <h3>注目の資格！<br>これから需要が高まる職業「ドローン管制士 3級」とは？</h3>
              </div>
              <div class="headBox sp">
                <h3>注目の資格！<br>これから需要が高まる職業「ドローン管制士 3級」とは？</h3>
              </div>
              <div class="postThumb">
                <div class="row">
                  <div class="postThumb__text col-md-7">
                    <h3>ドローン管制士とは？</h3>
                    <p>ドローン管制士は、ドローンの運航管理やドローンパイロットのディレクションを遠隔から行うという役割を果たす、今最も注目されている職業のひとつです。<br>複数のモニターを通してドローンを操縦しているパイロットに的確な指示を出すスキルが要されるため、ドローン管制士になるには資格が必要となります。</p>
                  </div>
                  <div class="postThumb__thumb col-md-5" style="text-align: center;">
                    <img src="<?php bloginfo('template_url')?>/service/controller/images/dronecontrol.jpg?v=ffecf01e962a13d74a86684d000ea885" alt="thumb">
                  </div>
                </div>
              </div>
            </article>
          </section>
          <!-- End /box LINE qualification -->
          <section class="regarding">
            <article class="container">
              <div class="headBox">
                <h3>ドローン管制士の高まるニーズと役割</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>近年、日本は目覚ましい産業の発展を遂げてきました。AI、ロボティクス、IoTなどに並んで、ドローンの需要も飛躍的に高まりました。一方で、EC率の上昇により宅配個数も上昇し、その数は2010年には32億２千万個、2016年には40億2千万個と6年で8億個も増加していますが、再配達率は20％にまで達するという大きな課題を抱えている現状があります。<br>今後は多様な手段を駆使し、シームレスで高度な物流システムが求められることは間違いありません。</p>
              </div>
              <div class="thumb">
                <div class="pc"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi01.jpg?v=28f9b43452bf2ee02fe8f7bcd4f0f490" alt="thumb"></div>
                <div class="sp"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi01sp.jpg?v=f7b8b7e62070670e494a4c284a54632c" alt="thumb"></div>
              </div>
              <p>物流におけるドローンの活用は、今後劇的に需要が高まるといえます。<br /><br /><span class="orange">2018年にはドローンによる離島・山間部への荷物配送が解禁され、2020年以降にはデリバリーの定着化によって都市部での荷物輸送が解禁されます。</span>荷物配送以外だと、橋などのインフラ管理もドローン活用が一般化されるでしょう。つまり、ドローンの目視外飛行が解禁されるというわけです。</p>
              <div class="thumb">
                <div class="pc"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi02.jpg?v=dd678212e64cf48e62db518771bde66c" alt="thumb"></div>
                <div class="sp"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi02sp.jpg?v=9ad0ea4ffb1aeb6d4f9dc2de64d07501" alt="thumb"></div>
              </div>
            </article>
          </section>
          <section class="whyIsDrones">
            <article class="container">
              <div class="headBox">
                <h3>ドローン管制士が解決できること</h3>
              </div>
              <div class="whyIsDrones__main mb50">
                <p>このように、ドローンの活躍には目覚ましいものがありますが、ドローン操作によってなされるビジネスには、「安全性の担保ができない」「業務管理の効率が低い」「現場が見えない」「ガバナンス体制の不備」といった課題がありました。<br>そこで、ドローン管制士のポジションを加えることで、<span class="strong">フライト状況の監視や操縦士への指示、操縦士の法令違反や業務怠慢などの抑制といったさまざまな視点からのメリット</span>が生まれることになります。<br>従来の操縦士のみのドローン活用では解決できなかった課題を、ドローン管制士のはたらきにおいてその課題は解決されるのです。</p>
              </div>
              <div class="thumb">
                <div class="ac mb50"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi03.jpg?v=3671654db7dfd1c56b0e4e77b916d88f" alt="thumb"></div>
                <div class="ac mb50"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi04.jpg?v=95bc36b92341da4e0ef4e31a03e6ebff" alt="thumb"></div>
              </div>
            </article>
          </section>
          <section class="whyIsDrones">
            <article class="container">
              <div class="headBox mb50">
                <h3>ドローン管制士が使うマストアイテムとは</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>ドローン管制士は、具体的にどのように操縦士に指示を伝えるのでしょうか。ドローン管制士は、<span class="orange">「フューチャードロー」</span>というアプリ機能を用いて操縦士に指示を出します。遠隔地にいる操縦士に、簡潔にかつ的確に指示を伝えるためには、このフューチャードローは必要不可欠なアイテムといえます。</p>
              </div>
              <div class="postThumbTitle">
                <h3><i class="fa fa-check" aria-hidden="true"></i> フューチャードローとは</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>ドローン管制士は、具体的にどのように操縦士に指示を伝えるのでしょうか。ドローン管制士は、複数のドローンパイロットの手元タブレット上の地図に、特定のドローン管制士がリアルタイムで自由にマーキングできるアプリ機能です。ドローン管制士がオフィスのパソコンから「フューチャードロー」で画面地図上に矢印を描くと、相手のドローンパイロットの手元にあるタブレット上の地図にも同時に同じ矢印が現れます。それにより、現場のパイロットに対して、ドローンで探索して欲しいエリアの指示や道先案内などが、現場に行かなくても遠隔でオフィスからリアルタイムに行えます。<br>「フューチャードロー」は、空撮映像のインターネットライブ配信システム「ドローンスコープ」内のアプリ機能になります。</p>
                <p class="note">※ドローンスコープとは、日本初となる空撮映像のインターネットライブ配信システムです。基本的に従来の市販のドローンでは、数ｋｍ以上先に空撮映像をライブ配信することが非常に困難でしたが、ドローンスコープがあれば、沖縄で空撮中の映像を北海道に簡単にライブ配信できます。さらにドローンスコープは大掛かりな中継機材を一切使わず、低コストで複数のドローンから複数のパソコンやスマホへ同時にライブ配信が可能です。また、各ドローンの位置情報もライブ映像と共に配信(複数の場所へ配信可)されるので、複数機の運航管理も行える多機能なシステムです。</p>
              </div>
              <div class="postThumbTitle">
                <h3><i class="fa fa-check" aria-hidden="true"></i> フューチャードロー使用例</h3>
              </div>
              <div class="example">
                <div class="thumb">
                  <h3>▶︎使用例① 単純移動指示の場合</h3>
                  <div class="pc"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi05.jpg?v=6fe7f3c9786308530aba1bb58e1cd119" alt="thumb"></div>
                  <div class="sp"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi05sp.jpg?v=ceca3b9532ee0795470b764ea1fbcf9e" alt="thumb"></div>
                </div>
                <div class="thumb">
                  <h3>▶︎使用例② メガソーラー点検箇所指定の場合</h3>
                  <div class="pc"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi06.jpg?v=1a1b953b2583de48f51f9c2288120e4f" alt="thumb"></div>
                  <div class="sp"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi06sp.jpg?v=6bdd60ee5df0203a94ae9188cae6f7a6" alt="thumb"></div>
                </div>
                <div class="thumb">
                  <h3>▶︎使用例③ 空撮場所指定の場合</h3>
                  <div class="pc"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi07.jpg?v=51624c10e0df2e23596f09e4c631497c" alt="thumb"></div>
                  <div class="sp"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi07sp.jpg?v=82ef125f9ac5f1aa07dbb2dbb67cb508" alt="thumb"></div>
                </div>
                <div class="thumb">
                  <h3>▶︎使用例④ 巡回警備エリア指定の場合</h3>
                  <div class="pc"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi08.jpg?v=480d0b4b86a336aa997269e448a08f17" alt="thumb"></div>
                  <div class="sp"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi08sp.jpg?v=c117406f18fb9b28ce42ce70328844b6" alt="thumb"></div>
                </div>
                <div class="thumb">
                  <h3>▶︎使用例⑤ 飛行禁止エリア告知の場合</h3>
                  <div class="pc"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi09.jpg?v=8c4f6fd728dfe99badba4179658c9e3e" alt="thumb"></div>
                  <div class="sp"><img src="<?php bloginfo('template_url')?>/service/controller/images/kanseishi09sp.jpg?v=9cacc18e3e75e9416e2ab08e3940bd20" alt="thumb"></div>
                </div>
              </div>
            </article>
          </section>

          <section class="lineQualification mb30">
            <article class="container">
                <div class="postThumb ">
                  <div class="row">
                    <div class="postThumb__text col-md-12">
                      <p>「ドローン管制士 ３級」を習得するための講習では、空撮現場にいるドローン操縦士に対して実際にこの「フューチャードロー」を使い、フライトプランの変更指示やミッションの追加を行うなど、ディレクションスキルを習得するためのレッスンを行っており、ハイレベルで実践的な授業内容となっております。また、目視外飛行の際の補助者にも必要なスキルが学べます。</p>
                    </div>
                  </div>
                </div>
            </article>
          </section>
          <section class="whyIsDrones">
            <article class="container">
              <div class="headBox">
                <h3>「ドローン管制士 3級」の受講について</h3>
              </div>
              <div class="lineQualification__main">
                <p>ドローン管制士 ３級は、世界初や業界初のドローン関連商品＆サービスを提供する会員制サービス「スカイビジネス会員」に入会している方(個人、法人、自治体)限定で受講可能となっております。</p>
              </div>
            </article>
          </section>
          <!-- End /box Regarding -->

          <section class="lessonPrice">
            <article class="container">
              <h3><i class="fa fa-bookmark" aria-hidden="true"></i> 受講価格：50,000円 (税別)</h3>
              <div class="lessonPrice__main">
                <div class="lessonPrice__main-list">
                  <h3>「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/controller/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="lessonPrice__main-postThumbbg">
                    <div class="row">
                      <div class="textBox col-md-7 col-6">
                        <p>ドローン管制士 ３級を受講し合格された方には、「ドローン管制士 ３級 資格証」が発行されます<br>(有効期限5年/民間資格)</p>
                      </div>
                      <div class="thumb col-md-5 col-6">
                        <div class="mainBox">
                          <img src="<?php bloginfo('template_url')?>/service/controller/images/lessonPrice2.png?v=8b44c60348e2ba88438bd511a73dd954" alt="thumb">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="lessonPrice__main-list">
                  <h3>受講できる店舗/スクール一覧</h3>
                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                    <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </article>
          </section>
          <!-- End /box Lesson price -->
      </main>

      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
      <div id="overlay" class="overlay"></div>
    </div>
    <?php get_footer();?>
  </body>
</html>