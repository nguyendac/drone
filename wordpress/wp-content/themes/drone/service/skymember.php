<?php
/*
  Template Name: Sky Business Member Service
 */
get_header();
?>

<body>
    <!-- You write code for this content block in another file -->
    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
          <div class="container">
            <div class="banner__main">
              <h3>WORLD DRONE MARKET</h3>
              <span>ワールドドローンマーケット</span>
            </div>
          </div>
        </section><!-- .banner // -->
        <section class="whyIsDrones">
          <article class="container">
            <div class="headBox">
              <h3> ワールドドローンマーケットとは</h3>
            </div>
            <div class="whyIsDrones__main">
              <p>ドローン含むIoT関連テクノロジーが人類に貢献する役割は年々増加し、その市場規模は2020年に200兆円を超え、2030年には325兆円に達するといわれています。これからの時代は、ドローンの活用と技術の向上により注目していくことが重要となります。</p>
            </div>
            <div class="bx_doro">
              <div class="row">
                <div class="col-md-7 left">
                  <h4>ドローンはIT革命の「第４の波」</h4>
                  <p>1983年のパソコン普及によるIT革命の第１の波に<br>
                  はじまり、1993年のインターネットやメールアドレス等の急速な普及で生じた第２の波、2003年のスマートフォンの登場でモバイル革命ともいわれた第３の波と、IoTの市場は目覚ましい成長を遂げてきました。
                  そして2019年は、第４の波となるドローンによる「空の産業革命」が起こると予測されています。</p>
                  <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_fly.jpg" alt=""></figure>
                </div>
                <div class="col-md-5 right">
                  <span>200兆円</span>
                  <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_01.jpg" alt=""></figure>
                </div>
              </div>
            </div>
            
            <!--/.bx_doro-->
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i> ドローン都市構想とは？</h3>
            </div>
            <p>世界中で需要が高まっているドローン市場。今、あらゆる場所にドローンを配備し、先端テクノロジーに守られた〈防災〉〈警備〉都市を築くというドローン都市構想が計画されています。</p>
            <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_02.jpg" alt=""></figure>
            <div class="bx_san">
              <div class="row">
                <div class="col-md-4 col_item">
                  <h4>防災</h4>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_art_01.jpg" alt="">
                  </figure>
                  <p>ドローンが災害現場上空を移動しながら４Kカメラや赤外線カメラで撮影・録画します。その詳細画像を防災センターやメディアへ発信することで、住民の安否や災害状況をリアルタイムで確認でき、迅速な初動対応や復旧計画策定につなげることが可能になります。また、災害時に陸路やヘリコプターで行けないような危険地域に救援物資を届けることもできます。</p>
                </div>
                <div class="col-md-4 col_item">
                  <h4>警備</h4>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_art_02.jpg" alt="">
                  </figure>
                </div>
                <div class="col-md-4 col_item">
                  <h4>ロジティクス</h4>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_art_03.jpg" alt="">
                  </figure>
                </div>
              </div>
            </div>
            
            <!--/.bx_san-->
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i>ドローン活用対象となる土地や建物の数はどれくらい？</h3>
            </div>
            <div class="bx_info">
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_01.jpg" alt="thumb">
                  <figcaption>
                    太陽光パネル
                    <span>28億枚</span>
                  </figcaption>
                </figure>
              </div>
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_02.jpg" alt="thumb">
                  <figcaption>
                    超高層ビルの数
                    <span>91,837棟</span>
                  </figcaption>
                </figure>
              </div>
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_03.jpg" alt="thumb">
                  <figcaption>
                    農業耕地面積
                    <span>1,730㎞/㎡ </span>
                  </figcaption>
                </figure>
              </div>
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_04.jpg" alt="thumb">
                  <figcaption>
                    世界遺産総数
                    <span>1,092件</span>
                  </figcaption>
                </figure>
              </div>
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_05.jpg" alt="thumb">
                  <figcaption>
                    海岸線の長さ
                    <span>783,724㎞</span>
                  </figcaption>
                </figure>
              </div>
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_06.jpg" alt="thumb">
                  <figcaption>
                    大規模災害件数
                    <span>年間371件</span>
                  </figcaption>
                </figure>
              </div>
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_07.jpg" alt="thumb">
                  <figcaption>
                    ダム
                    <span>80万基</span>
                  </figcaption>
                </figure>
              </div>
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_08.jpg" alt="thumb">
                  <figcaption>
                    世帯数
                    <span>約15億<br>8,600万世帯</span>
                  </figcaption>
                </figure>
              </div>
              <div class="item">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/img_b_09.jpg" alt="thumb">
                  <figcaption>
                    スタジアム数
                    <span>4,767施設</span>
                  </figcaption>
                </figure>
              </div>
            </div>
            <!--/.bx_info-->
          </article>
        </section>
        <section class="lineQualification st_arr">
          <article class="container">
            <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/arr_r.png" alt=""></figure>
            <h3>これほどの数の施設や建物が、ドローン活用によって活性化が期待される場所として存在しています。同時に、ドローンビジネスの参入もまた、急激に高まっていくといえるでしょう。</h3>
          </article>
        </section>
        <section class="lineQualification">
          <article class="container">
            <div class="postThumb mb30">
              <h3>ドローンネット社の「スカイビジネス会員」とは？</h3>
              <div class="row custom02">
                <div class="textBox col-7">
                  <p>スカイビジネス会員は、ドローンの操縦技術向上や知識習得、作業の効率化や、コスト削減など、多くの面で有益な効果を発揮するさまざまなオリジナル便利ツールやサービスを必要に応じて自由に利用することができる会員制サービスです。</p>
                  <p>スカイビジネス会員限定のオリジナルコンテンツを活用することで、ドローンのビジネス活用の可能性を拡げることが可能となるため、ドローン需要が高まる現代において、「ドローンで何か事業を行いたい」「仕事にドローンを使いたい」「ドローンで作業の効率化を図りたい」と思っている方に非常におすすめのサービスです。</p>
                </div>
                <div class="thumb col-5  mb30">
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/business_01.png" alt="">
                </div>
                
              </div>
            </div>

                               <div class="boxMore mt20">
                <button type="button" class="btn"     onclick="location.href='https://drone-the-world.com/entry'">入会フォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
              </div>
          </article>
        </section>

        <section class="whyIsDrones customwhyIsDrones">
          <article class="container">
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i> オリジナルサービス</h3>
            </div>
            <div class="code01">
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_01.png" alt=""></figure>
                <div class="textBox">
                  <p><span>アジア初</span><em>▶︎ スカイストック</em></p>
                  <h4>ドローン空撮素材専門の売買マーケットプレイス</h4>
                  <p>空撮素材を購入・出品することが可能。出品者の報酬は販売額の50%</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_02.png" alt=""></figure>
                <div class="textBox">
                  <p><span>世界初</span><em>▶︎ドローンスコープ</em></p>
                  <h4>目視外ドローン視点映像生中継&操縦ナビゲートができるドローン管制アプリ</h4>
                  <p>ドローンの業務依頼者が現場に行かずとも、リアルタイムで現場の監督をし、操縦士(複数可)と同じ画面を見ながらコミュニケーションできる 管制アプリが使用可能。</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_03.png" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ スカイクラウド</em></p>
                  <h4>ドローンを使った業務の依頼者と操縦士のマッチングシステム</h4>
                  <p>ドローン操縦士は登録することによりドローンを使用する業務を受注することが可能。また、依頼企業は登録しているドローン操縦士に直接仕事の依頼や交渉をすることが可能。</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_04.png" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ ドローン官制士</em></p>
                  <h4>ドローン操縦士を遠隔地からナビゲートする時代の最先端の職業</h4>
                  <p>ドローンスコープを駆使しながら、目視外飛行中の複数ドローンの”指揮監督術”を習得することができる『ドローン官制士』の講座が受講可能。</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_05.png" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ スカイイージー</em></p>
                  <h4>ドローン業務に必要な飛行申請書類を簡単スムーズに作成できるアシストソフトウェア</h4>
                  <p>ドローンパイロットの支援システムとしてパソコンで簡単に利用でき、確実性と利便性に注力した有益で画期的なサービスが使用可能。</p>
                </div>
              </div>
              <!--<div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_06.png" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ ドローン中継士</em></p>
                  <h4>現場の様子をリアルタイムで生中継するドローンパイロットの最先端スキル資格</h4>
                  <p>ドローンスコープを駆使して、１分１秒を争う災害現場の状況把握やスポーツ観戦などの生中継を行うことができる『ドローン中継士3級』の資格取得講座が受講可能。</p>
                </div>
              </div>-->
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_07.png" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ ドローンストア</em></p>
                  <h4>オリジナルグッズを販売する会員限定ECサイト</h4>
                  <p>ドローンネット社オリジナルグッズが購入可能。</p>
                </div>
              </div>
              <!--<div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_08.png" alt=""></figure>
                <div class="textBox">
                  <p><span>世界初</span><em>▶︎ フューチャードロー</em></p>
                  <h4>ドローンパイロット側の手元タブレット上の地図に遠隔で特定の第三者が自由にお絵描きできるアプリ</h4>
                  <p>ドローンスコープを使用し、オフィスのパソコンからドローン飛行の道先案内や、探索エリアの限定を地図にお絵描きして指示することができるアプリの使用が可能。</p>
                </div>
              </div>-->
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_09.png" alt=""></figure>
                <div class="textBox">
                  <p><em>▶︎ AIチャットボット</em></p>
                  <h4>AIチャットボット</h4>
                  <p>ドローンについての質問にも答えてくれるAIチャットボットと会話することが可能。</p>
                </div>
              </div>
              
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_10.png" alt=""></figure>
                <div class="textBox">
                 <p><em>▶︎ 会員限定イベント</em></p>
                  <h4>スカイビジネス会員限定のイベント</h4>
                  <p>ドローンビーナスをゲストに招いたドローンイベントや、ドローンと一緒にラウンドができるゴルフイベントなどに参加可能。</p>
                </div>
              </div>
            </div>
          </article>
        </section>
        <section class="whyIsDrones customwhyIsDrones">
          <article class="container">
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i> オリジナルプロダクト</h3>
            </div>
            <div class="code01">
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_15.png" alt=""></figure>
                <div class="textBox">
                  <p><span>アジア初</span><em>▶︎ デュアルサーチ</em></p>
                  <h4>アジア最軽量の小型ドローンへ簡単に装着可能なサーモグラフィカメラ装置</h4>
                  <p>約３０gのサーモグラフィカメラを、ユーザーが自分でドローンに取り付けて搭載できる装置が購入可能！%</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_13.png" alt=""></figure>
                <div class="textBox">
                  <p><em>▶︎ スカイツインズ</em></p>
                  <h4>ドローン操縦用コントローラーをツインモニター化するための便利グッズ</h4>
                  <p>ドローンコントローラーに２個目のモニターを、ユーザーが自分で取り付けて装着できる便利グッズが購入可能！</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/icon_14.png" alt=""></figure>
                <div class="textBox">
                  <p><span>アジア初</span><em>▶︎ スカイライフジャケット</em></p>
                  <h4>ドローンにライフジャケットを装着して投下する装置</h4>
                  <p>アームの動作でドローン自らがライフジャケットを投下することができる装置が購入可能!</p>
                </div>
              </div>
                          <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_15.png?v=47becc158f321fb546aa6210a9939623" alt=""></figure>
                <div class="textBox">
                  <p><span>アジア初</span><em>▶︎ スカイサーチライト</em></p>
                  <h4>強力LED照明で夜間の飛行をサポート</h4>
                  <p>機体のネジを交換して取り付けることが可能な、Inspire2専用設計のLEDサーチライトが購入可能！</p>
                </div>
              </div>
            </div>
          </article>
        </section>
        <section class="lessonPrice">
          <article class="container">
            <div class="lessonPrice__main">
                  <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店 </a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>

          </article>
        </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>

  </html>