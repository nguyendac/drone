<?php
/*
  Template Name: Sky Cloud Service
 */
get_header();
?>

<body id="service">
    <!-- You write code for this content block in another file -->
    
    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>

<!-- /header -->

<section class="banner">
    <div class="container">
    	<div class="banner__main">
    		<h3>SKY CLOUD</h3>
    		<span>スカイクラウド</span>
    	</div>
    </div>
</section><!-- .banner // -->

	<section class="lineQualification">
		<article class="container">
			<div class="headBox pc">
				<h3>日本初の職業となるドローン管制士等のドローン人材と、<br>ドローン業務依頼者のマッチングをサポートする、コミュニケーションサイト</h3>
				<span>～業務成立後の当サイト運営側の成功報酬０円！～</span>
			</div>
			<div class="headBox sp">
				<h3>日本初の職業となるドローン管制士等のドローン人材と、ドローン業務依頼者のマッチングをサポートする、コミュニケーションサイト</h3>
				<span>～業務成立後の当サイト運営側の成功報酬０円！～</span>
			</div>
			  <div class="mb50">
                <figure class="img_top">
                <img src="<?php bloginfo('template_url')?>/service/skycloud/images/img_top.jpg?v=8e28e4106355d8675ce0c6a233df1ffe" alt="">
              </figure>
              </div>
              
             <div class="postThumb">
                <div class="row">
                  <div class="postThumb__text col-md-7-2">
                    <h3>【スカイクラウドが開発された経緯】</h3>
                    <p>近年、ドローンは機能も大幅に進化し、建物点検事業や土木測量事業、農業、災害救助などの様々な分野で活用されるようになりました。<br>当社は、「ドローンを業務に使いたい」という企業や自治体が増えていることに着目し、ドローン業務依頼者が安心してドローン業務をドローン人材に発注できるしくみと、使いやすく便利なサービス作りとを実現しました。</p>
                  </div>
                </div>
              </div>
		</article>
	</section>
	<!-- End /box LINE qualification -->


	<section class="whyIsDrones">
		<article class="container">
	
			<div class="postThumbTitle">
					<h3><i class="fa fa-check" aria-hidden="true"></i> 「スカイクラウド」の特長やポイント</h3>	
			</div>

			  <div class="example">		
				<div class="thumb">
				<h3 style="text-align: left;">①「スカイクラウド」は、スカイビジネス会員であればパソコンやスマートフォンから無料で簡単にご利用できます。</h3>
					<div class="mb30"><img src="<?php bloginfo('template_url')?>/service/skycloud/images/skycloud02.png" alt="thumb"></div>			
				</div>
			 </div>
			 <div class="example">		
				<div class="thumb">
				<h3 style="text-align: left;">②「スカイクラウド」に登録したドローン人材は、仕事のオファーが来る＆自らエントリーすることも可能。依頼者は豊富なドローン人材の中から仕事を依頼することができます。</h3>
					<div class="mb30"><img src="<?php bloginfo('template_url')?>/service/skycloud/images/skycloud03.png" alt="thumb"></div>			
				</div>
			 </div>
			  <div class="example">		
				<div class="thumb">
				<h3 style="text-align: left;">③専用のコミュニケーションツール（無料）を使うことで、ドローン業務の受発注におけるやり取りが簡素化され、ドローン業務成立までの作業効率が上がり、進捗管理も行いやすくなります。</h3>
					<div class="mb30 pc"><img src="<?php bloginfo('template_url')?>/service/skycloud/images/skycloud05.png" alt="thumb"></div>	
					<div class="mb30 sp"><img src="<?php bloginfo('template_url')?>/service/skycloud/images/skycloud05sp.png" alt="thumb"></div>			
	
				</div>
			 </div>
			 <div class="example">		
				<div class="thumb">
				<h3 style="text-align: left;">④ドローン業務依頼者とドローン人材のマッチングや受発注が成立しても、当サイト運営側の成功報酬はいただいておりません！</h3>
					<div class="mb30 "><img src="<?php bloginfo('template_url')?>/service/skycloud/images/skycloud04.png" alt="thumb"></div>			
				</div>
			 </div>
		</article>
	</section>
			
			
	<section class="whyIsDrones">
            <article class="container art_cts">
              <div class="postThumbTitle">
                <h3><i class="fa fa-check" aria-hidden="true"></i> スカイクラウドはこんなところで活躍します</h3>
              </div>
                <p>イベントやPVの空撮、太陽光パネル点検、建物の点検、ダムなど大型建築物の点検、発電所の点検、農薬散布、教育、災害対策、警備・監視、土木測量、野生動物調査・監視など</p>
                <ul class="list_img">
                 <li><img src="<?php bloginfo('template_url')?>/service/skycloud/images/img_01.jpg?v=21ba58ac09e1a5a202f0690ccc9ba7fc" alt=""></li>
                 <li><img src="<?php bloginfo('template_url')?>/service/skycloud/images/img_02.jpg?v=f33bd1cfb584356f952e0c26e043cf98" alt=""></li>
                 <li><img src="<?php bloginfo('template_url')?>/service/skycloud/images/img_03.jpg?v=1962a4b029356876e23010ec86993879" alt=""></li>
               </ul>
               <p class="video"><iframe width="836" height="440" src="https://www.youtube.com/embed/mqKSGJFi5Q0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></p>
               <div class="boxMore">
                <button type="button" class="btn" onclick="location.href='https://skycloud-japan.com/'">スカイクラウドはこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

              </div>
            </article>
          </section>
          <section class="regarding">
            <article class="container">
              <div class="headBox">
                <h3>スカイクラウドのご利用について</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>「スカイクラウド」は、世界初や日本初のドローン関連商品＆サービスを提供する会員制サービス「<strong>スカイビジネス会員」に入会している方(個人、法人、自治体)限定サービスとなっております。</strong></p>
              </div>
            </article>
          </section>
          <!-- End /box Regarding -->

          <section class="lessonPrice">
            <article class="container">
              <div class="lessonPrice__main">
                <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">スカイクラウドの費用について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skycloud/images/skyclond.png" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>仕事完了後に、業務依頼者が各ドローン人材へ報酬を支払う</li>
                          <li>当サイト運営側への業務成立後の成功報酬無料</li>
                          <li>依頼者と各ドローン人材との直接コミュニケーション料無料</li>
                        </ul>
                                                <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://skycloud-japan.com/'">スカイクラウドはこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://skycloud-japan.com/'">スカイクラウドはこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>



                </div>
				  
                <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </article>
          </section>
      </main>
		
		
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>

  </html>