window.addEventListener('DOMContentLoaded', function() {
  new Tab();
});
var Tab = (function(){
  function Tab(){
    // showArticle(0);
    var t = this;
    this._target = document.querySelectorAll('.tabs');
    Array.prototype.forEach.call(this._target,function(el,i){
      var _eles = el.querySelectorAll('a');
      Array.prototype.forEach.call(_eles,function(c){
        showArticle(c,0);
        c.addEventListener('click',function(e){
          if(this.dataset.tabs!=2){
            e.preventDefault();
            this.parentNode.classList.add('active');
            Array.prototype.forEach.call(_eles,function(child){
              if(child!=c){
                child.parentNode.classList.remove('active');
              }
            });
            showArticle(this,this.dataset.tabs);
          }
        })
      })
    })
  }
  function showArticle(el,tabNum) {
    arr = closest(el,'.bx_tabs').querySelector('.wrap_components').querySelectorAll('.ctn_tabs');
    Array.prototype.forEach.call(arr,function(a){
      if(a.dataset.tabs == tabNum){
        a.style.display = "block";
      }
      else {
        a.style.display = "none";
      }
    });
  }
  return Tab;
})()