jQuery(function($) {
	$(document).ready(function() {
		//scroll
        $(function() {
	    	if ($('.pageTop a').length) {
			    var scrollTrigger = 100, // px
			        backToTop = function () {
			            var scrollTop = $(window).scrollTop();
			            if (scrollTop > scrollTrigger) {
			                $('.pageTop a').addClass('show');
			            } else {
			                $('.pageTop a').removeClass('show');
			            }
			        };
			    backToTop();
			    $(window).on('scroll', function () {
			        backToTop();
			    });
			    $('.pageTop a').on('click', function (e) {
			        e.preventDefault();
			        $('html,body').animate({
			            scrollTop: 0
			        }, 700);
			    });
			}
	    })
	});
});