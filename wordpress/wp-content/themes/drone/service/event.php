<?php
/*
  Template Name: event
 */
get_header();
?>

<body id="service">
    <!-- You write code for this content block in another file -->
    <div id="container">
           <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>

    <!-- You write code for this content block in another file -->
    

<!-- /header -->

<section class="banner">
    <div class="container">
    	<div class="banner__main">
    		<h3 style="line-height: 1.1em">SKY BUSINESS MEMBERS<br>
ONLY EVENT</h3>
    		<span>スカイビジネスメンバーズ 限定イベント</span>
    	</div>
    </div>
</section><!-- .banner // -->
	
	
	<section class="lineQualification">
		<article class="container">
			<div class="headBox pc">
				<h3>「スカイビジネス会員」限定のイベント</h3>
			</div>
			
						<div class="headBox sp">
				<h3>「スカイビジネス会員」限定の<br>イベントをご紹介!</h3>
			</div>
			
			<div class="whyIsDrones__main ac">
				<p>ドローンの操縦技術向上やスキルアップなど、<br>さまざまな便利ツールやシステムが利用できる
「スカイビジネス会員」限定のイベントをご紹介!</p>				
			</div>

				<div class="postThumb mb50">
					<div class="row">
						<div class="postThumb__text col-md-7">
							<h3>スカイビジネス会員とは</h3>
							<p>スカイビジネス会員は、「ドローンで何か事業を立ち上げたい」「ドローンを使って作業効率を改善したい」そんな風に思っているドローンユーザーのための会員サービスです。
ドローンに関するさまざまなオリジナル便利ツールやサービスを必要に応じて自由に利用することができ、ドローンのビジネス活用の可能性を拡げることができます。
</p>
						</div>
						<div class="postThumb__thumb col-md-5" style="text-align: right;">
                  <img src="<?php bloginfo('template_url')?>/service/skybusinessmember/images/business_01.png" alt="">
							
							
						</div>
					</div>
				</div>
				
               <div class="boxMore">
                <button type="button" class="btn"   onclick="location.href='https://drone-the-world.com/entry'">入会フォームへ  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
              </div>
				
			</div>
		</article>
	</section>
	<!-- End /box LINE qualification -->
	
		
			
				
					
						
							
	<section class="regarding">
		<article class="container">
			<div class="headBox pc">
				<h3>スカイビジネス会員限定のドローンイベントとは</h3>
			</div>	
			<div class="headBox sp">
				<h3>スカイビジネス会員限定の<br>ドローンイベントとは</h3>
			</div>				

			<div class="whyIsDrones__main ac">
				<p>ドローンネット社では、スカイビジネス会員限定でお楽しみいただけるさまざまなドローンイベントを企画・開催しています。</p>				
			</div>	
		</article>
	</section>		

	<section class="whyIsDrones">
		<article class="container">
	
			<div class="postThumbTitle" style="margin-top:-50px">
					<h3  class="pc">スカイビジネス会員限定イベント 例①<br>日本初！ドローン×ゴルフでラウンド体験できるイベント
</h3>	
		<h3  class="sp">スカイビジネス会員限定イベント 例①<br>日本初！ドローン×ゴルフで<br>ラウンド体験できるイベント
</h3>	
			</div>
			<div class="whyIsDrones__main ac">
				<p>日本初となる、ゴルフ場を貸し切り、ドローンと一緒にラウンドができるイベントを2018年3月16日(金)に開催しました。</p>				
			</div>	
			  

			  <div class="example">		
				<div class="thumb">

					<div class="mb30 ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event01.png" alt="thumb"></div>
							
					
				</div>
			 </div>
			 
			 <div class="whyIsDrones__main">
				<p>近年では、ドローンの市場は年々伸び続けており、ドローンが生活、趣味などあらゆるシーンで利用されることが当たり前になっています。本イベントでは、ファントム、インスパイア、マビックなど多種多様なドローンをゴルフ場内で飛ばし、参加者と一緒にラウンドしました。ゴルフ場を1日貸し切り実施する本イベントは、日本では初の開催でした。</p>				
			</div>	
			
			
			<div class="example">		
				<div class="thumb">

					<div class="mb30 ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event02.png" alt="thumb"></div>			
				</div>
			 </div>
			 

				<p>当日は、ドローン×ゴルフをより楽しんでもらえるように、ドローンを活用した様々なコンテンツを準備しました。
ドローンならではの特徴である空撮を利用したティーショット撮影やレース機による打球追跡、ドックレッグコースのドローンによるコースレイアウト確認、ドローンによるニアピン・ドラコン判定などを行いました。また、人の目では見えづらい暗闇でも活躍する赤外線カメラ搭載機を使用し、林などに打ち込んだボールを探すこともできました。<br>
<br>
今後も、ゴルフというメジャーなスポーツだけではなく、様々な業種・業態でドローンが活躍でき、楽しさと便利さ、更には、ドローンの将来性も発信していけるような企画を実施していきます。</p>				

	
			<div class="postThumbTitle" >
					<h3  class="pc"></i>スカイビジネス会員限定イベント 例②<br>ドローンビーナスと<br>ドローン飛行が楽しめるイベント</h3>	
					<h3  class="sp"></i>スカイビジネス会員限定イベント 例②<br>ドローンビーナスと<br>ドローン飛行が楽しめるイベント</h3>	
					
			</div>
			

				<p>”可愛い・飛ばせる・無限の可能性”が揃った新世代ユニット「DRONE VENUS」のメンバーをゲストに招いたドローンイベントを随時開催しています。</p>				

			
				<div class="thumb">

					<div class="mb30 ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event03.png" alt="thumb"></div>			
				</div>
			

			  <div class="example">		
				<div class="thumb">
					<div class="mb30 pc ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event4-1pc.png " alt="thumb"></div>	
					<div class="mb30 sp ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event4-1sp.png " alt="thumb"></div>			
		
					<div class="mb30 pc ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event4-2pc.png " alt="thumb"></div>	
					<div class="mb30 sp ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event4-2sp.png " alt="thumb"></div>		
					
					<div class="mb30 pc ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event4-3pc.png " alt="thumb"></div>	
					<div class="mb30 sp ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event4-3sp.png " alt="thumb"></div>		
					
					<div class="mb30 ac"><img src="<?php bloginfo('template_url')?>/service/event/images/event5.png" alt="thumb"></div>	
					<p class="note" style="font-size:0.8em; line-height: 1.5em;">※ご当選案内は、当選された方のみ、ご連絡します。※参加費には、飛行料、ドローンレンタルフィー、スタッフフィー、レース場利用料・諸経費、トークショー・交流会参加費、またお食事代（2,000円（税込）まで・ソフトドリンク1杯無料）が含まれています。</p>	
				</div>
			 </div>
			 
			 </div>
		</article>
	</section>
			
			
			
			
			




			
			


          <section class="lessonPrice">
            <article class="container">
              <div class="lessonPrice__main">


                <div class="lessonPrice__main-list">

                 <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </article>
          </section>
	<!-- End /box Lesson price -->
	<!-- End /box Lesson price -->
      </main>

      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>

  </html>