<?php
/*
  Template Name: Sky Easy Service
 */
get_header();
?>

<body>
    <!-- You write code for this content block in another file -->
    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
          <div class="container">
            <div class="banner__main">
              <h3>SKY EASY</h3>
              <span>スカイイージー</span>
            </div>
          </div>
       </section><!-- .banner // -->
        <section class="regarding">
          <article class="container">
            <div class="headBox pc">
              <h3> ドローン業務に必要な飛行申請書類を<br>簡単スムーズに作成できるアシストソフトウェア<br>「SKYEASY（スカイイージー）」</h3>
            </div>
                        <div class="headBox sp">
              <h3 style="font-size:1.6em;"> ドローン業務に必要な<br>飛行申請書類を簡単スムーズに作成できるアシストソフトウェア<br>「SKYEASY（スカイイージー）」</h3>
            </div>
            <div class="whyIsDrones__main">
              <p>SKYEASY（スカイイージー）は、ドローンパイロットがドローンを飛ばす際、場所や状況によって必要となる飛行許可・承認申請書を、簡単な入力をするだけで作成できるソフトウェアです。ドローン業務に必要な飛行申請書類の作成をパソコンで簡単に行えるため、確実性と利便性に優れた有益で画期的なサービスです。</p>
            </div>
            <div class="thumb">
              <div class="pc ac"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/skyeasy01.jpg?v=2dcbd155466cb51d4bd6baddf7f4fda8" alt="thumb"></div>
              <div class="sp ac"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/skyeasy01sp.jpg?v=c0aa3e0c2749311b31696f5f5b543368" alt="thumb"></div>
            </div>
          </article>
        </section>
          <section class="whyIsDrones">
            <article class="container">
              <div class="headBox">
                <h3>2020年代は年間数十兆円規模になり得る超巨大市場へ</h3>
              </div>
              <div class="whyIsDrones__main mb50">
                <p>ここ数年でドローンの需要が爆発的に拡がり、さまざまな業種でドローン活用の需要が高まっています。その市場規模は、2020年代には年間数十兆円規模になり得ると予測されています。しかし、このようなドローン市場の目覚ましい成長が見られる一方、航空安全の徹底が求められています。</p>
              </div>
              <div class="postThumbTitle">
                <h3><i class="fa fa-check" aria-hidden="true"></i> ドローン活用が可能な業種一覧</h3>
              </div>
              <div class="thumb">
                <div class="ac pc"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu01.jpg?v=45721bb712d0541e924413dba32bbd08" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac pc"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu02.jpg?v=c037df8f1db64ee6868be252e23173cd" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac pc"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu03.jpg?v=32c1ee7f743651b6fec821740f384d2b" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac pc"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu04.jpg?v=97882110b134f143304a9b0bf236c150" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac sp"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu01sp.jpg?v=38be56d4d8968e8f1ae8f02bc25b6adb" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac sp"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu02sp.jpg?v=902d69347cbfccf273cb63ed9cec670e" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac sp"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu03sp.jpg?v=ea1bfe37176dd113e7ffcd94debc489a" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac sp"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu04sp.jpg?v=6fe5060cc3d1d774b3ffec3e098244d5" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac sp"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu05sp.jpg?v=a9203df8368c7d103015b852a68dca50" alt="thumb"></div>
              </div>
              <div class="thumb">
                <div class="ac sp"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/kyoshu06sp.jpg?v=a9d3132986b6932b3a33b7a246cbea01" alt="thumb"></div>
              </div>
              <div class="headBox">
                <h3>航空法のルールである飛行許可･承認申請について</h3>
              </div>
              <div class="whyIsDrones__main mb50">
                <p>平成27年12月に航空法が改正されました。それまでは、法律上の縛りが殆ど無く、誰でも自由にどこでもドローンを飛ばすことができていましたが、改正航空法によってドローンの飛行規制ルールが定められ、人口密集地や上空150M以上等、飛行エリアや飛行形態によっては、国交省の許可・承認なしに飛行することができなくなりました。よって現在は、多くのドローンパイロットが空撮業務を行う上で、国交省に事前に各種飛行許可・承認の申請をしています。</p>
              </div>
              <div class="postThumbOnebl">
                <h3>2015年12月施行2017円６月交付された「改正航空法」では<span>「無人航空機の飛行などに関する罪」が定められており、この罪に該当するとされた場合は“五十万円以下の罰金に処する”とされています。</span></h3>
              </div>
              <div class="postThumbTitle">
                  <h3><i class="fa fa-check" aria-hidden="true"></i> ドローンを飛行禁止区域で飛行させる場合</h3>
              </div>
              <div class="example col-md-5 float-left">
                <h3>飛行禁止区域とは？</h3>
                <p class="ban"><i class="fa fa-ban blueli"></i> 人口・建物密集地区<br>
                <i class="fa fa-ban blueli"></i> お祭りなどのイベント上空<br>
                <i class="fa fa-ban blueli"></i> 空港周辺<br>
                <i class="fa fa-ban blueli"></i> 高度150m以上の空域<br>
                </p>
              </div>
              <div class="example col-md-5 float-left mb30">
                <h3>飛行禁止方法</h3>
                <p class="ban"><i class="fa fa-ban blueli"></i> 夜間飛行<br>
                <i class="fa fa-ban blueli"></i> 目視外飛行<br>
                <i class="fa fa-ban blueli"></i> 第三者・建物・車の30m未満の距離<br>
                <i class="fa fa-ban blueli"></i> 危険物輸送<br>
                <i class="fa fa-ban blueli"></i> 物件投下
                </p>
              </div>
              <div class="thumb">
                <div class="pc"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/skyeasy03.png?v=441fc64e18ae3177be57e2b229d0b63c" alt="thumb"></div>
                <div class="sp"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/skyeasy03sp.png?v=2ddb0c88cc889abd9878916a26a567a3" alt="thumb"></div>
              </div>
            </article>
          </section>
          <section class="whyIsDrones">
            <article class="container">
              <div class="headBox mb50">
                <h3>自分で飛行許可・承認を取得</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>本来、飛行申請は個人でできるものですが、行政へ申請する書類となるために知識が無くては、許可までに莫大な時間や手間がかかります。他の方法として行政書士に依頼することでスムーズに許可を取得する方法もありますが、費用がかかったり、多少の時間を要します。「スカイイージー」は基本的な情報を入力し、システムの流れによって必要な情報を選択しながら入力するだけで簡単に申請書類を作成できます。そのため費用をかけずに最低限の時間で飛行申請を取得することが可能です。</p>
              </div>
            </article>
          </section>
          <section class="lineQualification">
            <article class="container">
              <div class="postThumb">
                <div class="row">
                  <div class="postThumb__text col-md-7 ac">
                    <h3>申請書に記入しなければいけない内容</h3>
                  </div>
                  <div class="shinseilist mt20">
                    <li>飛行場所、飛行経路</li>
                    <li>飛行に知事</li>
                    <li>飛行目的</li>
                    <li>飛行させるドローンの機体情報</li>
                    <li>ドローンに対する知識</li>
                    <li>ドローン飛行実績</li>
                    <li>過去許可申請履歴</li>
                    <li>飛行マニュアル（トラブルが起きたときの対処法など）</li>
                    <li>飛行させる人リスト</li>
                    <li>飛行させる人の知識、飛行実績</li>
                  </div>
                  <div class="postThumb__thumb col-md-5 mt20 mb30 ac" >
                    <img src="<?php bloginfo('template_url')?>/service/skyeasy/images/skyeasy04.png?v=5d3514513b1a3cb49ad16b8e2ea1f05d" alt="thumb"  style="width:60%">
                  </div>
                  <div class="thumb">
                    <div class="pc ac mt40"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/skyeasy05.png?v=5fe59e691e1e94d9261c1bb11418c09a" alt="thumb" style="width:80%"></div>
                    <div class="sp ac"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/skyeasy05sp.png?v=640fd254bedd5234cda07fdee6a26ef6" alt="thumb"></div>
                  </div>
                </div>
              </div>
            </article>
          </section>
          <!-- End /box LINE qualification -->
          <section class="whyIsDrones">
            <article class="container">
              <div class="headBox mb50">
                <h3>スカイイージーについて</h3>
              </div>
              <div class="whyIsDrones__main">
                <p>スカイイージーの４つの特徴をご紹介します。</p>
                <div class="mb20"><p> <span class="tokucho">特徴 &#10102;　</span><br>初めて利用する方でもフォーマットが見やすく、直接入力をアシストしてくれるガイドラインがあるので、簡単に飛行許可・承認申請書を作成することができます。</p>
                </div>
                <div class="mb20"><p> <span class="tokucho">特徴 &#10103;　</span><br>日本全国包括/夜間/目視外での飛行など、各種飛行形態に対応した許可・承認申請書を、より簡単に作成することが可能です。2018年2月より航空法改正された催し物上空の飛行に必要な許可・承認申請書も、スカイイージーのアシスト機能によって、簡単スムーズに作成することができます。</p>
                </div>
                <div class="mb20"><p> <span class="tokucho">特徴 &#10104;　</span><br>国土交通省に飛行実績の報告が定期的に必要な包括飛行許可・承認を取得した場合でも、一定期間の飛行記録を簡単に、データとしてまとめたり保存することができるため、許可・承認後の飛行記録管理をドローンパイロットが、楽に行うことができます。</p>
                </div>
                <div class="mb20"><p> <span class="tokucho">特徴 &#10105;　</span><br>専門家に依頼しなくても、費用をかけず最低限の時間で、ドローンパイロットが自分で飛行許可・承認申請を行うことが可能です。</p>
                </div>
              </div>
              <div class="thumb">
                <div class="ac mt40"><img src="<?php bloginfo('template_url')?>/service/skyeasy/images/skyeasy06.png?v=ba57b569ad6de76d572510c5603b71e2" alt="thumb" ></div>
              </div>
            </article>
          </section>
          <section class="whyIsDrones">
            <article class="container">
              <div class="headBox">
                <h3>スカイイージーのご利用方法</h3>
              </div>
              <p>「スカイイージー」は、世界初や日本初のドローン関連商品＆サービスを提供する会員制サービス「スカイビジネス会員」に入会している方（個人、法人、自治体）限定販売となっております。</p>
              <div class="boxMore">
                <button type="button" class="btn mb30"   onclick="location.href='https://www.skyeasy.jp'">スカイイージーはこちら <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
              </div>
            </article>
          </section>
          <!-- End /box Regarding -->
          <section class="lessonPrice">
            <article class="container">
              <div class="lessonPrice__main">
                <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                   <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                    </ul>
                  </div>
                   <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </article>
          </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>

  </html>