<?php
/*
  Template Name: flight certification2 Service
 */
get_header();
?>

<body>
    <!-- You write code for this content block in another file -->

    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <!-- /header -->
      <main>
        <section class="banner">
          <div class="container">
            <div class="banner__main">
              <h3>FLIGHT CERTIFICATION</h3>
              <span>10時間飛行証明コース</span>
            </div>
          </div>
        </section><!-- .banner // -->
        <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>10時間飛行証明コース</h3>
            </div>
            <div class="lineQualification__main">
              <div class="gr_controller">
              
              <div class="example">
                  <h3>初級</h3>
              </div>
                <div class="row">

                  <div class="i_top col-md-4">
                  
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/flightcertification2/images/top_01.jpg" alt="">
                      <figcaption>計1時間（1時間×1コマ）</figcaption>
                    </figure>
                    <p>フライトシミュレーターを使って、ドローン専用コントローラーの使用方法をしっかりと学べる講座です。
</p>
                  </div>
                  <div class="i_top col-md-4">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/flightcertification2/images/top_02.jpg" alt="">
                      <figcaption>計５時間（1時間×5コマ）</figcaption>
                    </figure>
                    <p>ドローンを目視しながら、８の字飛行/円移動など30種類の技能をマスターできる講座です。<br>※実技試験（約10分）を含みます。</p>
                  </div>
                  <div class="i_top col-md-4">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/flightcertification2/images/top_03.jpg" alt="">
                      <figcaption>計１時間（1時間×1コマ）</figcaption>
                    </figure>
                    <p>ドローンの構造や航空法の重要部分を学べる講座です。<br>※座学試験（約15分）を含みます。</p>
                  </div>
                                    </div>


              <div class="example">
                  <h3>中級</h3>
              </div>
                <div class="row">

                  <div class="i_top col-md-4">
                  
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/flightcertification2/images/top_04.jpg" alt="">
                      <figcaption>計1時間（1時間×1コマ）</figcaption>
                    </figure>
                    <p>ドローンを目視せずに、FPV(一人称視点)ゴーグルの中の画面を見ながら、ドローン搭載カメラ目視で、８の字飛行/円移動など30種類の技能をマスターできる講座です。
<br>※実技試験（約10分）を含みます。
</p>
                  </div>
                  <div class="i_top col-md-4">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/flightcertification2/images/top_05.jpg" alt="">
                      <figcaption>計５時間（1時間×５コマ）</figcaption>
                    </figure>
                    <p>ドローン搭載カメラの映像を手元モニターで見ながら、ドローンの飛行位置や方向を把握しつつ、ノーズインサークルなど20種類の技能をマスターできる講座です。
<br>※実技試験（約10分）を含みます。

</p>
                  </div>
                  <div class="i_top col-md-4">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/flightcertification2/images/top_06.jpg" alt="">
                      <figcaption>計1時間（1時間×1コマ）</figcaption>
                    </figure>
                    <p>著作権、肖像権、プライバシー侵害、カメラワーク、包括申請等についての知識を学べる講座です。※座学試験（約15分）を含みます。
</p>
                  </div>






                </div>
              </div>
              <!--/.gr_broad-->
              <div class="b_price">
                <div class="row">
                  <div class="col-md-7">
                    <div class="lessonPrice__main-postThumbbg">
                      <div class="row">
                        <div class="textBox col-6">
                          <p>【合格証書】<br>合格すると、「10時間飛行履歴証明書」が発行されます。</p>
                        </div>
                        <div class="thumb col-6">
                          <div class="mainBox">
                            <img src="<?php bloginfo('template_url')?>/service/flightcertification2/images/lessonPrice2.png?v=8b44c60348e2ba88438bd511a73dd954" alt="thumb">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <dl>
                      <dt>受講価格</dt>
                      <dd>110,000円（税別）<span style="font-size:0.7em;">※機器レンタル無料</span></dd>
                    </dl>
                    <dl>
                      <dt>受講時間（コマ数）</dt>
                      <dd><span style="font-size:90%;">【初級コース】7時間+【中級コース】7時間</span><p style="font-size:0.6em; line-height:1.4">※ドローン操縦講座の総受講時間の7時間は、複数日に分けて受講することが可能です。</p></dd>
                    </dl>
                  </div>
                </div>
              </div>
              <!--/.b_price-->
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
        <section class="lineQualification">
          <article class="container">
            <div class="postThumb">
              <div class="row custom02">
                <div class="textBox col-md-7">
                  <h3>10時間飛行証明とは？</h3>
                  <p>
                    ドローン飛行の際には厳しい規則が設けられていますが、「夜間に飛行させたい」「目視外で飛行させる必要がある」「人口集中地域で飛行させたい」といったように、規則に触れての操縦を行う場合には、国土交通省の手続きを経て、「包括飛行許可」という認証を得る必要があります。<br>
包括飛行認証の申請を行う際には「10時間飛行」の実績が必須となり、この実績を得ることは容易なことではありません。

                  </p>
                </div>
                <div class="postThumb__thumb col-md-5" style="text-align: center;">
                  <img src="<?php bloginfo('template_url')?>/service/flightcertification2/images/fly.jpg" alt="">
                </div>
              </div>
            </div>
            <button type="button" class="btn" style="margin:30px 0;"   onclick="location.href='/service/flight/'">詳しくはこちら  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
          </article>
        </section>
        <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>その他コースのご案内</h3>
            </div>
            <div class="lineQualification__main">
              <div class="gr_infor_other">
                <div class="row">
                  <div class="item col-md-6">
                    <figure>
                      <a href="#"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_01.jpg" alt=""></a>
                    </figure>
                    <h3 style="color:visited {color: #eb6100}; :hover {color: #eb6100}; :active{color: #eb6100};"><a href="#">10時間飛行証明コース</a></h3>
                    <p>高価な機体を購入してスクールに通う必要がありませんので、気軽にドローンを体験、操縦スキルを習得することが出来ます。目視外飛行・ドローン中継不動産管理など、『ドローンビジネス』に必要な、即戦力のスキルが身につく資格取得講座。</p>
                  </div>
                  <!--/.item-->
                  <div class="item col-md-6">
                    <figure>
                      <a href="https://drone-the-world.com/service/school/controller_course/"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_02.jpg" alt=""></a>
                    </figure>
                    <h3><a href="https://drone-the-world.com/service/school/controller_course/">ドローン管制士 3級コース</a></h3>
                    <p>世界初！目視外ドローンのカメラ映像をライブ監視しながら、操縦士をアプリでナビゲートする最先端スキルが、短時間で身に付く、注目の資格取得講座。</p>
                  </div>
                  <!--/.item-->
                  <div class="item col-md-6">
                    <figure>
                      <a href="https://drone-the-world.com/service/school/service-school-broadcasting_course/"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_03.jpg" alt=""></a>
                    </figure>
                    <h3><a href="https://drone-the-world.com/service/school/service-school-broadcasting_course/">ドローン中継士 3級コース</a></h3>
                    <p>ドローンで撮影中の映像を、リアルタイムで複数の場所に生中継。ドローンによるリアルな空撮LIve映像を、いち早く遠隔にいる人たちへ届ける、これからの時代に欠かせない重要な職業の資格取得講座。</p>
                  </div>
                  <!--/.item-->
                  <!--<div class="item col-md-6">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_04.jpg" alt="">
                    </figure>
                    <h3>目視外飛行10時間訓練コース</h3>
                    <p>最先端の職業、ドローン管制士とドローン中継士の資格と、国土交通省の目視外飛行ガイドラインの操縦技量が取得できる資格取得訓練講座。</p>
                  </div>-->
                  <!--/.item-->
                </div>
              </div>
              <!--/.gr_infor_other-->
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
         <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>体験レッスンのご案内</h3>
            </div>
            <div class="lineQualification__main">
              <div class="b_qc">
                <img src="<?php bloginfo('template_url')?>/service/controller_2/images/qc_01.png" alt="">
              </div>
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
      </main>

      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
      <div id="overlay" class="overlay"></div>
    </div>
    <?php get_footer();?>
  </body>
</html>