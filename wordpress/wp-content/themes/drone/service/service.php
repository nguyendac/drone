<?php
/*
  Template Name: Service Search
 */
get_header();
?>

<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <section id="service" class="service">
      <div class="service_main row">
        <div class="service_main_l">
          <h2 class="roboto">SERVICE</h2>
          <ul class="objs">
            <?php
              $childs = apply_filters('get_childrens',$post->ID);
              while($childs->have_posts()):$childs->the_post();
            ?>
            <li>
              <?php
                $banner = (get_field('banner')) ? get_field('banner') : get_bloginfo('template_url')."/service/images/img_04.jpg";

              ?>
              <a href="<?php the_permalink()?>" data-img="<?php _e($banner)?>">
                <span><?php the_title();?></span>
                <figure>
                  <img src="<?php _e($banner)?>" alt="">
                  <figcaption>
                  <?php
                  $logo = '';
                  if(get_field('logo')) {
                    $logo = get_field('logo');
                    ?>
                    <img src="<?php _e($logo)?>" alt="">
                    <?php
                  } elseif(get_field('text_logo')) {
                    $logo =  get_field('text_logo');
                    _e($logo);
                  } else {
                    $logo = '';
                    _e($logo);
                  }
                  ?>
                  </figcaption>
                </figure>
              </a>
            </li>
          <?php endwhile;wp_reset_query()?>
          </ul>
        </div>
        <div class="service_main_r show_pc">
          <div id="slide" class="slide">
            <ul></ul>
          </div>
        </div>
      </div>
    </section><!-- end service -->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
  <div id="overlay" class="overlay"></div>
</div>
<?php get_footer();?>
</body>
</html>