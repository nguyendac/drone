<?php
/*
  Template Name: Broadcasting Service
 */
get_header();
?>
<body>
    <!-- You write code for this content block in another file -->

    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
          <div class="container">
            <div class="banner__main">
              <h3>DRONE BROADCASTING</h3>
              <span>ドローン中継士</span>
            </div>
          </div>
        </section><!-- .banner // -->

        <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>LINE映像配信を支える新しい職業「ドローン中継士」の資格が<br>取得できる日本初のスクール！</h3>
              <span>ドローン中継士３級</span>
            </div>
            <div class="lineQualification__main">
              <p>現場にわざわざ行かなくても内見・点検・測量・工事などの様々な現場をリアルタイムで見える化 !</p>
              <p>例えば、東京にいながら九州の建築現場をリアルタイムで監督できる !ドローンの業務依頼者が現場に行かずとも、リアルタイムで現場の操縦士(複数可)と同じ画面を見ながらコミュニケーションできる !<br>これら全ての事を可能にした最強ツールの、ドローンスコープ(超遠隔リアルタイムライブ監視&指示アプリ)を利用することが可能(有料)</p>
              <div class="thumb">
                <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/live.png?v=24782b042d55da4e7f42d7b56088d7d0" alt="thumb">
              </div>
              <div class="postThumb">
                <div class="row">
                  <div class="postThumb__text col-md-7">
                    <h3>ドローン中継士とは？</h3>
                    <p>ドローン中継士とは、ドローンで撮影中の映像を、リアルタイムで複数の場所に生中継する職業です。<br>従来のドローン技術では困難だった、ドローンによるリアルな空撮LIVE映像を、いち早く遠隔にいる人たちへ届ける、<br>これからの時代に欠かせない重要な役割といえます。<br>ドローン中継士になるには、ドローン人材育成スクールに通い、資格を取得する必要があります。</p>
                  </div>
                  <div class="postThumb__thumb col-md-5" style="text-align: center;">
                    <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/lineQualification.png?v=5e6fd484b214f68f1ec78496f7b3d769" alt="thumb">
                  </div>
                </div>
              </div>
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->

        <section class="whyIsDrones">
          <article class="container">
            <div class="headBox">
              <h3>なぜドローン中継士が必要とされるのか</h3>
            </div>
            <div class="whyIsDrones__main">
              <p>自然災害の多い日本において、ドローン中継士という職業は非常にニーズが高まっています。なぜドローン中継士はそれほど必要とされているのでしょうか。それは、災害時の救助活動において抱えている次の課題がポイントとなっています。</p>
              <div class="postThumbTitle">
                <h3><i class="fa fa-check" aria-hidden="true"></i> 救護職員の人材不足</h3>
                <div class="row">
                  <div class="postThumbTitle__thumb col-md-6">
                    <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/whyIsDrones1.png?v=7ba4ed24c6307981e7392f0cda81c5b4" alt="thumb">
                  </div>
                  <div class="postThumbTitle__text col-md-6">
                    <p>今日本で発生している自然災害の件数（被害規模）が、どのくらいかご存知でしょうか。</p>
                    <p class="line">火災発生件数が<span>39,198件</span>（平成29年）、水難事故件数が<span>1,505件</span>（平成28年）、山や雪山での遭難事故件数が<span>2,495件</span>（平成28年）と、たった1年のうちにこんなにもたくさんの災害が起こっているのです。<br>そして、その被害総額はなんと<span>350,637百万円</span>にものぼります。</p>
                    <p>それに対し、自衛隊の人数は22万4,422人、警察職員は29万5,664人、消防士は約16万人と、深刻な人材不足の問題を抱えている現状があります。</p>
                  </div>
                </div>
              </div>
              <div class="postThumbTitle mt-0">
                <h3><i class="fa fa-check" aria-hidden="true"></i> ヒトによる救命活動には限界がある</h3>
                <div class="row">
                  <div class="postThumbTitle__thumb col-md-6">
                    <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/whyIsDrones2.png?v=5247502a947df8374bcd276311750d5e" alt="thumb">
                  </div>
                  <div class="postThumbTitle__text col-md-6">
                    <p>災害時の救助活動における二つ目の課題は、「ヒトの力だけでは限界がある」という点です。</p>
                    <p>「高所・水上など危険な場所における作業の安全性」「目視のスピードや精度の高さ」「夜間・早朝での対応力」など、どうしてもヒトの手による救助活動には大きな課題や限界があります。</p>
                    <p>そして、ヒトによる救助活動の安全性が担保されることは、逆にいうと、1分1秒を争う災害現場での時間ロスにつながってしまうという厳しい課題も同時に抱えることになるのです。</p>
                  </div>
                </div>
              </div>
              <div class="thumb">
                <div class="pc"><img src="<?php bloginfo('template_url')?>/service/broadcasting/images/whyIsDrones3.png?v=fe27adfeeb5121cdd979f7d2621b488c" alt="thumb"></div>
                <div class="sp"><img src="<?php bloginfo('template_url')?>/service/broadcasting/images/whyIsDrones3-sp.png?v=abe0f4cb55dbf391b8d8c4805551a071" alt="thumb"></div>
              </div>
              <div class="arrow">
                <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/arrow.png?v=2f98613db83c8d285cf0846674504e46" alt="thumb">
              </div>
              <div class="postThumbOne">
                <div class="thumb">
                  <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/whyIsDrones4.png?v=5d4e8eea483963b090adae85fa0dd668" alt="thumb">
                </div>
                <h3>ドローン中継士は、１分１秒を争う災害現場状況や救護状況などを、<br>ドローンを使ってスピーディに生中継します。<br>多くの災害が発生する日本では、ドローン中継士の需要は<br>今後急激に伸びるといえます。</h3>
              </div>
            </div>
          </article>
        </section>
        <!-- End /box Why is a drones -->

        <section class="droneRelay">
          <article class="container">
            <div class="headBox">
              <h3>ドローン中継士はこんなところでも活躍します</h3>
            </div>
            <div class="droneRelay__main">
              <p>ドローン中継士の活躍する場は、災害時だけではありません。イベントなどのエンターテイメントや、スポーツの中継でもドローンによる生中継は大活躍します。</p>
              <div class="postThumbOne">
                <div class="thumb">
                  <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/droneRelay1.png?v=5634631062399dc59e1db70f7c8a6a3d" alt="thumb">
                </div>
                <h3>日本のコンテンツ産業の利用規模はなんと<span>12兆円。</span><br>そのうち映像部門は<span>4.3兆円</span>と、目覚ましい需要の拡大が見られます。<br>ドローン中継士は、災害時だけでなくこのようなコンテンツ中継の場でも、<br>大いに活躍しているのです。</h3>
              </div>
              <div class="thumb">
                <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/droneRelay2.png?v=bff03b9cd1a7ea3018cf3a9f075a51eb" alt="thumb">
              </div>
            </div>
          </article>
        </section>
        <!-- End /box Drone's relay -->

        <section class="regarding">
          <article class="container">
            <div class="headBox">
              <h3>ドローン中継士3級の受講について</h3>
            </div>
            <div class="regarding__main">
              <p>ドローン中継士３級を受講すると、当社のライブ配信システム「ドローンスコープ※」を使って、ドローンから複数のＰＣや<br>スマホ等の端末に空撮中の映像をインターネットライブ配信するスキルを習得することができます。</p>
              <div class="note">
                <p>※ドローンスコープとは、日本初となる空撮映像のインターネットライブ配信システムです。<br>基本的に従来の市販のドローンでは、数ｋｍ以上先に空撮映像をライブ配信することが非常に困難でしたが、ドローンスコープがあれば、<br>沖縄で空撮中の映像を北海道に簡単にライブ配信できます。<br>さらにドローンスコープは大掛かりな中継機材を一切使わず、低コストで複数のドローンから複数のパソコンやスマホへ同時にライブ配信が<br>可能です。空撮中の映像配信だけでなく、ドローンの位置情報地図もリアルタイムに配信できるので、どの場所の空撮映像なのか、<br>見ている側も一目でよく分かります(特許申請中)。</p>
              </div>
              <div class="thumb">
                <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/regarding1.png?v=6178cc7a1b66fd3b6f3d50e362c0e391" alt="thumb">
              </div>
              <p>ドローン中継士 ３級は、世界初や業界初のドローン関連商品＆サービスを提供する会員制サービス「スカイビジネス会員」に<br>入会している方(個人、法人、自治体)限定で受講可能となっております</p>
            </div>
          </article>
        </section>
        <!-- End /box Regarding -->

        <section class="lessonPrice">
          <article class="container">
            <h3><i class="fa fa-bookmark" aria-hidden="true"></i> 受講価格：３0,000円 (税別)</h3>
            <div class="lessonPrice__main">
              <div class="lessonPrice__main-list">
                <h3>「スカイビジネス会員」入会について</h3>
                <div class="lessonPrice__main-postThumb">
                  <div class="row">
                    <div class="thumb col-4">
                      <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                    </div>
                    <div class="textBox col-8">
                      <ul>
                        <li>入会金無料</li>
                        <li>月額7,980円(税別)</li>
                        <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                      </ul>
                      <button type="button" class="btn pc" style="margin:30px 0;">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </div>
                <button type="button" class="btn sp" style="margin:30px 0;">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                <div class="lessonPrice__main-postThumbbg">
                  <div class="row">
                    <div class="textBox col-md-7 col-6">
                      <p>ドローン中継士 ３級を受講し合格された方には、「ドローン中継士 ３級 資格証」が発行されます<br>(有効期限5年/民間資格)。</p>
                    </div>
                    <div class="thumb col-md-5 col-6">
                      <div class="mainBox">
                        <img src="<?php bloginfo('template_url')?>/service/broadcasting/images/lessonPrice2.png?v=8b44c60348e2ba88438bd511a73dd954" alt="thumb">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="lessonPrice__main-list">
                <h3>受講できる店舗/スクール一覧</h3>
                <div class="lessonPrice__main-nav">
                  <ul>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                  </ul>
                  <ul>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                  </ul>
                  <ul>

                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                  </ul>
                  <ul>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店</a></li>
                  </ul>
                </div>
                <div class="boxMore">
                  <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </article>
        </section>
        <!-- End /box Lesson price -->
      </main>

      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
      <div id="overlay" class="overlay"></div>
    </div>
    <?php get_footer();?>
    </body>
    </html>