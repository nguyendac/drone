	<?php
/*
  Template Name: Broadcasting 2 Service
 */
get_header();
?>

<body>
    <!-- You write code for this content block in another file -->

    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <!-- /header -->
      <main>
        <section class="banner">
          <div class="container">
            <div class="banner__main">
              <h3>DRONE BROADCASTING</h3>
              <span>ドローン中継士３級コース</span>
            </div>
          </div>
        </section><!-- .banner // -->
        <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>ドローン中継士３級コース</h3>
            </div>
            <div class="lineQualification__main">
              <div class="gr_broad">
                <div class="row">
                  <div class="b_img col-md-5">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/img_i_01.png" alt="">
                      <figcaption>2時間（座学）</figcaption>
                    </figure>
                  </div>
                  <div class="txt_right col-md-7">
                    <p>ドローンを飛行させるにあたり、ドローンパイロットとして気をつけなければいけない関連法規や、実際の運用方法などを学べる講座です。<br>実機を使いドローン映像を配信することができます。</p>
                  </div>
                </div>
              </div>
              <!--/.gr_broad-->
              <div class="b_price">
                <div class="row">
                  <div class="col-md-7">
                    <div class="lessonPrice__main-postThumbbg">
                      <div class="row">
                        <div class="textBox col-6">
                          <p>合格すると、<br>「ドローン中継士3級」の資格証が２週間以内に発行されます。</p>
                        </div>
                        <div class="thumb col-6">
                          <div class="mainBox">
                            <img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/lessonPrice2.png?v=8b44c60348e2ba88438bd511a73dd954" alt="thumb">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <dl>
                      <dt>受講価格</dt>
                      <dd>30,000円（税別）</dd>
                    </dl>
                    <dl>
                      <dt>受講時間（コマ数）</dt>
                      <dd>2時間（2時間×1コマ）</dd>
                    </dl>
                  </div>
                </div>
              </div>
              <!--/.b_price-->
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
        <section class="lineQualification">
          <article class="container">
            <div class="postThumb">
              <div class="row custom02">
                <div class="textBox col-md-7">
                  <h3>ドローン中継士とは？</h3>
                  <p>
                    ドローン中継士とは、ドローンで撮影中の映像を、リアルタイムで複数の場所に生中継する職業です。<br>
                    従来のドローン技術では困難だった、ドローンによるリアルな空撮LIVE映像を、いち早く遠隔にいる人たちへ届ける、<br>
                    これからの時代に欠かせない重要な役割といえます。<br>
                    ドローン中継士になるには、ドローン人材育成スクールに通い、資格を取得する必要があります。
                  </p>
                </div>
                <div class="postThumb__thumb col-md-5" style="text-align: center;">
                  <img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/fly.png" alt="">
                </div>
              </div>
            </div>
            <button type="button" class="btn" style="margin:30px 0;"    onclick="location.href='/service/broadcasting/'">詳しくはこちら  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
          </article>
        </section>
        <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>その他コースのご案内</h3>
            </div>
            <div class="lineQualification__main">
              <div class="gr_infor_other">
                <div class="row">
                  <div class="item col-md-6">
                    <figure>
                      <a href="<?php bloginfo('template_url')?>/service/flight/"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_01.jpg" alt=""></a>
                    </figure>
                    <h3 style="color:visited {color: #eb6100}; :hover {color: #eb6100}; :active{color: #eb6100};"><a href="<?php bloginfo('template_url')?>/service/flight/">10時間飛行証明コース</a></h3>
                    <p>高価な機体を購入してスクールに通う必要がありませんので、気軽にドローンを体験、操縦スキルを習得することが出来ます。目視外飛行・ドローン中継不動産管理など、『ドローンビジネス』に必要な、即戦力のスキルが身につく資格取得講座</p>
                  </div>
                  <!--/.item-->
                  <div class="item col-md-6">
                    <figure>
                      <a href="<?php bloginfo('template_url')?>/service/controller/"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_02.jpg" alt=""></a>
                    </figure>
                    <h3><a href="<?php bloginfo('template_url')?>/service/controller/">ドローン管制士 3級コース</a></h3>
                    <p>世界初！目視外ドローンのカメラ映像をライブ監視しながら、操縦士をアプリでナビゲートする最先端スキルが、短時間で身に付く、注目の資格取得講座</p>
                  </div>
                  <!--/.item-->
                  <div class="item col-md-6">
                    <figure>
                      <a href="<?php bloginfo('template_url')?>/service/broadcasting/"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_03.jpg" alt=""></a>
                    </figure>
                    <h3><a href="<?php bloginfo('template_url')?>/service/broadcasting/">ドローン中継士 3級コース</a></h3>
                    <p>ドローンで撮影中の映像を、リアルタイムで複数の場所に生中継。ドローンによるリアルな空撮LIve映像を、いち早く遠隔にいる人たちへ届ける、これからの時代に欠かせない重要な職業の資格取得講座。</p>
                  </div>
                  <!--/.item-->
                  <!--<div class="item col-md-6">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_04.jpg" alt="">
                    </figure>
                    <h3>目視外飛行10時間訓練コース</h3>
                    <p>最先端の職業、ドローン管制士とドローン中継士の資格と、国土交通省の目視外飛行ガイドラインの操縦技量が取得できる資格取得訓練講座。</p>
                  </div>-->
                  <!--/.item-->
                </div>
              </div>
              <!--/.gr_infor_other-->
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
         <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>体験レッスンのご案内</h3>
            </div>
            <div class="lineQualification__main">
              <div class="b_qc">
                <img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/qc_01.png" alt="">
              </div>
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
      </main>

      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
      <div id="overlay" class="overlay"></div>
    </div>
    <?php get_footer();?>
    </body>
    </html>