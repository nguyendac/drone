<?php
/*
  Template Name: School Service
 */
get_header();
?>

<body>
  <div id="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main>
      <section class="banner">
        <div class="container">
          <div class="banner__main">
            <h3>DRONE THE WORLD SCHOOL</h3>
            <span>ドローンザワールド スクール</span>
          </div>
        </div>
      </section><!-- .banner // -->
      <section class="whyIsDrones">
        <div class="container">
          <div class="bx_tabs">
            <div class="wrap">
              <ul class="tabs" id="tabs">
                <li class="active"><a href="" data-tabs="0">はじめての方へ</a></li>
                <li><a href="" data-tabs="1">コース案内</a></li>
                <li><a href="<?php _e(home_url())?>/shops/?type=スクール運営店" data-tabs="2">店舗一覧</a></li>
              </ul>
              <div id="wrap_tabs_components" class="wrap_components">
                <div class="ctn_tabs" data-tabs="0">
                  <div class="main_tabs">
                    <div class="bx_art">
                      <div class="headBox">
                        <h3>はじめての方へ<br>~DRONE THE WORLD SCHOOLとは~</h3>
                      </div>
                      <div class="gr_art row">
                        <div class="col-md-12">
                          <div class="bx_item">
                            <h4>DRONE THE WORLD SCHOOLとは</h4>
                            <p>DRONE THE WORLD SCHOOLは、プロが作った豊富なカリキュラムと、当社が独自に開発したマイクロドローンを使ってレッスンする、少人数制(1人～3人)の操縦教室です。趣味のカメラ撮影・ビジネス活用・レース参加といった、皆様の使用目的に応じた知識や技能を習得することができます。</p>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="bx_item">
                            <h4>DRONE THE WORLD SCHOOLが選ばれる理由</h4>
                            <div class="gr_item_w">
                              <div class="item_wrap row">
                                <div class="col-md-7">
                                  <h5>カリキュラムで使用するドローン・FPVゴーグルは全て無料貸し出し手ぶらで気軽にドローン操縦を学べます。</h5>
                                  <p>高価な機体を購入してスクールに通う必要がありませんので、気軽にドローンを体験、操縦スキルを習得することが出来ます。</p>
                                </div>
                                <div class="col-md-5">
                                  <figure>
                                    <img src="<?php bloginfo('template_url')?>/service/school/images/tabs01/img_01.jpg" alt="thumb">
                                  </figure>
                                </div>
                              </div>
                              <!--/.item_wrap-->
                              <div class="item_wrap row">
                                <div class="col-md-7">
                                  <h5>最大受講数３名までの楽しいレッスン密度の濃いカリキュラムで技術の最短取得ができます。</h5>
                                  <p>少人数制を導入することによって、ドローンの理解・技術上達スピードを早めることができます。「人が多く質問ができない」事がありませんので、安心してカリキュラムを受講できます。</p>
                                </div>
                                <div class="col-md-5">
                                  <figure>
                                    <img src="<?php bloginfo('template_url')?>/service/school/images/tabs01/img_02.jpg" alt="thumb">
                                  </figure>
                                </div>
                              </div>
                              <!--/.item_wrap-->
                              <div class="item_wrap row">
                                <div class="col-md-7">
                                  <h5>好きな時間割で受講が可能目的やライフスタイルに合わせて受講時間が選べます。</h5>
                                  <p>DRONE THE WORLD SCHOOLでは受講者の目的やライフスタイルに合わせて、授業時間を選択することが出来ます。</p>
                                  <p>例えばドローン操縦（入門）を受講する際に、<span>「１日１時間だけ受講して1週間で学びたい」「１日７時間の集中受講で一気に学びたい」「週末だけ受講して１ヶ月ゆっくりと学びたい」</span>など、あなたの時間に合った受講が全コースで行えます。</p>
                                </div>
                                <div class="col-md-5">
                                  <figure>
                                    <img src="<?php bloginfo('template_url')?>/service/school/images/tabs01/img_03.jpg" alt="thumb">
                                  </figure>
                                </div>
                              </div>
                              <!--/.item_wrap-->
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--/.gr_art-->
                    </div>
                    <!--/.bx_art-->
                    <div class="bx_art">
                      <div class="headBox">
                        <h3>受講者の声</h3>
                      </div>
                      <div class="gr_art">
                        <div class="item_bt">
                          <div class="row">
                            <div class="col-md-4">
                              <figure><img src="<?php bloginfo('template_url')?>/service/school/images/tabs01/img_04.jpg" alt=""></figure>
                            </div>
                            <div class="col-md-8">
                              <h4>初級受講者　太田在さん</h4>
                              <p>ドローンにはずっと興味はありましたが、チャレンジするきっかけがなく始められませんでした。近くにドローンザワールドのお店がオープンしたので体験させてもらいました！とても楽しかったのですぐに初級を受講しました。<br>私ができるのか不安でしたが、スタッフの方がわかりやすく操縦やドローンの事を教えてくださったのですぐに飛ばせるようになりました！</p>
                            </div>
                          </div>
                        </div>
                        <div class="item_bt">
                          <div class="row">
                            <div class="col-md-4">
                              <figure><img src="<?php bloginfo('template_url')?>/service/school/images/tabs01/img_05.jpg" alt=""></figure>
                            </div>
                            <div class="col-md-8">
                              <h4>中級受講者　嶋内さん</h4>
                              <p>ドローンは少し飛ばしたりする事は出来るのですが、もっと上手になりたくて受講しました。ただ単に飛ばしているだけでなく、いろんな動かし方、空撮で使うようなテクニックも学べて為になりました。<br>他にもFVPでの操縦を初めてやったのですがドローンに乗ってるような体験ができてとても面白かったです。もっとドローンが好きになりました！</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--/.gr_art-->
                    </div>
                     <!--/.bx_art-->
                  </div>
                  <!--/.main_tabs-->
                </div>
                <div class="ctn_tabs" data-tabs="1">
                  <div class="main_tabs">
					  
					  
					         <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>コースのご案内</h3>
            </div>
            <div class="lineQualification__main">
              <div class="gr_infor_other">
                <div class="row">
                  <div class="item col-md-6">
                    <figure>
                      <a href="/service/flight/"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_01.jpg" alt=""></a>
                    </figure>
                              <button type="button" class="btn" style="margin:30px 0;"  onclick="location.href='/service/flight/'">10時間飛行証明コース  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                    <p>高価な機体を購入してスクールに通う必要がありませんので、気軽にドローンを体験、操縦スキルを習得することが出来ます。目視外飛行・ドローン中継不動産管理など、『ドローンビジネス』に必要な、即戦力のスキルが身につく資格取得講座</p>
                  </div>
                  <!--/.item-->
                  <div class="item col-md-6">
                    <figure>
                      <a href="/service/school/controller_course/"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_02.jpg" alt=""></a>
                    </figure>
 <button type="button" class="btn" style="margin:30px 0;"  onclick="location.href='/service/school/controller_course/'">ドローン管制士 3級コース  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                    <p>世界初！目視外ドローンのカメラ映像をライブ監視しながら、操縦士をアプリでナビゲートする最先端スキルが、短時間で身に付く、注目の資格取得講座</p>
                  </div>
                  <!--/.item-->
                  <div class="item col-md-6">
                    <figure>
                      <a href="/service/school/service-school-broadcasting_course/"><img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/art_03.jpg" alt=""></a>
                    </figure>
<button type="button" class="btn" style="margin:30px 0;"  onclick="location.href='/service/broadcasting/'">ドローン中継士 3級コース  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                    <p>ドローンで撮影中の映像を、リアルタイムで複数の場所に生中継。ドローンによるリアルな空撮LIve映像を、いち早く遠隔にいる人たちへ届ける、これからの時代に欠かせない重要な職業の資格取得講座。</p>
                  </div>
 
                </div>
              </div>
              <!--/.gr_infor_other-->
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
         <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>体験レッスンのご案内</h3>
            </div>
            <div class="lineQualification__main">
              <div class="b_qc">
                <img src="<?php bloginfo('template_url')?>/service/broadcasting_2/images/qc_01.png" alt="">
              </div>
            </div>
          </article>
        </section>
        <!-- End /box LINE qualification -->
					  
					  
					  

                    
 
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>
</body>

</html>