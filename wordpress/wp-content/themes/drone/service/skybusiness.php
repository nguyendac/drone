<?php
/*
  Template Name: Sky Business Service
 */
get_header();
?>

<body>
    <!-- You write code for this content block in another file -->
    <div id="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main>
        <section class="banner">
          <div class="container">
            <div class="banner__main">
              <h3>SKY BUSINESS MEMBERS</h3>
              <span>スカイビジネスメンバーズ</span>
            </div>
          </div>
        </section><!-- .banner // -->
        <section class="lineQualification">
          <article class="container">
            <div class="headBox">
              <h3>スカイビジネスメンバーズ</h3>
            </div>
            <p>アジア初、日本初、業界初といった新規性＆進歩性を有する、ドローンソリューションを利用できる『会員制サービス』です。<br>ビジネス用からエンタメ用まで、ドローンネット社限定の便利でお得なオリジナルツール＆サービスをご利用頂けます。</p>
          </article>
        </section>
        <section class="lineQualification">
          <article class="container">
            <div class="postThumb">
              <h3>スカイビジネス会員のメリット</h3>
              <div class="row custom02">
                <div class="thumb col-4">
                  <img src="<?php bloginfo('template_url')?>/service/skybusiness/images/business_01.png?v=d96c3c0c7bc83463eba05c39a74f2b3a" alt="">
                </div>
                <div class="textBox col-8">
                  <p>「ドローンで何か事業を立ち上げたい」「ドローンを使って作業効率を改善したい」そんな風に思っているドローンユーザーの方は多くいます。<br>ですが、実際には、ドローンを購入しスクールで10時間飛行を練習を行うだけでは、事業における即戦力として活用することは不十分だという現状があります。</p>
                  <p>スカイビジネス会員では、ドローンの操縦技術向上やスキルアップなど、さまざまな便利ツールやシステムを必要分だけピックアップして利用することができるので、ドローンにおけるビジネス活用の可能性を拡げることが可能になります。</p>
                </div>
              </div>
            </div>
          </article>
        </section>
        <section class="regarding">
          <article class="container">
            <div class="whyIsDrones__main">
              <div class="video">
                <p><iframe width="836" height="440" src="https://www.youtube.com/embed/YaCTxtJJXxE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </p>
              </div>
            </div>
            <button type="button" class="btn" style="margin:30px 0;"   onclick="location.href='https://drone-the-world.com/entry'">入会フォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
            <figure style="margin-top: 30px;">
              <img src="<?php bloginfo('template_url')?>/service/skybusiness/images/business_02.png?v=41330fd6cb4c1a96bd2185116284e1dc" alt="">
            </figure>
          </article>
        </section>
        <section class="whyIsDrones customwhyIsDrones">
          <article class="container">
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i> オリジナルサービス</h3>
            </div>
            <div class="code01">
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_01.png?v=0b2e11f19a935f00fa58bf0d010d0f60" alt=""></figure>
                <div class="textBox">
                  <p><span>アジア初</span><em>▶︎ スカイストック</em></p>
                  <h4>ドローン空撮素材専門の売買マーケットプレイス</h4>
                  <p>空撮素材を購入・出品することが可能。出品者の報酬は販売額の50%</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_02.png?v=1b4ac617fca24689b7fc2cb99997b158" alt=""></figure>
                <div class="textBox">
                  <p><span>世界初</span><em>▶︎ドローンスコープ</em></p>
                  <h4>目視外ドローン視点映像生中継&操縦ナビゲートができるドローン管制アプリ</h4>
                  <p>ドローンの業務依頼者が現場に行かずとも、リアルタイムで現場の監督をし、操縦士(複数可)と同じ画面を見ながらコミュニケーションできる 管制アプリが使用可能。</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_03.png?v=47becc158f321fb546aa6210a9939623" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ スカイクラウド</em></p>
                  <h4>ドローンを使った業務の依頼者と操縦士のマッチングシステム</h4>
                  <p>ドローン操縦士は登録することによりドローンを使用する業務を受注することが可能。また、依頼企業は登録しているドローン操縦士に直接仕事の依頼や交渉をすることが可能。</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_04.png?v=134d9d0dcaea624ec4738ca930e1d7c1" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ ドローン官制士</em></p>
                  <h4>ドローン操縦士を遠隔地からナビゲートする時代の最先端の職業</h4>
                  <p>ドローンスコープを駆使しながら、目視外飛行中の複数ドローンの”指揮監督術”を習得することができる『ドローン官制士』の講座が受講可能。</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_05.png?v=af9a00da86c813b4678341bcb05dba99" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ スカイイージー</em></p>
                  <h4>ドローン業務に必要な飛行申請書類を簡単スムーズに作成できるアシストソフトウェア</h4>
                  <p>ドローンパイロットの支援システムとしてパソコンで簡単に利用でき、確実性と利便性に注力した有益で画期的なサービスが使用可能。</p>
                </div>
              </div>
                 <!--  <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_06.png?v=242277879b3413777c1a3b129e19a51b" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ ドローン中継士</em></p>
                  <h4>現場の様子をリアルタイムで生中継するドローンパイロットの最先端スキル資格</h4>
                  <p>ドローンスコープを駆使して、１分１秒を争う災害現場の状況把握やスポーツ観戦などの生中継を行うことができる『ドローン中継士3級』の資格取得講座が受講可能。</p>
                </div>
              </div>-->
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_07.png?v=0ad0baadd4b423ce2641a0278704d85b" alt=""></figure>
                <div class="textBox">
                  <p><span>日本初</span><em>▶︎ ドローンストア</em></p>
                  <h4>オリジナルグッズを販売する会員限定ECサイト</h4>
                  <p>ドローンネット社オリジナルグッズが購入可能。</p>
                </div>
              </div>
                  <!-- <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_08.png?v=3e681a7fe6a62555f441e6e6ff4a6bc4" alt=""></figure>
                <div class="textBox">
                  <p><span>世界初</span><em>▶︎ フューチャードロー</em></p>
                  <h4>ドローンパイロット側の手元タブレット上の地図に遠隔で特定の第三者が自由にお絵描きできるアプリ</h4>
                  <p>ドローンスコープを使用し、オフィスのパソコンからドローン飛行の道先案内や、探索エリアの限定を地図にお絵描きして指示することができるアプリの使用が可能。</p>
                </div>
              </div>-->
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_09.png?v=77b1e96b21ccb8fd2b739d6882ee197d" alt=""></figure>
                <div class="textBox">
                  <p><em>▶︎ AIチャットボット</em></p>
                  <h4>AIチャットボット</h4>
                  <p>ドローンについての質問にも答えてくれるAIチャットボットと会話することが可能。</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_10.png?v=d30d53efd9921909d39e3dbe74e8014c" alt=""></figure>
                <div class="textBox">
					   <p><em>▶︎ 会員限定イベント</em></p>
                  <h4>スカイビジネス会員限定のイベント</h4>
                  <p>ドローンビーナスをゲストに招いたドローンイベントや、ドローンと一緒にラウンドができるゴルフイベントなどに参加可能。</p>
                </div>

              
            </div>
          </article>
        </section>
        <section class="whyIsDrones customwhyIsDrones">
          <article class="container">
            <div class="postThumbTitle">
              <h3><i class="fa fa-check" aria-hidden="true"></i> オリジナルプロダクト</h3>
            </div>
            <div class="code01">
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_12.png?v=2b618d8c4495a3fa0972f7e5f8c087f0" alt=""></figure>
                <div class="textBox">
                  <p><span>アジア初</span><em>▶︎ デュアルサーチ</em></p>
                  <h4>アジア最軽量の小型ドローンへ簡単に装着可能なサーモグラフィカメラ装置</h4>
                  <p>約３０gのサーモグラフィカメラを、ユーザーが自分でドローンに取り付けて搭載できる装置が購入可能！%</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_13.png?v=55869a7bcdd42eace74989102047aec6" alt=""></figure>
                <div class="textBox">
                  <p><em>▶︎ スカイツインズ</em></p>
                  <h4>ドローン操縦用コントローラーをツインモニター化するための便利グッズ</h4>
                  <p>ドローンコントローラーに２個目のモニターを、ユーザーが自分で取り付けて装着できる便利グッズが購入可能！</p>
                </div>
              </div>
              <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_14.png?v=47becc158f321fb546aa6210a9939623" alt=""></figure>
                <div class="textBox">
                  <p><span>アジア初</span><em>▶︎ スカイライフジャケット</em></p>
                  <h4>ドローンにライフジャケットを装着して投下する装置</h4>
                  <p>アームの動作でドローン自らがライフジャケットを投下することができる装置が購入可能!</p>
                </div>
              </div>
             <div class="row">
                <figure><img src="<?php bloginfo('template_url')?>/service/skybusiness/images/icon_15.png?v=47becc158f321fb546aa6210a9939623" alt=""></figure>
                <div class="textBox">
                  <p><span>アジア初</span><em>▶︎ スカイサーチライト</em></p>
                  <h4>強力LED照明で夜間の飛行をサポート</h4>
                  <p>機体のネジを交換して取り付けることが可能な、Inspire2専用設計のLEDサーチライトが購入可能！</p>
                </div>
              </div>
            </div>
          </article>
        </section>
          <section class="lessonPrice">
            <article class="container">
              <div class="lessonPrice__main">
                <div class="lessonPrice__main-list">
                  <h3 style="margin-top: 30px;">「スカイビジネス会員」入会について</h3>
                  <div class="lessonPrice__main-postThumb">
                    <div class="row">
                      <div class="thumb col-4">
                        <img src="<?php bloginfo('template_url')?>/service/skystock/images/lessonPrice1.png?v=da67f47e890560824c083f4dab97b103" alt="thumb">
                      </div>
                      <div class="textBox col-8">
                        <ul>
                          <li>入会金無料</li>
                          <li>月額7,980円(税別)</li>
                          <li>飛行許可申請書を自動作成するソフトや、世界最軽量のサーマルドローンシステム等、様々な便利ツールをご利用できる特典付き。</li>
                        </ul>
                        <button type="button" class="btn pc" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn sp" style="margin:30px 0;"  onclick="location.href='https://drone-the-world.com/entry'">スカイビジネス会員へ入会する <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>

                </div>
                <div class="lessonPrice__main-list">

                  <div class="lessonPrice__main-nav">
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京dn店/">東京DN店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台フォーラス店/">仙台フォーラス店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/仙台南店/">仙台南店 </a></li>
							  <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福岡イオン乙金店/">福岡イオン乙金店 </a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/栃木宇都宮店/">栃木宇都宮店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/福島郡山店/">福島郡山店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京お台場店/">東京お台場店 </a></li><li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/名古屋千種店/">名古屋千種店 </a></li>
                    </ul>
                    <ul>

                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/千葉BIGHOP店/">千葉BIGHOP店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/東京千代田店/">東京千代田店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/秋田-akita店/">AKITA店</a></li>
                    </ul>
                    <ul>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/高知本町店/">高知本町店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/久慈-西モータース店/">久慈 西モータース店</a></li>
                      <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="/shop/京都-京田辺店/">京田辺店 </a></li>
                    </ul>
                  </div>
                  <div class="boxMore">
                                      <button type="button" class="btn" onclick="location.href='/contact/'">お問い合わせフォームへ <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </article>
          </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
  </body>
  </html>