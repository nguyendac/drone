<?php
require_once("meta-box-class/my-meta-box-class.php");
require_once("Tax-meta-class/tax-meta-class.php");
// require_once("theme_option.php");
function ga_js() {
  $ga = '<script type="text/javascript">';
  $ga .= "var _uic = _uic ||{}; var _uih = _uih ||{};_uih['id'] = 53905;
  _uih['lg_id'] = '';
  _uih['fb_id'] = '';
  _uih['tw_id'] = '';
  _uih['uigr_1'] = ''; _uih['uigr_2'] = ''; _uih['uigr_3'] = ''; _uih['uigr_4'] = ''; _uih['uigr_5'] = '';
  _uih['uigr_6'] = ''; _uih['uigr_7'] = ''; _uih['uigr_8'] = ''; _uih['uigr_9'] = ''; _uih['uigr_10'] = '';

  /* DO NOT ALTER BELOW THIS LINE */
  /* WITH FIRST PARTY COOKIE */
  (function() {
  var bi = document.createElement('script');bi.type = 'text/javascript'; bi.async = true;
  bi.src = '//cs.nakanohito.jp/b3/bi.js';
  var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(bi, s);
})();";
  $ga .= '</script>';
  echo $ga;
}
add_action( 'admin_head', 'ga_js' );
// Add hook for front-end <head></head>
add_action( 'wp_head', 'ga_js' );
function enqueue_script(){
  wp_enqueue_script('libs',get_template_directory_uri().'/common/js/libs.js', '1.0', 1 );
  if(is_front_page()) {
    wp_enqueue_script('toppage', get_template_directory_uri().'/js/script.js', '1.0', 1 );
    wp_enqueue_script('slick', get_template_directory_uri().'/common/js/slick/slick.js', '1.0', 1 );
  }
  if(is_page_template('service/service.php') || is_page_template('product/product.php')){
    wp_enqueue_script('service', get_template_directory_uri().'/service/js/script.js', '1.0', 1 );
  }
  if(is_page_template('service/school.php')) {
    wp_enqueue_script( 'school-script', get_stylesheet_directory_uri() . '/service/school/js/scripts.js');
  }
  if(is_tax('area')) {
    wp_enqueue_script( 'shop-script',get_stylesheet_directory_uri().'/shops/js/script.js');
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_script');
function enqueue_style() {
  if(is_front_page()) {
    wp_enqueue_style('slick',get_template_directory_uri().'/common/js/slick/slick.css');
    wp_enqueue_style('toppage',get_stylesheet_directory_uri().'/css/style.css');
  }
  if(is_page_template('shops/shops.php') || is_singular('shop') || taxonomy_exists('area')) {
    wp_enqueue_style( 'shops-style', get_stylesheet_directory_uri() . '/shops/css/style.css');
  }
  if(is_page_template('service/service.php') || is_page_template('product/product.php')){
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/style.css');
  }
  if(is_page_template('news/news.php') || is_singular('news') || taxonomy_exists('cate')) {
    wp_enqueue_style( 'news-style', get_stylesheet_directory_uri() . '/news/css/style.css');
  }
  if(is_page_template('about/about.php')) {
    wp_enqueue_style( 'about-style', get_stylesheet_directory_uri() . '/about/css/style.css');
  }
  if(is_page_template('contact/contact.php') || is_page_template('shops/contact-shop.php')) {
    wp_enqueue_style( 'contact-style', get_stylesheet_directory_uri() . '/contact/css/style.css');
  }
  if(is_page_template('company/company.php')) {
    wp_enqueue_style( 'company-style', get_stylesheet_directory_uri() . '/company/css/style.css');
  }
  if(is_page_template('privacy/privacy.php')) {
    wp_enqueue_style( 'privacy-style', get_stylesheet_directory_uri() . '/privacy/css/style.css');
  }
  if(is_page_template('service/broadcasting.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'broadcasting-style', get_stylesheet_directory_uri() . '/service/broadcasting/css/style.css');
  }
  if(is_page_template('service/controller.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'controller-style', get_stylesheet_directory_uri() . '/service/controller/css/style.css');
  }
  if(is_page_template('service/flight.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'flight-style', get_stylesheet_directory_uri() . '/service/flightcertification/css/style.css');
  }
    if(is_page_template('service/flightcertification2.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'flight-style', get_stylesheet_directory_uri() . '/service/flightcertification2/css/style.css');
  }
 
  if(is_page_template('service/school.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'school-style', get_stylesheet_directory_uri() . '/service/school/css/style.css');
  }
  if(is_page_template('service/skycloud.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'skycloud-style', get_stylesheet_directory_uri() . '/service/skycloud/css/style.css');
  }
  if(is_page_template('service/skyeasy.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'skyeasy-style', get_stylesheet_directory_uri() . '/service/skyeasy/css/style.css');
  }
  if(is_page_template('service/skylight.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'skylight-style', get_stylesheet_directory_uri() . '/service/skylight/css/style.css');
  }
  if(is_page_template('service/store.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'store-style', get_stylesheet_directory_uri() . '/service/store/css/style.css');
  }
  if(is_page_template('service/skybusiness.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'skybusiness-style', get_stylesheet_directory_uri() . '/service/skybusiness/css/style.css');
  }
  if(is_page_template('service/skystock.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'skystock-style', get_stylesheet_directory_uri() . '/service/skystock/css/style.css');
  }
  if(is_page_template('service/skylifejacket.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'skylifejacket-style', get_stylesheet_directory_uri() . '/service/skylifejacket/css/style.css');
  }
  if(is_page_template('service/dronescope.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'dronescope-style', get_stylesheet_directory_uri() . '/service/dronescope/css/style.css');
  }
  if(is_page_template('service/mini.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'minimini-style', get_stylesheet_directory_uri() . '/service/minimini/css/style.css');
  }
  if(is_page_template('service/skymember.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'skybusinessmember-style', get_stylesheet_directory_uri() . '/service/skybusinessmember/css/style.css');
  }
  if(is_page_template('service/event.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'event-style', get_stylesheet_directory_uri() . '/service/event/css/style.css');
  }
  if(is_page_template('service/broadcasting2.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'broadcasting2-style', get_stylesheet_directory_uri() . '/service/broadcasting_2/css/style.css');
  }
  if(is_page_template('service/controller2.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'controller2-style', get_stylesheet_directory_uri() . '/service/controller_2/css/style.css');
  }
  if(is_page_template('product/skytwins.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'store-style', get_stylesheet_directory_uri() . '/product/skytwins/css/style.css');
  }
  if(is_page_template('product/dualsearch.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/service-child.css');
    wp_enqueue_style( 'dualsearch200_new-style', get_stylesheet_directory_uri() . '/product/dualsearch200_new/css/style.css');
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_style' );
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');
/* end post type shop */
function shop_arr(){
  $args = array(
    'public'=> true,
    'labels' => array(
      'name' => 'List Shop',
      'singular_name' => 'shop',
      'add_new_item' => 'New Shop',
    ),
    'supports' => array(
      'title'
    )
  );
  register_post_type('shop',$args);
}
add_action('init','shop_arr');
$config_shop = array(
  'id' => 'shop',
  'title' => 'Shop',
  'pages' => array('shop'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$shop_meta = new AT_Meta_Box($config_shop);
$shop_meta->addText('shop_email',array('name'=> 'Shop Email','style'=>'max-width :100%; width:100%; height:auto'));
$shop_meta->addImage('shop_image',array('name'=> 'Shop Image','style'=>'max-width :100%; width:100%; height:auto'));
$shop_meta->addTextarea('shop_intro',array('name'=>' Shop Short Intro'));
$shop_meta->addText('shop_date',array('name'=> 'Shop Date'));
$shop_meta->addText('shop_address',array('name'=> 'Shop Address'));
$shop_meta->addText('shop_time_open',array('name'=> 'Shop Time Open'));
$shop_meta->addText('shop_tel',array('name'=>'Shop Tel'));
$shop_meta->addText('shop_mail',array('name'=>'Shop Mail'));
$shop_meta->addText('shop_remark',array('name'=>'Shop Remark'));
$shop_meta->addCheckboxList('shop_type',array(1=>'スクール運営店',2=>'正規販売店舗',3=>'移動型店舗'),array('name'=> 'Shop Type'));
$shop_meta->Finish();
/* end post type shop */
function variable_shop(){
  global $variable_shop;
  $variable_shop = array(
    'shop_type' => array(1=>'スクール運営店',2=>'正規販売店舗',3=>'移動型店舗'),
  );
}
add_action('init','variable_shop');
/* national - local */
function create_area_shop(){
  $labels = array(
    'name'              => _x( 'Area Shop', 'taxonomy general name' ),
    'singular_name'     => _x( 'By Type Area Shop', 'taxonomy singular name' ),
    'search_items'      => __( 'Search By Area Shop' ),
    'all_items'         => __( 'All By Area Shop' ),
    'parent_item'       => __( 'Parent By Area Shop' ),
    'parent_item_colon' => __( 'Parent By Area Shop:' ),
    'edit_item'         => __( 'Edit By Area Shop' ),
    'update_item'       => __( 'Update By Area Shop' ),
    'add_new_item'      => __( 'Add New By Area Shop' ),
    'new_item_name'     => __( 'New By Area Shop' ),
    'menu_name'         => __( 'By Area Shop' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'area-shop' ),
  );
  register_taxonomy( 'area', array( 'shop' ), $args );
}
add_action( 'init', 'create_area_shop', 0 );
$config_area = array(
  'id' => 'order_area',
  'title' => 'Order By Area',
  'pages' => array('area'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$area_meta = new Tax_Meta_Class($config_area);
$area_meta->addText('order', array('name' => 'Order By Area'));
/* end */
// get all term //
function get_all_terms() {
  $terms = array();
  $parents = get_terms(
    array(
      'taxonomy' => 'area',
      'parent'   => 0,
      'hide_empty' => false,
      'meta_key' => 'order',
      'orderby'  => 'meta_value',
      'order'    => 'ASC',
    )
  );
  wp_reset_query();
  foreach($parents as $parent) {
    $childs = get_terms(
      array(
        'taxonomy' => 'area',
        'child_of' => $parent->term_id,
        'hide_empty' => false
      )
    );
    $term = new stdClass();
    $term->name_parent = $parent->name;
    $term->order = get_term_meta($parent->term_id,'order',true);
    $term->childs = $childs;
    array_push($terms,$term);
  }
  return $terms;
}
add_filter('all_terms','get_all_terms');
// end get all term //
// get all post all term //
function get_all_post_term($term_name){
  $args = array(
    'post_type' => 'shop',
    'posts_per_page' => -1,
    'orderby' => 'id',
    'order' => 'desc',
    'tax_query' => array(
      array(
        'taxonomy' => 'area',
        'field' => 'slug',
        'terms' => $term_name,
      )
    )
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('all_post_term','get_all_post_term',10,1);
// end get all post all term //
function taxonomy_hierarchy() {
  global $post;
  $taxonomy = 'area'; //Put your custom taxonomy term here
  $terms = wp_get_post_terms( $post->ID, $taxonomy );
  foreach ( $terms as $term ) {
    if ($term->parent == 0) {
      $myparent = $term;
    }
  }
  return $myparent->name;
}
function get_shop_type($type) {
  $arg = array(
    'post_type' => 'shop',
    'orderby'   => 'id',
    'order'     => 'desc',
    'meta_key' => 'shop_type',
    'meta_query' => array(
      'key' => 'shop_type',
      'value' => $type,
      'compare' => '='
    ),
    'posts_per_page' => -1,
  );
  $query = new WP_Query($arg);
  return $query;
}
add_filter('shop_type','get_shop_type',10,1);

// news //
function news_arr(){
  $args = array(
    'public'=> true,
    'labels' => array(
      'name' => 'News',
      'singular_name' => 'news',
      'add_new_item' => 'New Article',
    ),
    'supports' => array(
      'title',
      'editor',
      'excerpt'
    )
  );
  register_post_type('news',$args);
}
add_action('init','news_arr');
$config_ar = array(
  'id' => 'article',
  'title' => 'Article',
  'pages' => array('news'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$news_meta = new AT_Meta_Box($config_ar);
$news_meta->addImage('image_field',array('name'=>'Image News','style'=>'max-width :100%; width:100%; height:auto'));
$news_meta->Finish();
function create_type_article(){
  $labels = array(
    'name'              => _x( 'Name By Type News', 'taxonomy general name' ),
    'singular_name'     => _x( 'By Type News', 'taxonomy singular name' ),
    'search_items'      => __( 'Search By Type News' ),
    'all_items'         => __( 'All By Type News' ),
    'parent_item'       => __( 'Parent By Type News' ),
    'parent_item_colon' => __( 'Parent By Type News:' ),
    'edit_item'         => __( 'Edit By Type News' ),
    'update_item'       => __( 'Update By Type News' ),
    'add_new_item'      => __( 'Add New By Type News' ),
    'new_item_name'     => __( 'New By Type News' ),
    'menu_name'         => __( 'By Type News' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'parent_item'  => null,
    'parent_item_colon' => null,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'cate-news' ),
  );
  register_taxonomy( 'cate', array( 'news' ), $args );
}
add_action( 'init', 'create_type_article', 0 );
function wpse_139269_term_radio_checklist($args) {
  if (!empty($args['taxonomy']) && (($args['taxonomy'] === 'cate'))) {
    if (empty($args['walker']) || is_a($args['walker'], 'Walker')) { // Don't override 3rd party walkers.
      if (!class_exists('WPSE_139269_Walker_Category_Radio_Checklist')) {
        /**
         * Custom walker for switching checkbox inputs to radio.
         *
         * @see Walker_Category_Checklist
         */
        class WPSE_139269_Walker_Category_Radio_Checklist extends Walker_Category_Checklist {
          function walk($elements, $max_depth, $args = array()) {
            $output = parent::walk($elements, $max_depth, $args);
            $output = str_replace(
                    array('type="checkbox"', "type='checkbox'"), array('type="radio"', "type='radio'"), $output
            );
            return $output;
          }
        }
      }
      $args['walker'] = new WPSE_139269_Walker_Category_Radio_Checklist;
    }
  }
  return $args;
}
add_filter('wp_terms_checklist_args', 'wpse_139269_term_radio_checklist');
function action_taxo($taxo){
  $terms = get_terms(array(
    'taxonomy' => $taxo,
    'hide_empty' => false,
    'orderby' => 'ID',
    'order' => 'asc',
    'parent' => 0
  ));
  return $terms;
}
add_filter('list_taxo','action_taxo');
function action_news($name = '',$get){
  if(!$get) {
    $paged = 1;
  } else {
    if(array_key_exists('pg',$get)){
      if($get['pg']) {
        $paged = $get['pg'];
      } else {
        $paged = 1;
      }
    } else {
      $paged = 1;
    }
    // $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $search_tax = array();
  if($name) {
    $tax = array(
      'taxonomy' => 'cate',
      'field' => 'slug',
      'terms' => $name
    );
    array_push($search_tax,$tax);
  }
  $args = array(
    'post_type' => 'news',
    'orderby'   => 'id',
    'order'     => 'desc',
    'paged' => $paged,
    'tax_query' => $search_tax,
    'posts_per_page' => 10
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('list_news','action_news',10,2);
function mp_pagination($prev = 'Prev', $next = 'Next', $pages='') {
    global $wp_query, $wp_rewrite, $textdomain;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }
    if(array_key_exists('pg',$_GET)){
      if($_GET['pg']) {
        $pg = $_GET['pg'];
      } else {
        $pg = 1;
      }
    } else {
      $pg = 1;
    }
    $big = 999999999;
    $pagination = array(
    'base'      => @add_query_arg( 'pg', '%#%' ),
    'format'   => '',
    'current'   => max ( 1, $pg),
    'total'   => $pages,
    'prev_text' => __($prev,$textdomain),
    'next_text' => __($next,$textdomain),
    'type'   => 'list',
    'end_size'  => 1,
    'mid_size'  => 2
    );
    $return =  paginate_links( $pagination );
    // echo str_replace( "<a class='page-numbers'>", '<a class="page_num">', $return );
    echo $return;
}
// end news //
add_filter( 'wpcf7_validate_email*', 'custom_email_confirmation_validation_filter', 20, 2 );

function custom_email_confirmation_validation_filter( $result, $tag ) {
  if ( 'email-confirm' == $tag->name ) {
    $your_email = isset( $_POST['email'] ) ? trim( $_POST['email'] ) : '';
    $your_email_confirm = isset( $_POST['email-confirm'] ) ? trim( $_POST['email-confirm'] ) : '';
    if ( $your_email != $your_email_confirm ) {
      $result->invalidate( $tag, "Are you sure this is the correct address?" );
    }
  }
  return $result;
}

function action_get_childrens($pageID) {
  $arr = array(
    'post_type'      => 'page',
    'post_parent'       => $pageID,
    'order'             => 'ASC',
    'orderby'           => 'menu_order',
    'posts_per_page'    => -1
  );
  $query = new WP_Query($arr);
  return $query;
}
add_filter('get_childrens','action_get_childrens',10,1);
function action_get_3_news() {
 $arr = array(
   'post_type'      => 'news',
   'order'             => 'DESC',
   'orderby'           => 'id',
   'posts_per_page'    => 3
 );
 $query = new WP_Query($arr);
 return $query;
}
add_filter('get_3_news','action_get_3_news');
function widget() {
  register_sidebar(array(
    'name' => __('Service', 'service'),
    'id' => 'service',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
  ));
}
add_action('init', 'widget');
function action_get_thumb(){
  $thumbs = array();
  $pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'service/service.php'
  ));
  if(count($pages)):
  $service = $pages[0]->ID;
  $childs = apply_filters('get_childrens',$service);
  while($childs->have_posts()):$childs->the_post();
   if(get_field('banner')) {
     $obj = new stdClass();
     $obj->banner = get_field('banner');
     $obj->link = get_the_permalink();
     array_push($thumbs,$obj);
   }
 endwhile;wp_reset_query();endif;
  $pages_p = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'product/product.php'
  ));
  if(count($pages_p)):
  $product = $pages_p[0]->ID;
  $childs_p = apply_filters('get_childrens',$product);
  while($childs_p->have_posts()):$childs_p->the_post();
  if(get_field('banner')) {
    $obj = new stdClass();
    $obj->banner = get_field('banner');
    $obj->link = get_the_permalink();
    array_push($thumbs,$obj);
  }
endwhile;wp_reset_query();endif;
  return $thumbs;
}
add_filter('get_thumb','action_get_thumb');
function flash( $name = '', $message = '',$class = 'frm_error') {
  //We can only do something if the name isn't empty
  if(!empty($name)) {
    //No message, create it
    if(!empty( $message ) && empty( $_SESSION[$name])){
      if( !empty( $_SESSION[$name] ) ){
        unset( $_SESSION[$name] );
      }
      if( !empty( $_SESSION[$name.'_class'] ) ){
        unset( $_SESSION[$name.'_class'] );
      }
      $_SESSION[$name] = $message;
      $_SESSION[$name.'_class'] = $class;
    }
    //Message exists, display it
    elseif( !empty( $_SESSION[$name] ) && empty( $message ) ){
      echo $_SESSION[$name];
      unset($_SESSION[$name]);
      unset($_SESSION[$name.'_class']);
    }
  }
}
function distributor_arr(){
  $args = array(
    'public'=> true,
    'labels' => array(
      'name' => 'List Distributor',
      'singular_name' => 'Distributor',
      'add_new_item' => 'New Distributor',
    ),
    'supports' => array(
      'title'
    )
  );
  register_post_type('distributor',$args);
}
add_action('init','distributor_arr');
$config_distributor = array(
  'id' => 'distributor',
  'title' => 'Distributor',
  'pages' => array('distributor'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$distributor_meta = new AT_Meta_Box($config_distributor);
$distributor_meta->addText('distributor_address',array('name'=> 'Distributor Address'));
$distributor_meta->addText('distributor_phone',array('name'=> 'Distributor Phone'));
$distributor_meta->Finish();
function action_distributor() {
 $arr = array(
   'post_type'      => 'distributor',
   'order'             => 'DESC',
   'orderby'           => 'id',
   'posts_per_page'    => -1
 );
 $query = new WP_Query($arr);
 return $query;
}
add_filter('get_distributor','action_distributor');
?>