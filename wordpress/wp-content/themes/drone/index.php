<?php get_header();?>
<body>
  <!-- <video playsinline muted loop autoplay class="video" id="mv_video"><source src="<?php bloginfo('template_url')?>/images/dronecity.m4v" type="video/mp4"></video> -->
  <div id="mv_video" class="video show_pc">
    <div id="ytb"></div>
  </div>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main>
      <div class="banner" id="fv">
        <figure class="show_sp"><img src="<?php bloginfo('template_url')?>/images/img_banner_sp.jpg" alt="drone the world"></figure>
        <div class="bx_fig">
          <h2>ABOUT DRONE THE WORLD</h2>
          <p>ドローンザワールドについて</p>
          <span class="btn_white" href="#" id="viewed">View More</span>
        </div>
        <!--/.bx_fig-->
      </div>
      <!--/.banner-->
      <div class="ctn_main">
        <div class="bx_news">
          <div class="row">
            <ul class="list_news">
              <?php
                $news = apply_filters('get_3_news','');
                while ($news->have_posts()) : $news->the_post();
              ?>
              <li>
                <a href="<?php the_permalink()?>">
                  <time datetime="<?php the_time('Y-m-d')?>"><?php the_time('Y.m.d')?></time>
                  <em><?php the_title();?></em>
                </a>
              </li>
              <?php endwhile;wp_reset_query();?>
            </ul>
          </div>
        </div>
        <!--/.bx_news-->
        <div class="bx_item bx_top">
          <div class="row">
            <div class="link">
              <div class="link_r">
                <article>
                  <a href="/service">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/img_service.jpg" alt="Service">
                    </figure>
                    <div class="main_art">
                      <h3>SERVICE</h3>
                      <p>ドローンザワールドのサービス一覧</p>
                      <!--<em>テキストテキストテキストテキストテキストテキストテキスト<br>テキストテキストテキストテキストテキスト</em>-->
                      <span class="btn_black" href="#">View More</span>
                    </div>
                    <!--/.main_art-->
                  </a>
                </article>
              </div>
              <!--/.right-->
              <div class="link_l">
                <div class="g_item">
                  <a href="/service/sky-stock/">
                    <figure class="item">
                      <img src="<?php bloginfo('template_url')?>/images/img_small_01.jpg" alt="Small 01">
                      <figcaption>
                        <span>スカイストック</span>
                        <em>ドローン空撮素材のマーケットプレイス</em>
                      </figcaption>
                    </figure>
                  </a>
                </div>
                <!--/.item-->
                <div class="g_item">
                  <a href="/service/sky-cloud">
                    <figure class="item">
                      <img src="<?php bloginfo('template_url')?>/images/img_small_02.jpg" alt="Small 02">
                      <figcaption>
                        <span>スカイクラウド</span>
                        <em>依頼者と操縦士のマッチングシステム</em>
                      </figcaption>
                    </figure>
                  </a>
                </div>
                <!--/.item-->
              </div>
              <!--/.left-->
            </div>
            <!--/.link-->
            <div class="bx_slider">
              <ul class="slider">
                <?php
                  $pages = get_pages(array(
                    'meta_key' => '_wp_page_template',
                    'meta_value' => 'service/service.php'
                  ));
                  if(count($pages) > 0) :
                  $service = $pages[0]->ID;
                  $childs = apply_filters('get_childrens',$service);
                  while($childs->have_posts()):$childs->the_post();
                ?>
                <li>
                  <?php
                    $banner = (get_field('banner')) ? get_field('banner') : get_bloginfo('template_url')."/service/images/img_04.jpg";
                  ?>
                  <a href="<?php the_permalink();?>">
                    <figure>
                      <img src="<?php _e($banner)?>" alt="slider 01">
                      <figcaption>
                        <span><?php the_title();?></span>
                        <em><?php _e(get_field('intro'))?></em>
                      </figcaption>
                    </figure>
                  </a>
                </li>
              <?php endwhile;wp_reset_query();endif;?>

<!--               <li>
                  <a href="#">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/img_sl_controler.jpg" alt="slider 02">
                      <figcaption>
                    <span>ドローン管制士</span>
                        <em>Comming Soon</em>
                      </figcaption>
                    </figure>
                  </a>
                </li>
               <li>
                  <a href="#">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/img_sl_broadcasting.jpg" alt="slider 02">
                      <figcaption>
                    <span></span>
                        <em>Comming Soon</em>
                      </figcaption>
                    </figure>
                  </a>
                </li> 
  --> 
				</ul>
            </div>
            <!--/.slider-->
          </div>
        </div>
        <!--/.bx_item-->
        <div class="bx_item bx_center">
          <div class="row">
            <div class="link">
              <div class="link_r">
                <article>
                  <a href="/product">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/img_product.jpg" alt="product">
                    </figure>
                    <div class="main_art">
                      <h3>PRODUCT</h3>
                      <p>ドローンザワールドのプロダクト一覧</p>
                      <!--<em>テキストテキストテキストテキストテキストテキストテキスト<br>テキストテキストテキストテキストテキスト</em>-->
                      <span class="btn_black" href="#">View More</span>
                    </div>
                    <!--/.main_art-->
                  </a>
                </article>
              </div>
              <!--/.right-->
              <div class="link_l">
                <div class="g_item">
                  <a href="/product/dual-search-200-new/">
                    <figure class="item">
                      <img src="<?php bloginfo('template_url')?>/images/img_small_03.jpg" alt="Small 03">
                      <figcaption>
                        <span>デュアルサーチ</span>
                        <em>世界軽量の赤外線カメラ搭載ドローン</em>
                      </figcaption>
                    </figure>
                  </a>
                </div>
                <!--/.item-->
                <div class="g_item">
                  <a href="/product/skytwins">
                    <figure class="item">
                      <img src="<?php bloginfo('template_url')?>/images/img_small_04.jpg" alt="Small 04">
                      <figcaption>
                        <span>スカイツインズ</span>
                        <em>着脱式ツインモニター</em>
                      </figcaption>
                    </figure>
                  </a>
                </div>
                <!--/.item-->
              </div>
              <!--/.left-->
            </div>
            <!--/.link-->
            <div class="bx_slider">
              <ul class="slider">
                <?php
                  $pages_p = get_pages(array(
                    'meta_key' => '_wp_page_template',
                    'meta_value' => 'product/product.php'
                  ));
                  if(count($pages_p)):
                  $product = $pages_p[0]->ID;
                  $childs_p = apply_filters('get_childrens',$product);
                  while($childs_p->have_posts()):$childs_p->the_post();
                ?>
                <li>
                  <?php
                    $banner = (get_field('banner')) ? get_field('banner') : get_bloginfo('template_url')."/service/images/img_04.jpg";
                  ?>
                  <a href="<?php the_permalink();?>">
                    <figure>
                      <img src="<?php _e($banner)?>" alt="slider 01">
                      <figcaption>
                        <span><?php the_title();?></span>
                        <em><?php _e(get_field('intro'))?></em>
                      </figcaption>
                    </figure>
                  </a>
                </li>
              <?php endwhile;wp_reset_query();endif;?>
                <li>
                  <a href="#">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/img_sl_02_02.jpg" alt="slider 02">
                      <figcaption>
                        <em>Comming Soon</em>
                      </figcaption>
                    </figure>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/img_sl_02_03.jpg" alt="slider 03">
                      <figcaption>
                        <em>Comming Soon</em>
                      </figcaption>
                    </figure>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/img_sl_02_04.jpg" alt="slider 04">
                      <figcaption>
                        <em>Comming Soon</em>
                      </figcaption>
                    </figure>
                  </a>
                </li>
              </ul>
            </div>
            <!--/.slider-->
          </div>
        </div>
        <!--/.bx_center-->
        <section class="st_sky">
          <div class="row">
            <a href="/service/sky-business">
              <div class="sky_txt">
                <h3>SKY BUSINESS MENBERS</h3>
                <p>ドローンビジネス向け会員制サービス</p>
                <span class="btn_black" href="/service/sky-business/">View More</span>
              </div>
              <!--/.sky_txt-->
              <picture class="img_01">
                <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/img_sky_sp.jpg" />
                <img src="<?php bloginfo('template_url')?>/images/img_sky_pc.jpg" alt="Sky 01" />
              </picture>
            </a>
          </div>
        </section>
       <section class="st_sky">
          <div class="row">
            <a href="http://skyfight.jp">
              <div class="sky_txt">
                <h3>SKY FIGHT</h3>
                <p>ドローンレース</p>
                <span class="btn_black" href="http://skyfight.jp">View More</span>
              </div>
              <!--/.sky_txt-->
              <picture class="img_01">
                <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/img_skyfight_sp.jpg" />
                <img src="<?php bloginfo('template_url')?>/images/img_skyfight_pc.jpg" alt="WSP 01" />
              </picture>
            </a>
          </div>
        </section>
       <section class="st_sky">
          <div class="row">
            <a href="https://worldscanproject.com">
              <div class="sky_txt">
                <h3>WORLD SCAN PROJECT</h3>
                <p>ドローンで絶景・文化・遺跡を3Dスキャン</p>
                <span class="btn_black" href="https://worldscanproject.com">View More</span>
              </div>
              <!--/.sky_txt-->
              <picture class="img_01">
                <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/img_wsp_sp.jpg" />
                <img src="<?php bloginfo('template_url')?>/images/img_wsp_pc.jpg" alt="WSP 01" />
              </picture>
            </a>
          </div>
        </section>
        <!--/.st_sky-->
        <div class="gr_list">
          <div class="row">
            <div class="gr_list_l">
              <article>
                <a href="/shop?type=スクール運営店">
                  <picture class="img_01">
                    <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/img_bt_01_sp.jpg" />
                    <img src="<?php bloginfo('template_url')?>/images/img_bt_01_pc.jpg" alt="List 01" />
                  </picture>
                  <span class="art_txt">
                    <h3>SCHOOL LIST</h3>
                    <em>スクール一覧</em>
                  </span>
                  <!--/.art_txt-->
                </a>
              </article>
              <article>
                <a href="/shop?type=正規販売店舗">
                  <picture class="img_01">
                    <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/img_bt_02_sp.jpg" />
                    <img src="<?php bloginfo('template_url')?>/images/img_bt_02_pc.jpg" alt="List 01" />
                  </picture>
                  <span class="art_txt">
                    <h3>SHOP LIST</h3>
                    <em>店舗一覧</em>
                  </span>
                  <!--/.art_txt-->
                </a>
              </article>
              <article>
                <a href="/contact">
                  <picture class="img_01">
                    <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/img_bt_03_sp.jpg" />
                    <img src="<?php bloginfo('template_url')?>/images/img_bt_03_pc.jpg" alt="List 01" />
                  </picture>
                  <span class="art_txt">
                    <h3>CONTACT</h3>
                    <em>お問合せ</em>
                  </span>
                  <!--/.art_txt-->
                </a>
              </article>
            </div>
          </div>
        </div>
        <!--/.gr_list-->
        <div class="gr_social">
          <div class="row">
            <ul class="gr_social_l">
              <li>
                <a href="https://www.instagram.com/drone_theworld/">
                  <figure><img src="<?php bloginfo('template_url')?>/images/icon_int.png" alt="INSTAGRAM"></figure>
                  <h4 class="roboto">INSTAGRAM</h4>
                  <em>公式インスタグラム</em>
                </a>
              </li>
              <li>
                <a href="https://twitter.com/DroneNet_">
                  <figure><img src="<?php bloginfo('template_url')?>/images/icon_tw.png" alt="Twitter"></figure>
                  <h4 class="roboto">TWITTER</h4>
                  <em>公式ツイッター</em>
                </a>
              </li>
              <li>
                <a href="https://www.facebook.com/DroneNet.Inc/">
                  <figure><img src="<?php bloginfo('template_url')?>/images/icon_fb.png" alt="facebook"></figure>
                  <h4 class="roboto">facebook</h4>
                  <em>公式フェイスブック</em>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <!--/.gr_social-->
        <section class="st_contact">
          <div class="bx_contact">
            <a href="/contact">
              <picture class="img_01">
                <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/img_contact_sp.jpg" />
                <img src="<?php bloginfo('template_url')?>/images/img_contact_pc.jpg" alt="WiMAX 01" />
              </picture>
              <div class="bx_fig">
                <h2>CONTACT US</h2>
                <p>［ドローンザワールドの各種サービス・プロダクトについて<br class="show_sp">お問い合わせください］</p>
                <span class="btn_white" href="#">View More</span>
              </div>
              <!--/.bx_fig-->
            </a>
          </div>
          <!--/.bx_contact-->
          <div class="fv_slider carousel" id="carousel">
            <div class="main_carousel">
              <ul>
                <?php
                  $thumbs =  apply_filters('get_thumb','');
                  foreach($thumbs as $thumb):
                    if($thumb->banner):
                ?>
                <li><a href="<?php _e($thumb->link)?>"><img src="<?php _e($thumb->banner) ?>"></a></li>
              <?php endif;endforeach;?>
              </ul>
            </div>
          </div>
          <!--/.carousel-->
        </section>
        <!--/.st_contact-->
      </div>
      <!--/.ctn_main-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
    <div id="popup" class="popup">
      <div class="main_popup">
        <div id="content_popup" class="content_popup">
          <!-- <iframe width="900" height="506" src="https://www.youtube.com/embed/1uMe5OTQ5S0?controls=false&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
          <video playsinline>
            <source src="<?php bloginfo('template_url')?>/images/Dronecitywork-07032.m4v" type="video/mp4">
          </video>
        </div>
        <a class="close" id="close" href="#">close</a>
      </div>
    </div>
  </div>
<?php get_footer();?>
<script src="//www.youtube.com/iframe_api"></script>
</body>
</html>