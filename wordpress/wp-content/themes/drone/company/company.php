<?php
/*
  Template Name: Company Page
 */
get_header();
?>

<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">COMPANY</h2>
        <span>会社概要</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="st_company">
      <div class="row">
        <ul class="breadcrumb show_pc">
          <li><a href="">TOP</a></li>
          <li>COMPANY</li>
        </ul>
        <!--/.breadcrumb-->
        <div class="gr_company wrap">
          <table class="tbl_company">
            <tbody>
              <tr>
                <th>社名</th>
                <td>株式会社ドローンネット</td>
              </tr>
              <tr>
                <th>事業内容</th>
                <td>デジタルコンテンツ及び情報技術に関する商品とサービス等の企画、開発、制作並びに運営事業</td>
              </tr>
              <tr>
                <th>設立</th>
                <td>2017年3月</td>
              </tr>
              <tr>
                <th>資本金</th>
                <td>302,846,650円</td>
              </tr>
              <tr>
                <th>代表取締役社長</th>
                <td>村上一幸</td>
              </tr>
              <tr>
                <th>従業員数</th>
                <td>20名</td>
              </tr>
              <tr>
                <th>所在地</th>
                <td>〒102-0083　東京都千代田区麹町4-3-29 VORT 紀尾井坂ビル1階</td>
              </tr>
              <tr>
                <th>営業本部</th>
                <td>〒102-0083 東京都千代田区麹町4-3-4 宮ビル9F</td>
              </tr>
              <tr>
                <th>電話番号</th>
                <td><a class="tel" href="tel:03-6261-0440">03-6261-0440</a></td>
              </tr>
              <tr>
                <th>FAX</th>
                <td>03-6261-0450</td>
              </tr>
              <tr>
                <th>メールアドレス</th>
                <td><a class="mail" href="mailto:info@drone-net.co.jp">info@drone-net.co.jp</a></td>
              </tr>
              <tr>
                <th>ソフトウェア開発部</th>
                <td>〒135-0034　東京都江東区永代2丁目30番7号 パールファーストビル1・2階</td>
              </tr>
              <tr>
                <th>ハードウェア開発部</th>
                <td>〒106-0047　東京都港区南麻布2丁目9番19号 MINAMIAZABU2919ビル1階</td>
              </tr>
            </tbody>
          </table>
        </div>
        <!--/.gr_company-->
      </div>
    </section>
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>

<?php get_footer();?>
</body>
</html>