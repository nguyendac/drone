<!DOCTYPE html>
<html lang="ja">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NG2NJ9B');</script>
<!-- End Google Tag Manager -->

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Description" content="<?php bloginfo('description'); ?>">
<meta name="Keywords" content="<?php bloginfo('keywords'); ?>">
<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
<meta property="og:title" content="<?php bloginfo('name')?>">
<meta property="og:url" content="">
<meta property="og:image" content="">
<meta property="og:type" content="website">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url')?>/common/css/normalize.css">
<link rel="stylesheet" href="<?php bloginfo('template_url')?>/common/css/common.css">
<link rel="stylesheet" href="<?php bloginfo('template_url')?>/common/js/slick/slick.css">
<link rel="stylesheet" href="<?php bloginfo('template_url')?>/service/css/font-awesome.css">
<?php wp_head();?>
</head>