<?php
/*
  Template Name: News Page
 */
get_header();
?>

<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">NEWS</h2>
        <span>ニュース一覧</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section id="news" class="news row">
      <ul class="breadcrumb show_pc">
        <li><a href="/">TOP</a></li>
        <li>NEWS</li>
      </ul>
      <div class="news_wrap">
        <?php get_template_part('news/news','aside')?>
        <div class="news_list">
          <div class="news_list_wrap">
            <?php
              $news = apply_filters('list_news','',$_GET);
              while ($news->have_posts()) : $news->the_post();
              $cat = wp_get_post_terms($post->ID,'cate',array("fields" => "all"));
              $cat = $cat[0];
            ?>
            <article><a href="<?php the_permalink()?>">
                <figure>
                  <?php
                    if(get_post_meta($post->ID,'image_field',true)):
                  ?>
                  <img src="<?php _e(get_post_meta($post->ID,'image_field',true)['url'])?>" alt="<?php the_title()?>">
                  <?php else:?>
                    <img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="<?php the_title()?>">
                  <?php endif;?>
                  <figcaption><?php _e($cat->name)?></figcaption>
                </figure>
                <time datetime="<?php the_time('Y-m-d')?>"><?php the_time('Y.m.d')?></time>
                <h3><?php the_title(); ?></h3>
                <p><?php _e(get_the_excerpt())?></p>
              </a></article>
            <?php endwhile;wp_reset_query(); ?>
          </div>
          <div class="news_list_pagi">
            <?php
            mp_pagination($prev = '&lt;', $next = '&gt;', $pages=$news->max_num_pages);
            wp_reset_query();
            ?>
          </div>
        </div>
      </div>
    </section><!-- end news -->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>