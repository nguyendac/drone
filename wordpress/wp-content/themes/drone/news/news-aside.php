<aside class="news_aside" id="category">
  <h3 class="roboto">category</h3>
  <ul>
    <?php
      $types = apply_filters('list_taxo','cate');
      if($types):
      foreach($types as $type) :
    ?>
    <li><a href="<?php _e(get_term_link($type))?>"><?php _e(nl2br($type->name)) ?></a></li>
  <?php endforeach;endif;?>
  </ul>
</aside>