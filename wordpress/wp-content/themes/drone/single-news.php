<?php
get_header();
?>
<body>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">NEWS</h2>
        <span>ニュース一覧</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <div class="news" id="news">
      <div class="row wrap">
        <ul class="breadcrumb show_pc">
          <li><a href="/">TOP</a></li>
          <li><a href="/news">Next</a></li>
        </ul>
        <!--/.breadcrumb-->
        <div class="news_wrap">
          <?php get_template_part('news/news','aside')?>
          <?php $terms = wp_get_post_terms(get_the_ID(),'cate',array("fields" => "all"))[0];?>
          <div class="news_list">
            <article class="news_article">
              <?php
                if(get_post_meta($post->ID,'image_field',true)):
              ?>
              <figure class="news_article_banner">
                <img src="<?php _e(get_post_meta($post->ID,'image_field',true)['url'])?>" alt="<?php the_title()?>">
              </figure>
            <?php endif;?>
              <p class="info"><span><?php _e($terms->name)?></span><time datetime="<?php _e(get_the_date('Y-m-d'))?>"><?php _e(get_the_date('Y.m.d'))?></time></p>
              <h2 class="news_article_ttl"><?php the_title()?></h2>
              <!-- <div class="news_article_social"><img src="<?php bloginfo('template_url')?>/news/images/sns.png?v=6d00853ab01716bc2fbd04d5c3766d56" alt="sns"></div> -->
              <div class="news_article_social">
                <?php echo do_shortcode('[Sassy_Social_Share type="floating"]')?>
              </div>
              <div class="news_article_content">
                <?php _e(nl2br(the_content()))?>
              </div>
            </article>
          </div>
        </div>
        <!--/.gr_shop-->
      </div>
      <!--wrap-->
    </div>
    <!--/.st_shop-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>