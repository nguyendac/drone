<?php
get_header();
?>
<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main>
      <div class="gr_ttl">
        <div class="row wrap">
          <h2 class="roboto">SHOPS</h2>
          <span>店舗一覧</span>
        </div>
        <!--/wrap-->
      </div>
      <!--/.gr_ttl-->
      <?php
      $term_name = get_term_by('slug',get_query_var('term'),get_query_var( 'taxonomy' ));
      $parent_obj = get_term_by('id',$term_name->parent,'area');
      ?>
      <section class="st_shop">
        <div class="row wrap">
          <ul class="breadcrumb show_pc">
            <li><a href="/">TOP</a></li>
            <li><a href="/shops">SHOPS</a></li>
            <li><?php _e($parent_obj->name)?>の店舗一覧</li>
          </ul>
          <!--/.breadcrumb-->
          <div class="gr_shop_shoplist">
            <h3 class="ttl_art"><?php _e($parent_obj->name)?>の店舗一覧</h3>
            <div class="item_art">
              <?php
                $articles = apply_filters('all_post_term',$term_name);
                while($articles->have_posts()):$articles->the_post();
              ?>
              <article>
                <div class="top">
                  <h4><a href="<?php the_permalink()?>"><?php the_title();?></a></h4>
                </div>
                <?php
                if(get_post_meta($post->ID,'shop_image',true)): ?>
                  <figure><a href="<?php the_permalink();?>"><img src="<?php _e(get_post_meta($post->ID,'shop_image',true)['url'])?>" alt="<?php the_title()?>"></a></figure>
                <?php else:?>
                  <figure class="default_logo"><a href="<?php the_permalink()?>"><img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="<?php the_title()?>"></a></figure>
                <?php endif;?>
                <a class="btn_link" href="<?php the_permalink()?>"><span>詳細を見る</span></a>
                <!-- <em><span>お問合せ電話番号</span><a class="tel roboto" href="tel:<?php _e(get_post_meta($post->ID,'shop_tel',true))?>"><?php _e(get_post_meta($post->ID,'shop_tel',true))?></a></em> -->
                <p><?php _e(get_post_meta($post->ID,'shop_address',true))?></p>
              </article>
              <?php endwhile;wp_reset_query();?>
            </div>
            <!--/.item_art-->
            <div class="back_shop"><a href="/shops">店舗一覧へ戻る</a></div>
          </div>
          <!--/.gr_shop-->
          <?php 
            $distributors = apply_filters('get_distributor','');
            if($distributors->post_count):
          ?>
          <div class="gr_shop_area">
            <h4 id="show_list_area">正規販売代理店</h4>
            <div class="gr_shop_area_wrap">
              <?php 
                while($distributors->have_posts()):$distributors->the_post();
              ?>
              <article>
                <h5><?php the_title()?></h5>
                <address><?php _e(get_post_meta($post->ID,'distributor_address',true))?></address>
                <a href="tel:<?php _e(get_post_meta($post->ID,'distributor_phone',true))?>"><?php _e(get_post_meta($post->ID,'distributor_phone',true))?></a>
              </article>
            </div>
          </div>
        <?php endwhile;wp_reset_query();endif;?>
          <?php get_template_part('shops/shops','type')?>
        </div>
        <!--wrap-->
      </section>
      <!--/.st_shop-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer();?>
</body>
</html>