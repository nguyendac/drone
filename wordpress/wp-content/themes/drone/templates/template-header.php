<div class="bx_header">
  <div class="row">
    <h1 class="bx_header_logo">
      <a href="<?php _e(home_url())?>"><img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="Logo"></a>
    </h1>
    <!--/.logo-->
    <div class="hamburger hamburger--spin" id="hamburger">
        <div class="hamburger-box">
          <div class="hamburger-inner"></div>
        </div>
      </div>
    <div class="bx_header_bottom" id="bx_header_bottom">
      <nav id="nav" class="nav">
        <ul class="list_nav">
          <li><a href="<?php _e(home_url())?>">HOME</a></li>
          <li><a href="/service">SERVICE</a></li>
          <li><a href="/product">PRODUCT</a></li>
          <li><a href="/shops">SHOPS</a></li>
          <li><a href="/news">NEWS</a></li>
          <li><a href="/about">ABOUT</a></li>
          <li><a href="/contact">CONTACT</a></li>
        </ul>
      </nav>
      <!--/.nav-->
      <ul class="list_social show_pc" id="list_social">
        <li><a href="https://www.facebook.com/DroneNet.Inc/" class="icon_fb"  target="_blank">facebook</a></li>
        <li><a href="https://twitter.com/DroneNet_" class="icon_tw"  target="_blank">twitter</a></li>
        <li><a href="https://www.youtube.com/channel/UCD_Lc3jGDHqxSSYCpVaFOpA" class="icon_yt"  target="_blank">youtube</a></li>
        <li><a href="https://www.instagram.com/drone_theworld/" class="icon_int"  target="_blank">instagram</a></li>
        <!--<li class="ev_search">
          <span id="search" class="icon_s">search</span>
          <div class="bx_search" id="bx_search">
            <form class="frm_search" method="post">
              <input type="text" id="IpSearch">
              <button type="submit" class="btn_search"><img src="<?php bloginfo('template_url')?>/common/images/icon_search.png" alt="Icon Search"></button>
            </form>-->
            <!--/.frm_search-->
          </div>
          <!--/.bx_search-->
        </li>
      </ul>
      <!--/.list_social-->
    </div>
    <!--/.bottom-->
  </div>
</div>
<!--/.bx_header-->