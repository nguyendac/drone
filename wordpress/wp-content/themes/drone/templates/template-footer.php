<div class="bx_footer">
  <div class="row">
    <a href="<?php _e(home_url())?>" class="pagetop roboto" id="gotop">PAGETOP</a>
    <ul class="nav_ft roboto">
      <li><a href="/privacy">PRIVACY POLICY</a></li>
      <li><a href="/company">COMPANY</a></li>
    </ul>
    <!--/.nav_ft-->
  </div>
  <p class="copyright roboto">&copy; 2018 DRONE THE WORLD</p>
</div>
<!--/.bx_footer-->