<?php
/*
  Template Name: Contact Page
 */

// if( !session_id()){
//   session_start();
// }
// if(isset($_POST['sm'])) {
//   $check = array();
//   $mail = '';
//   $post = $_POST;
//   if(!isset($_POST['check_privacy'])) {
//     array_push($check,$key);
//     flash('check_privacy','<span class="txt_error">必須項目です</span>');
//   } else {
//     flash('check_privacy_checked','checked');
//   }
//   foreach($post as $key => $value) {
//     if(!$value) {
//       array_push($check,$key);
//       flash($key,'<span class="txt_error">必須項目です</span>');
//     }
//     if($key == 'email') {
//       if(!$value) {
//         array_push($check,$key);
//         flash($key,'<span class="txt_error">必須項目です</span>');
//       } else {
//         if(!filter_var($value,FILTER_VALIDATE_EMAIL)) {
//           flash($key,'<span class="txt_error">必須項目です</span>');
//         } else {
//           $mail = $value;
//         }
//       }
//     }
//     if($key == 'email_confirm' && $value != $mail) {
//       array_push($check,$key);
//       flash($key,'<span class="txt_error">必須項目です</span>');
//     }
//     if($value) {
//       flash($key.'_value',$value);
//     }
//   }
//   if(count($check) > 0) {
//     wp_safe_redirect(get_bloginfo('url').'/contact');
//     exit;
//   } else {
//     $to = 'daccuongdn@gmail.com ryu@waza-mono.com';
//     $subject = 'DRONE THE WORLD へのお問い合わせ';
//     $headers =  'MIME-Version: 1.0' . "\r\n";
//     $headers .= 'From: '.$post['last-name'].' <wordpress@dtw.dev-wazamono.com>' . "\r\n";
//     $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//     $mess_admin = "DRONE THE WORLD へのお問い合わせ<br><br>";
//     $mess_admin .= '━━━━━━━━━━━━━━━━━━━━━━━━<br>';
//     $mess_admin .= '以下の内容でメールを受け付けました。<br>';
//     $mess_admin .= '━━━━━━━━━━━━━━━━━━━━━━━━<br>';
//     $mess_admin .= '■ お名前　 '.$post['last_name'].'<br>';
//     $mess_admin .= '■ メールアドレス　 '.$post['email'].'<br>';
//     $mess_admin .= '■ お問い合わせ内容<br><br>';
//     $mess_admin .= $post['content'];
//     $mess_admin .= '<br><br>';
//     $mess_admin .= '———————-<br>';
//     $mess_admin .= 'このメールは DRONE THE WORLD (https://drone-the-world.com/) のお問い合わせフォームから送信されました';
//     mail($to, $subject, $mess_admin, $headers);
//   }
// }
get_header();
?>
<body>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">CONTACT</h2>
        <span>お問い合せ</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="st_contact">
      <div class="row">
        <ul class="breadcrumb show_pc">
          <li><a href="">TOP</a></li>
          <li>CONTACT</li>
        </ul>
        <!--/.breadcrumb-->
        <div class="gr_contact wrap">
          <h3 class="ttl_art">お問い合せ</h3>
          <p class="req">*必須</p>
          <!-- <form class="frm_contact" action="" method="post">
            <div class="frm_group">
              <label>お名前<sub>*</sub></label>
              <div class="ip_left frm_ip">
                <div class="cts_ip">
                  <input type="text" class="frm_control" placeholder="姓" name="first_name" value="<?php //flash('first_name_value')?>">
                  <?php //flash('first_name')?>
                </div>
                <div class="cts_ip">
                  <input type="text" class="frm_control" placeholder="名" name="last_name" value="<?php //flash('last_name_value')?>">
                  <?php //flash('last_name')?>
                </div>
              </div>
            </div>

            <div class="frm_group">
              <label>メールアドレス<sub>*</sub></label>
              <div class="ip_left">
                <input type="text" class="frm_control" placeholder="example@example.com" name="email" value="<?php //flash('email_value')?>">
                <?php //flash('email')?>
                <em>※確認のため2度入力してください</em>
              </div>
            </div>

            <div class="frm_group">
              <label>メールアドレス確認<sub>*</sub></label>
              <div class="ip_left">
                <input type="text" class="frm_control" placeholder="example@example.com" name="email_confirm">
                <?php //flash('email_confirm')?>
              </div>
            </div>

            <div class="frm_group">
              <label>お問い合わせ内容<sub>*</sub></label>
              <div class="ip_left">
                <textarea class="frm_textarea" name="content"><?php //flash('content_value')?></textarea>
                <?php //flash('content')?>
              </div>
            </div>

            <div class="frm_group">
              <label>個人情報の取扱い<sub>*</sub></label>
              <div class="ip_left">
                <label class="frm_checkbox"><input type="checkbox" name="check_privacy" class="check" <?php //flash('check_privacy_checked')?>><span><a href="/privacy">個人情報の取扱い</a>に同意する</span></label>
                <?php //flash('check_privacy')?>
              </div>
            </div>

            <div class="frm_btn">
              <button type="submit" name="sm" value="1"><span>送信する</span></button>
            </div>

          </form> -->
          <div class="frm_contact">
            <?php the_content();?>
          </div>
        </div>
        <!--/.gr_contact-->
      </div>
      <!--/.wrap-->
    </section>
    <!--/.st_contact-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>