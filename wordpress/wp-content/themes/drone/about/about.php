<?php
/*
  Template Name: About Page
 */
get_header();
?>

<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto">ABOUT</h2>
        <span>ドローンザワールドとは</span>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="st_about">
      <div class="row">
        <ul class="breadcrumb show_pc">
          <li><a href="">TOP</a></li>
          <li>ABOUT</li>
        </ul>
        <!--/.breadcrumb-->
        <div class="gr_about wrap">
          <figure class="logo_ab">
            <img src="<?php bloginfo('template_url')?>/about/images/logo_ab.png?v=62263b545dca31b2436800328a29c392" alt="Logo"  width="50%">
          </figure>
          <article>
            <h3 class="ttl_art">ドローン ザ ワールド とは</h3>
            <div class="main_art">
              <p>世界初・日本初の、ここでしか手に入らないオンリーワンプロダクト、オンリーワンサービス、オンリーワンイベントを提供する日本初コンセプトの体験型SHOP&SCHOOLです。</p>
              <figure>
                <img src="<?php bloginfo('template_url')?>/about/images/img_ab.png?v=307494ec6bd1d2d9bf1e8162bf8dc8a5" alt="Images company">
              </figure>
              <p>地図付きのLIVE配信システムや、ドローン管制システム、ドローンで撮影した映像の売買ができるマーケットプレイス、ドローン業務全般のお仕事マッチングシステム、管制士などの次世代の職業のパイオニアになれるスクール、国交相への飛行許可申請書類作成ツール、世界最軽量のサーマルドローン、会員限定イベントなど、オンリーワンのコンテンツが盛りだくさんです。</p>
            </div>
            <!--/.main_art-->
          </article>
          <article>
            <h3 class="ttl_art">ドローン ザ ワールドが提供する<br>ドローン関連コンテンツ</h3>
            <div class="main_art">
              <figure>
                <img src="<?php bloginfo('template_url')?>/about/images/img_fly.png?v=324dddd2a7ea9212bc164627a8b0d187" alt="Images flycam">
              </figure>
            </div>
            <!--/.main_art-->
          </article>
        </div>
        <!--/.gr_about-->
     </div>
    </section>
    <!--/.st_about-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>

<?php get_footer();?>
</body>
</html>