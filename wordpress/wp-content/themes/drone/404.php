<?php
get_header();
?>
<body>
<style>
  @media screen and (min-width: 769px), print {
    p{
      font-size: 1.4rem;
      line-height: 28px;
      color: #08001e;
      margin-bottom: 33px;
      text-align: center;
      margin-top: 50px;
    }
    .btn_top{
      text-align: center;
      margin: 30px 0;
    }
    .btn_top a{
      font-size: 1.4rem;
      line-height: 1;
      font-family: "Lato", sans-serif;
      color: #fff;
      width: 200px;
      height: 50px;
      display: -webkit-box;
      display: -webkit-flex;
      display: -moz-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: center;
      -webkit-justify-content: center;
      -moz-box-pack: center;
      -ms-flex-pack: center;
      justify-content: center;
      -webkit-box-align: center;
      -webkit-align-items: center;
      -moz-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      position: relative;
      -webkit-transform: perspective(1px) translateZ(0);
      -moz-transform: perspective(1px) translateZ(0);
      transform: perspective(1px) translateZ(0);
      -webkit-transition-property: color;
      -moz-transition-property: color;
      transition-property: color;
      -webkit-transition-duration: 0.3s;
      -moz-transition-duration: 0.3s;
      transition-duration: 0.3s;
      background: #000;
      border: 1px solid #000;
      margin: 0 auto 0;
    }
    .btn_top a:before{
      content: "";
      position: absolute;
      z-index: -1;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background: #fff;
      -webkit-transform: scaleX(0);
      -moz-transform: scaleX(0);
      -ms-transform: scaleX(0);
      transform: scaleX(0);
      -webkit-transform-origin: 50%;
      -moz-transform-origin: 50%;
      -ms-transform-origin: 50%;
      transform-origin: 50%;
      -webkit-transition-property: -webkit-transform;
      transition-property: -webkit-transform;
      -moz-transition-property: transform, -moz-transform;
      transition-property: transform;
      transition-property: transform, -webkit-transform, -moz-transform;
      -webkit-transition-duration: 0.3s;
      -moz-transition-duration: 0.3s;
      transition-duration: 0.3s;
      -webkit-transition-timing-function: ease-out;
      -moz-transition-timing-function: ease-out;
      transition-timing-function: ease-out;
    }
    .btn_top a:hover{
      color: #000;
    }
    .btn_top a:hover:before{
      -webkit-transform: scaleX(1);
      -moz-transform: scaleX(1);
      -ms-transform: scaleX(1);
      transform: scaleX(1);
    }
  }
  @media screen and (max-width: 768px) {
    p{
      font-size: 2.93333vw;
      line-height: 1.6;
      color: #000;
      margin-top: 4.8vw;
      text-align: center;
    }
    .btn_top a {
      font-size: 2.4vw;
      line-height: 1;
      font-family: "Lato", sans-serif;
      color: #fff;
      width: 33.33333vw;
      height: 8.4vw;
      display: -webkit-box;
      display: -webkit-flex;
      display: -moz-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: center;
      -webkit-justify-content: center;
         -moz-box-pack: center;
          -ms-flex-pack: center;
              justify-content: center;
      -webkit-box-align: center;
      -webkit-align-items: center;
         -moz-box-align: center;
          -ms-flex-align: center;
              align-items: center;
      position: relative;
      -webkit-transform: perspective(1px) translateZ(0);
         -moz-transform: perspective(1px) translateZ(0);
              transform: perspective(1px) translateZ(0);
      -webkit-transition-property: color;
      -moz-transition-property: color;
      transition-property: color;
      -webkit-transition-duration: 0.3s;
         -moz-transition-duration: 0.3s;
              transition-duration: 0.3s;
      background: #000;
      border: 1px solid #000;
      margin: 4.8vw auto 0;
    }
    .btn_top a:before {
      content: "";
      position: absolute;
      z-index: -1;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background: #fff;
      -webkit-transform: scaleX(0);
         -moz-transform: scaleX(0);
          -ms-transform: scaleX(0);
              transform: scaleX(0);
      -webkit-transform-origin: 50%;
         -moz-transform-origin: 50%;
          -ms-transform-origin: 50%;
              transform-origin: 50%;
      -webkit-transition-property: -webkit-transform;
      transition-property: -webkit-transform;
      -moz-transition-property: transform, -moz-transform;
      transition-property: transform;
      transition-property: transform, -webkit-transform, -moz-transform;
      -webkit-transition-duration: 0.3s;
         -moz-transition-duration: 0.3s;
              transition-duration: 0.3s;
      -webkit-transition-timing-function: ease-out;
         -moz-transition-timing-function: ease-out;
              transition-timing-function: ease-out;
    }
    .btn_top a:hover {
      color: #000;
    }
    .btn_top a:hover:before {
      -webkit-transform: scaleX(1);
         -moz-transform: scaleX(1);
          -ms-transform: scaleX(1);
              transform: scaleX(1);
    }
  }
</style>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <h2 class="roboto" style="font-size:2em;">お探しのページは見つかりません。</h2>
      </div>
      <!--/wrap-->
    </div>
    <!--/.gr_ttl-->
    <section class="page">
      <div class="row wrap">
        <p>お探しのページは一時的にアクセスができない状況にあるか、<br>移動もしくは削除された可能性があります。</p>
        <div class="btn_top">
          <a href="<?php _e(home_url())?>"><span>TOPに戻る</span></a>
        </div>
      </div>
      <!--wrap-->
    </section>
    <!--/.st_shop-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>