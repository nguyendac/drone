<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'drone');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+AQ(ZIL;NwP!&6[U1x`cI3dJ&6B`S8@PGclMP2)|r6@Ka=Ta}%pV=;JT9h0J?E$?');
define('SECURE_AUTH_KEY',  'dxPUbXN|8kp|,d<uxz@RRKcs`:=Dse%be}fdEk6T$X-vGHS+QTg7e1D6Wo3O~W^m');
define('LOGGED_IN_KEY',    ' /:O&snB7?l5]m8)}(-W<bhqbLj<X^o5XD4c/kM4ea1ia=[kVBqE8-TasT [U/R-');
define('NONCE_KEY',        '_^P=~^JrwJY]l2unArQbkG4z`={T#c+0h`f,G59YkY^_^7#F47UaRsMzxNsD2r1C');
define('AUTH_SALT',        'X4`S@!(#I}TM7;BL2jH}.Na(TXAS8vsyh_:s9.Mqvk9:0tVuDA^|)Js*`yEzk=H^');
define('SECURE_AUTH_SALT', '<ZMnTjU6y[;}Ch~@cQz]Xx<m4oT9GYGB]+?qu^*id?^cPejYMPMq:=UXVBINq.@u');
define('LOGGED_IN_SALT',   'z]b=wy;=76vWqoI/ UQ6!,hTAZ3$iE54onr@sAHrC1S^$4o?X,(cM&5Bh%YQ9{!r');
define('NONCE_SALT',       'YAY|B~b!#]fO^dnaP#|qGohh(%@u-Hdw^fO#>B!`D^2GFocp>rG_BQIh,c[B: nd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
