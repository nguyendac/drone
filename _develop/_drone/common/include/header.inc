<div class="bx_header">
  <div class="row">
    <h1 class="bx_header_logo">
      <a href=""><img src="/common/images/logo.png" alt="Logo"></a>
    </h1>
    <!--/.logo-->
    <div class="hamburger hamburger--spin" id="hamburger">
        <div class="hamburger-box">
          <div class="hamburger-inner"></div>
        </div>
      </div>
    <div class="bx_header_bottom" id="bx_header_bottom">
      <nav id="nav" class="nav">
        <ul class="list_nav">
          <li><a href="#">HOME</a></li>
          <li><a href="#">SERVICE</a></li>
          <li><a href="#">PRODUCT</a></li>
          <li><a href="#">SHOPS</a></li>
          <li><a href="#">NEWS</a></li>
          <li><a href="#">ABOUT</a></li>
          <li><a href="#">CONTACT</a></li>
        </ul>
      </nav>
      <!--/.nav-->
      <ul class="list_social show_pc" id="list_social">
        <li><a href="#" class="icon_fb">facebook</a></li>
        <li><a href="#" class="icon_tw">twitter</a></li>
        <li><a href="#" class="icon_yt">youtube</a></li>
        <li><a href="#" class="icon_int">instagram</a></li>
        <li class="ev_search">
          <span id="search" class="icon_s">search</span>
          <div class="bx_search" id="bx_search">
            <form class="frm_search" method="post">
              <input type="text" id="IpSearch">
              <button type="submit" class="btn_search"><img src="/common/images/icon_search.png" alt="Icon Search"></button>
            </form>
            <!--/.frm_search-->
          </div>
          <!--/.bx_search-->
        </li>
      </ul>
      <!--/.list_social-->
    </div>
    <!--/.bottom-->
  </div>
</div>
<!--/.bx_header-->