window.addEventListener('DOMContentLoaded',function(){
  $('.slider').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    prevArrow:'<button type="button" class="slick-prev"></button>',
    nextArrow:'<button type="button" class="slick-next"></button>',
    autoplaySpeed: 6000,
    dots: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          arrows: true,
          dots: false,
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 476,
        settings: {
          arrows: true,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });
  $('.list_news').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    dots: false,
    arrows: false,
  })
  new Carousel();
  new Popup();
})
window.addEventListener('load',function(){
  new Fv();
})
var Popup = (function(){
  function Popup(){
    var p = this;
    this._target = document.getElementById('popup');
    this._content =  document.getElementById('content_popup');
    this._video = this._content.querySelector('video');
    this._obj = document.getElementById('viewed');
    this._obj.addEventListener('click',function(e){
      e.preventDefault();
      p._target.classList.add('open');
      // p._video.play();
      p._video.controls = true;
      document.body.style.overflow = 'hidden';
    });
    this._target.querySelector('.close').addEventListener('click',function(e){
      e.preventDefault();
      p._target.classList.remove('open');
      p._video.pause();
      document.body.style.overflow = 'inherit';
    })
    this._target.addEventListener('click',function(e){
      if(e.target.id == 'popup') {
        p._target.querySelector('.close').click();
      }
    })
  }
  return Popup;
})()
var Fv = (function(){
  function Fv(){
    var f = this;
    this._target =  document.getElementById('fv');
    this._video = document.getElementById('mv_video');
    this._header = document.getElementById('header');
    this.handling = function(){
      if(window.innerWidth > 768) {
        f._video.style.top = '50%';
        f._target.style.height = window.innerHeight - f._header.clientHeight+'px';
      } else {
        f._video.style.top = f._header.clientHeight+'px';
        f._target.style.height =  f._video.clientHeight+'px';
      }
    }
    this.handling();
    window.addEventListener('resize',f.handling,false);
  }
  return Fv;
})()
var Carousel = (function(){
  function Carousel(){
    var c = this;
    this._target = document.getElementById('carousel');
    this._carousel = this._target.querySelector('.main_carousel');
    new Slider(c._carousel, 'left');
  }
  return Carousel;
})()
var Slider = (function() {
  function Slider(el, direction) {
    var s = this;
    this.ul_child = el.querySelector('ul');
    this.ul_width = 0;
    this.ul_height;
    this.scale = 1;
    Array.prototype.forEach.call(s.ul_child.children, function(ele, i) {
      s.ul_width += ele.clientWidth;
      s.ul_width += parseInt(getCssProperty(ele,'margin-left'))*2;
      // ele.style.float = direction;
    });
    if (direction == 'left') {
      s.ul_child.style[fdProperty] = 'row';
    }
    if (direction == 'right') {
      s.ul_child.style[fdProperty] = 'row-reverse';
    }
    this.widthframe = this.ul_width;
    this.ul_child.style.width = this.ul_width + 'px';
    this.ul_child.style.height = this.ul_height + 'px';
    el.style.height = this.ul_height + 'px';
    this.ul_clone = this.ul_child.cloneNode(true);
    el.appendChild(this.ul_clone);
    this.timer;
    this.getRandomArbitrary = function(min, max) {
      return Math.random() * (max - min) + min;
    }
    if (direction === 'left') {
      this.basicleft = s.getRandomArbitrary(-this.widthframe, 0);
      this.cloneleft = this.widthframe - Math.abs(this.basicleft);
      this.ul_child.style.left = 0;
      this.ul_clone.style.left = s.widthframe + 'px';
    }
    if (direction === 'right') {
      this.basicleft = s.getRandomArbitrary(0, this.widthframe);
      this.cloneleft = -s.widthframe + this.basicleft;
      this.ul_clone.style.left = -s.widthframe + 'px';
      this.ul_child.style.left = 0;
    }
    this.step = 0.5;
    this.handling = function() {
      if (direction === 'left') {
        s.basicleft = s.basicleft - s.step;
        s.ul_child.style.left = s.basicleft + 'px';
        if (Math.abs(s.basicleft) > s.widthframe) {
          s.basicleft = s.widthframe;
        }
        s.cloneleft = s.cloneleft - s.step;
        s.ul_clone.style.left = s.cloneleft + 'px';
        if (Math.abs(s.cloneleft) > s.widthframe) {
          s.cloneleft = s.widthframe;
        }
        s.timer = window.requestAnimFrame(s.handling);
      }
      if (direction === 'right') {
        s.basicleft = s.basicleft + s.step;
        s.ul_child.style.left = s.basicleft + 'px';
        if (Math.abs(s.basicleft) > s.widthframe) {
          s.basicleft = -s.widthframe;
        }
        s.cloneleft = s.cloneleft + s.step;
        s.ul_clone.style.left = s.cloneleft + 'px';
        if (Math.abs(s.cloneleft) > s.widthframe) {
          s.cloneleft = -s.widthframe;
        }
        s.timer = window.requestAnimFrame(s.handling);
      }
    }
    // reset
    this.reset = function() {
      s.ul_width = 0;
      Array.prototype.forEach.call(s.ul_child.children, function(ele, i) {
        s.ul_width += ele.clientWidth;
      });
      s.widthframe = s.ul_width;
      s.ul_child.style.width = s.ul_width + 'px';
      s.ul_clone.style.width = s.ul_width + 'px';
      if (direction === 'left') {
        s.basicleft = s.getRandomArbitrary(-s.widthframe, 0);
        s.cloneleft = s.widthframe - Math.abs(s.basicleft);
        s.ul_child.style.left = 0;
        s.ul_clone.style.left = s.widthframe + 'px';
      }
      if (direction === 'right') {
        s.basicleft = s.getRandomArbitrary(0, this.widthframe);
        s.cloneleft = -s.widthframe + s.basicleft;
        s.ul_clone.style.left = -s.widthframe + 'px';
        s.ul_child.style.left = 0;
      }
    }
    // end reset
    // resize
    this.rtime;
    this.timeout = false;
    this.delta = 200;
    this.screen = window.innerWidth;
    this.resizeend = function() {
      if (new Date() - s.rtime < s.delta) {
        setTimeout(s.resizeend, s.delta);
      } else {
        s.timeout = false;
        s.reset();
        s.screen = window.innerWidth;
        s.handling()
      }
    }
    window.addEventListener('resize', function() {
      if (s.screen != window.innerWidth) {
        s.rtime = new Date();
        window.cancelAnimFrame(s.timer)
        if (s.timeout === false) {
          s.timeout = true;
          setTimeout(s.resizeend, s.delta);
        }
      }
    });
    // end resize
    this.handling();
  }
  return Slider;
})()
