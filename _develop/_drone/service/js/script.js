window.addEventListener('DOMContentLoaded',function(){
  new Service();
})
var Service = (function(){
  function Service(){
    var s = this;
    this._target = document.getElementById('service');
    this._objs = this._target.querySelectorAll('.objs li a');
    this.objsrc;
    this.stepwidth =  this._target.querySelector('#slide').clientWidth;
    this.wrap_f = this._target.querySelector('#slide ul');
    this.lies;
    this.b_f = 0;
    this.current = 0;
    this.action = false;
    this.sd = 0;
    this.delay;
    this.step = this.stepwidth/30;
    this.timer;
    this.mouseEnter = function(el,i){
      el.addEventListener('mouseenter',function(){
        if(i == s.current) {
          return;
        }
        if(s.action == false) {
          el.classList.add('active');
          s.handling_prev(i);
        }
      })
      el.addEventListener('mouseleave',function(){

      })
    }
    Array.prototype.forEach.call(s._objs,function(el,i){
      var li = document.createElement('li');
      var a = document.createElement('a');
      var link = el.getAttribute('href');
      var src = el.dataset.img;
      var img = new Image();
      img.onload = function(){
      };
      img.src = src;
      a.setAttribute('href',link);
      a.appendChild(img);
      li.appendChild(a);
      s.wrap_f.appendChild(li);
      s.mouseEnter(el,i);
    })
    this.pos = function() {
      var arrs = document.getElementById('slide').querySelectorAll('li')
      Array.prototype.forEach.call(arrs,function(el,k){
        if(k == s.current) {
          arrs[k].style.left = 0;
          arrs[k].style.display = 'block';
          s._objs[k].classList.add('active');
        } else {
          arrs[k].style.left = -s.stepwidth+'px';
          arrs[k].style.display = 'none';
        }
      })
      s.lies = arrs;
    }
    this.pos();
    this.handling_prev = function(nt) {
      var prev = nt;
      var cur = s.current;
      Array.prototype.forEach.call(s._objs, function(el, k) {
        if (k == prev) {
          el.classList.add('active');
        } else {
          el.classList.remove('active');
        }
      });
      s.lies[prev].style.display = 'block';
      var lc = 0;
      var lp = parseInt(getCssProperty(s.lies[prev],'left'));
      var timer;
      function tick(){
        if(lc < s.stepwidth) {
          s.action = true;
          timer = window.requestAnimFrame(tick);
          lc+=s.step;
          lp+=s.step;
          s.lies[cur].style.left = lc+'px';
          if(lp >= 0) {
            lp = 0;
          }
          s.lies[prev].style.left = lp+'px';
        } else {
          s.current = prev;
          s.pos();
          window.cancelAnimFrame(timer);
          s.action = false;
        }
      }
      tick();
    }
    this._links = this._target.querySelectorAll('a');
    this.overlay = document.getElementById('overlay');
    Array.prototype.forEach.call(s._links,function(link,k){
      link.addEventListener('click',function(e){
        e.preventDefault();
        var l = link.getAttribute('href');
        console.log(l);
        s.overlay.classList.add('active');
        s._target.classList.add('fadeOut');
        setTimeout(function(){
          location.href = l;
        },2000)
      })
    })
    window.addEventListener('load',function(){
      var c = 0;
      Array.prototype.forEach.call(s._objs,function(el,i){
        el.parentNode.style[trsd] = i*0.3+'s';
        c = i;
      })
      s._target.querySelector('.objs').classList.add('active');
      s._target.querySelector('#slide').classList.add('active');
      s._target.querySelector('#slide').style[trsd] = (c+1)*0.3+'s';
    })
  }
  return Service;
})()